//package com.vantec.pick.util;
//
//import static org.junit.Assert.assertTrue;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.List;
//
//import org.junit.Test;
//
//import com.vantec.pick.model.OrderObjects;
//
//public class OrderFileHandlerHelperTest {
//
//	@Test
//	public void testTokenizeHeader() {
//		String line = "TABNAM:EDI_DC40";
//		assertTrue(OrderFileHandlerHelper.tokenizeHeader(line).equals("EDI_DC40"));
//	}
//	@Test
//	public void testTokenizeHeaderBlank() {
//		String line = "TABNAM:";
//		assertTrue(OrderFileHandlerHelper.tokenizeHeader(line).equals(""));
//	}
//	@Test
//	public void testTokenizeHeaderTrimmed() {
//		String line = "TABNAM:   001";
//		assertTrue(OrderFileHandlerHelper.tokenizeHeader(line).equals("001"));
//	}
//	
//	@Test
//	public void testTokenizeBodyAndDetail() {
//		String line = "SEGNAM:E2LTORH,LGNUM:LSP,TANUM:8004727100,BWLVS:943,TBPRI:,"
//				+ "TRART:A,REFNR:,BETYP:,BENUM:030-020-R1,KZPLAC:,PLDAT:,PLZEI:,LZNUM:030-020-A,"
//				+ "BNAME:IFUSER,KISTZ:,KZLEI:,PERNR:00000000,SOLWM:         0.000,SOLEX:         0.000,"
//				+ "ISTWM:         0.000,ZEIEI:,STDAT:00000000,ENDAT:00000000,STUZT:000000,"
//				+ "ENUZT:000000,L2SKA:,LGTOR:,LGBZO:,NOSPL:,SWABW:0000,AUSFB:,VBTYP:,QUEUE:,KGVNQ:X,TAPRI:00,INCOM:,KVQUI:";
////		
//		assertTrue(OrderFileHandlerHelper.tokenizeDocument(line).get("TANUM").equals("8004727100"));
//		assertTrue(OrderFileHandlerHelper.tokenizeDocument(line).get("AUSFB").equals(""));
//		assertTrue(OrderFileHandlerHelper.tokenizeDocument(line).get("ISTWM").equals("0.000"));
//
//
//	}
//	
//	@Test
//	public void testPrepareOrderDetails() throws IOException {
//		File file  = new File("C://Users//tagrawal//Desktop/Format PCK.txt");
//		List<OrderObjects> orderObjects = OrderFileHandlerHelper.prepareOrderDetails(file);
//		
//		assertTrue(orderObjects.get(0).getHeader().equals("EDI_DC40"));
//		assertTrue("795106501".equals(orderObjects.get(0).getDetails().get(2).getDetails().get("MATNR")));
//		assertTrue("744447705".equals(orderObjects.get(1).getDetails().get(10).getDetails().get("MATNR")));
//		assertTrue("135232".equals(orderObjects.get(3).getDetails().get(1).getDetails().get("ZBZEIT")));
//		assertTrue("135232".equals(orderObjects.get(3).getDetails().get(1).getDetails().get("ZBZEIT")));
//	}
//	
//	
//
//}
