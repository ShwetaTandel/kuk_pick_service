package com.vantec.pick.job;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vantec.pick.entity.Company;
import com.vantec.pick.repository.CompanyRepository;
import com.vantec.pick.service.OrderService;
import com.vantec.pick.util.ConstantHelper;

@Component
public class ImportFile {
	
	private static Logger logger = LoggerFactory.getLogger(ImportFile.class);
	
	@Autowired
	CompanyRepository companyRepository;
	
	@Autowired
	OrderService orderService;
	
	
	@Scheduled(fixedRate = 20000)
	public void readFile() throws Exception {
		
		
					//String inpPath ="C:\\kukfiles\\input";
					//String tmpPath ="C:\\kukfiles\\temp";
					String inpPath =getPath();
					String tmpPath =getTempPath();
					//  List<File> filesInFolder = Files.walk(Paths.get(getPath())).filter(Files::isRegularFile).map(Path::toFile).collect(Collectors.toList());
					  List<File> filesInFolder = Files.walk(Paths.get(inpPath)).filter(Files::isRegularFile).map(Path::toFile)
								.collect(Collectors.toList());
					  
					  List<File> allFilesInFolder = new ArrayList<File>();
					  allFilesInFolder.addAll(filesInFolder.stream()
				 				.filter(file -> file.getName().startsWith(ConstantHelper.PICKFILE1))
				 				.collect(Collectors.toList()));
					  allFilesInFolder.addAll(filesInFolder.stream()
				 				.filter(file -> file.getName().startsWith(ConstantHelper.PICKFILE2))
				 				.collect(Collectors.toList()));
					  
					  
					  for(File file:allFilesInFolder){
						  
						  String fileName = file.getName();
						  logger.info("reading file " + file.getName());;
						  
						  try{
								  orderService.readOrder(file);
								 
						  
						  }catch(DataIntegrityViolationException ex){
							  	file.renameTo(new File(tmpPath+ "/"+fileName));
							  	logger.error("Moving file to temp folder due to error");
							  	logger.error(ex.getMessage(), ex);
				    			throw new EntityNotFoundException(ex.getMessage());
				    	  }catch(ParseException ex){
							  	file.renameTo(new File(tmpPath+ "/"+fileName));
							  	logger.error("Moving file to temp folder due to error");
							  	logger.error(ex.getMessage(), ex);
				    			throw new EntityNotFoundException(ex.getMessage());
				    	  }catch(Exception ex){
							  	file.renameTo(new File(tmpPath+ "/"+fileName));
							  	logger.error("Moving file to temp folder due to error");
							  	logger.error(ex.getMessage(), ex);
				    			throw new Exception(ex.getMessage());
				    	  }finally{
				    		  deleteFile(file);
				    	  }
					  }
		    
	}
	  
	  private  String getPath(){
		List<Company> companys =  companyRepository.findAll();
		return companys.get(0).getInterfaceInput();
	  }
	  private  String getTempPath(){
			List<Company> companys =  companyRepository.findAll();
			return companys.get(0).getInterfaceTemp();
		  }
	  
	  private void deleteFile(File file){
			
			try{
				logger.info("deleting  file " + file.getName());;
				file.delete();
			}catch(Exception ex){
				logger.error(ex.getMessage(), ex);
			}
			
			
		}
	  

	
}
