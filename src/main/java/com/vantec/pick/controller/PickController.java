package com.vantec.pick.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.pick.model.GetPickDTO;
import com.vantec.pick.model.PickDTO;
import com.vantec.pick.model.ProcessPickDTO;
import com.vantec.pick.service.PickService;

@RestController
@RequestMapping("/")
public class PickController {
	
	
	private static Logger logger = LoggerFactory.getLogger(PickController.class);
	
	
	@Autowired
	PickService pickService;
	
	   
	    
	    @RequestMapping(value = "/getPickDetail", method = RequestMethod.POST)
		public ResponseEntity<PickDTO> getPickDetail(@RequestBody GetPickDTO getPickDTO) throws IOException {
				
				try{
					logger.info("getPickDetail called.." +getPickDTO.getAllValues());
					return new ResponseEntity<PickDTO>(pickService.getPickDetail(getPickDTO), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "documentReference : " + getPickDTO.getPickDocRef() +
			    			                      "userId : " + getPickDTO.getUserId() +
			    			                      "task Type : " + getPickDTO.getPickTask());
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<PickDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    @RequestMapping(value = "/processPick", method = RequestMethod.POST)
		public ResponseEntity<PickDTO> processPick(@RequestBody ProcessPickDTO pickDTO) throws IOException {
				
				try{
					logger.info("processPick called.. "+pickDTO.getAllValues());
					return new ResponseEntity<PickDTO>(pickService.processPick(pickDTO), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "Pick Id : " + pickDTO.getPickId() +
			    			                      "userId : " + pickDTO.getUserId());
			    			                      
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<PickDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    @RequestMapping(value = "/test", method = RequestMethod.POST)
		public ResponseEntity<PickDTO> test() throws IOException {
				
				try{
					System.out.println("called");
			    }catch(Exception ex){
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<PickDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
				return new ResponseEntity<PickDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

}
