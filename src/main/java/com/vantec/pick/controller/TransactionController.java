package com.vantec.pick.controller;

import java.io.IOException;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.pick.service.PickService;

@RestController
@RequestMapping("/")
public class TransactionController {
	
	
	@Autowired
	PickService pickService;
	
	private static Logger logger = LoggerFactory.getLogger(TransactionController.class);
	    
	  @RequestMapping(value = "/ptttTransaction", method = RequestMethod.GET)
		public ResponseEntity<String> ptttTransaction(@RequestParam("tagReference") String tagReference ,@RequestParam("locationCode") String locationCode,@RequestParam("userId") String userId) throws ParseException, IOException {
				
				
				try{
					logger.info("ptttTransaction called..");
					return new ResponseEntity<String>(pickService.ptttTransaction(tagReference,locationCode,userId), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "tagReference : " + tagReference +
			    			                      "locationCode : " + locationCode + 
			    			                      "userId : " + userId);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
				
	  }
	  
	  @RequestMapping(value = "/mttTransaction", method = RequestMethod.POST)
		public ResponseEntity<String> mttTransaction(@RequestParam("tagReference") String tagReference ,@RequestParam("locationCode") String locationCode,@RequestParam("userId") String userId) throws ParseException, IOException {
				
				
				try{
					logger.info("mttTransaction called..");
					return new ResponseEntity<String>(pickService.mttTransaction(tagReference,locationCode,userId), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "tagReference : " + tagReference +
			    			                      "locationCode : " + locationCode + 
			    			                      "userId : " + userId);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
				
	  }

}
