package com.vantec.pick.controller;

import java.io.IOException;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.pick.model.AllocateByPartDTO;
import com.vantec.pick.model.AllocateDTO;
import com.vantec.pick.service.AllocationService;

@RestController
@RequestMapping("/")
public class AllocationController {
	
	
	private static Logger logger = LoggerFactory.getLogger(AllocationController.class);
	
	
	@Autowired
	AllocationService allocationService;
	
	    @RequestMapping(value = "/allocateOrders", method = RequestMethod.POST)
		public ResponseEntity<Boolean> allocateOrders(@RequestBody AllocateDTO allocateDTO) throws IOException {
				
				try{
					logger.info("allocate called..");
					return new ResponseEntity<Boolean>(allocationService.allocateOrders(allocateDTO), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "documentReference : " + allocateDTO.getAllDocRef() +
			    			                      "userId : " + allocateDTO.getUserId());
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    @RequestMapping(value = "/allocateWithTO", method = RequestMethod.GET)
		public ResponseEntity<Boolean> allocateWithTO(@RequestParam("documentReference") String documentReference,@RequestParam("userId") String userId) throws ParseException {
	    	
	    	try{
				logger.info("allocateWithTO called..");
				return new ResponseEntity<Boolean>(allocationService.allocateWithPickReference(documentReference,userId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "documentReference : " + documentReference +
		    			                      "userId : " + userId);
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	    	
		}
	    
	    @RequestMapping(value = "/allocateForParts", method = RequestMethod.POST)
		public ResponseEntity<Boolean> allocateForParts(@RequestBody AllocateByPartDTO allocateByPartDTO) throws ParseException {
	    	
	    	try{
				logger.info("allocateForParts called..");
				return new ResponseEntity<Boolean>(allocationService.allocateForParts(allocateByPartDTO.getParts(), allocateByPartDTO.getUserId()), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "parts : " + allocateByPartDTO.getAllParts() +
		    			                      "userId : " + allocateByPartDTO.getUserId());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	    	
		}
	    
	    @RequestMapping(value = "/unAllocate", method = RequestMethod.GET)
		public ResponseEntity<Boolean> unAllocate(@RequestParam("documentReference") String documentReference,@RequestParam("userId") String userId) throws ParseException {
	    	
	    	try{
				logger.info("unAllocate called..");
				return new ResponseEntity<Boolean>(allocationService.unAllocate(documentReference,userId), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("Values : "   +  "documentReference : " + documentReference +
		    			                      "userId : " + userId);
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
	    	
		}
	    
	    @RequestMapping(value = "/transferToSap", method = RequestMethod.GET)
		public ResponseEntity<Boolean> transferToSap(@RequestParam("documentReference") String documentReference,@RequestParam("userId") String userId) throws IOException{
				
				try{
					logger.info("transferToSap called..");
					return new ResponseEntity<Boolean>(true, HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "documentReference : " + documentReference +
			    			                      "userId : " + userId);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
				
		}
	    
	    @RequestMapping(value = "/checkInventory", method = RequestMethod.GET)
		public ResponseEntity<Boolean> checkInventory(@RequestParam("orderBodyId") Long orderBodyId) throws IOException{
				
				try{
					logger.info("transferToSap called..");
					return new ResponseEntity<Boolean>(allocationService.checkInventory(orderBodyId), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "documentReference : " + orderBodyId);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
				
		}
	    
	   
	    
	    

}
