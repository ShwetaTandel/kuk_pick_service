package com.vantec.pick.controller;

import java.io.IOException;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.pick.service.DispatchService;
import com.vantec.pick.service.MessageService;

@RestController
@RequestMapping("/")
public class DispatchController {
	
	
	@Autowired
	DispatchService dispatchService;
	
	@Autowired
	MessageService messageService; ;
	
	private static Logger logger = LoggerFactory.getLogger(DispatchController.class);
	    
	  @RequestMapping(value = "/closeTrain", method = RequestMethod.GET)
		public ResponseEntity<String> closeTrain(@RequestParam("trainLocation") String trainLocation ,@RequestParam("userId") String userId) throws ParseException, IOException {
				
				
				try{
					logger.info("closeTrain called..");
					return new ResponseEntity<String>(dispatchService.closeTrain(trainLocation,  userId), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "trainLocation : " + trainLocation +
			    			                      
			    			                      "userId : " + userId);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
				
	  }
	  
	  @RequestMapping(value = "/transmitPickMessage", method = RequestMethod.GET)
		public ResponseEntity<Boolean> transmitPickMessage(@RequestParam("documentReference") String documentRef ,@RequestParam("userId") String userId) throws ParseException, IOException {
				
				
				try{
					logger.info("transmitPickMessage called.. from screen");
					return new ResponseEntity<Boolean>(messageService.sendPickedMessage(documentRef, userId), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "documentRef : " + documentRef +
			    			                      
			    			                      "userId : " + userId);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
				
	  }

}
