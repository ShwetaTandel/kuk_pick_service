package com.vantec.pick.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.pick.model.ReplenDTO;
import com.vantec.pick.model.ReplenResponseDTO;
import com.vantec.pick.model.ReplenScanDTO;
import com.vantec.pick.service.ReplenService;

@RestController
@RequestMapping("/")
public class ReplenController {
	
	private static Logger logger = LoggerFactory.getLogger(ReplenController.class);
	
	
	@Autowired
	ReplenService replenService;
	
	    @RequestMapping(value = "/createReplen", method = RequestMethod.POST)
		public ResponseEntity<Boolean> createReplen(@RequestBody ReplenDTO replenDTO) throws IOException {
				
				try{
					logger.info("createReplen called..");
					return new ResponseEntity<Boolean>(replenService.createReplen(replenDTO), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("ReplenDTO Values : "   +  "locationCode : " + replenDTO.getLocationCode() +
			    			                                "partNumber : " + replenDTO.getPartNumber() +
			    			                                "userId : " + replenDTO.getUserId());
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    @RequestMapping(value = "/editReplen", method = RequestMethod.POST)
		public ResponseEntity<Boolean> editReplen(@RequestBody ReplenDTO replenDTO) throws IOException {
				
				
				try{
					logger.info("editReplen called..");
					return new ResponseEntity<Boolean>(replenService.editReplen(replenDTO), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("ReplenDTO Values : "   +  "PickGroup : " + replenDTO.getPickGroupId() +
			    			                                "ReplenTaskId : " + replenDTO.getReplenTaskId() +
			    			                                "userId : " + replenDTO.getUserId());
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
				
		}
	    
	    @RequestMapping(value = "/deleteReplen", method = RequestMethod.POST)
		public ResponseEntity<Boolean> deleteReplen(@RequestBody ReplenDTO replenDTO) throws IOException {
				
				try{
					logger.info("deleteReplen called..");
					return new ResponseEntity<Boolean>(replenService.deleteReplen(replenDTO), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("ReplenDTO Values : "   +  "PickGroup : " + replenDTO.getPickGroupId() +
			    			                                "ReplenTaskId : " + replenDTO.getReplenTaskId() +
			    			                                "userId : " + replenDTO.getUserId());
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    @RequestMapping(value = "/findReplenTask", method = RequestMethod.POST)
		public ResponseEntity<ReplenResponseDTO> findReplenTask(@RequestParam String replenGroupCode,@RequestParam String userId) throws IOException {
	    	
				
				try{
					logger.info("findReplenTask called..");
					return new ResponseEntity<ReplenResponseDTO>(replenService.findReplenTask(replenGroupCode,userId), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +  "replenGroupCode : " + replenGroupCode +
			    			                                "userId : " + userId);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<ReplenResponseDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    @RequestMapping(value = "/createPallet", method = RequestMethod.POST)
		public ResponseEntity<String> createPallet(@RequestParam String userId) throws IOException {
				
				try{
					logger.info("createPallet called..");
					return new ResponseEntity<>(replenService.createPallet(userId), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +   "userId : " + userId);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
//	    @RequestMapping(value = "/updateReplenWithScannedSerial", method = RequestMethod.POST)
//		public ResponseEntity<Boolean> updateReplenWithScannedSerial(@RequestParam String tagReference,@RequestParam String serial,@RequestParam Long replenTaskId) throws IOException {
//				return new ResponseEntity<Boolean>(replenService.updateReplenWithScannedSerial(tagReference,serial,replenTaskId), HttpStatus.OK);
//		}
	    
	    @RequestMapping(value = "/getToLocationCodeForSerial", method = RequestMethod.POST)
		public ResponseEntity<String> getToLocationCodeForSerial(@RequestParam String serial,@RequestParam String partNumber) throws IOException {
				
				try{
					logger.info("getToLocationCodeForSerial called..");
					return new ResponseEntity<String>(replenService.getToLocationCodeForSerial(serial,partNumber), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +   "serial : " + serial +
			    			                       "partNumber : " + partNumber);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    @RequestMapping(value = "/validateScannedSerail", method = RequestMethod.POST)
		public ResponseEntity<Boolean> validateScannedSerail(@RequestParam String serial,@RequestParam String partNumber,@RequestParam String locationCode) throws IOException {
				
				try{
					logger.info("validateScannedSerail called..");
					return new ResponseEntity<Boolean>(replenService.validateScannedSerail(serial,partNumber,locationCode), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +   "serial : " + serial +
			    			                       "locationCode : " + locationCode +
			    			                       "partNumber : " + partNumber);
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    
	    @RequestMapping(value = "/scanReplen", method = RequestMethod.POST)
		public ResponseEntity<String> scanReplen(@RequestBody ReplenScanDTO replenScanDTO) throws IOException {
	    	
	    	try{
				logger.info("scanReplen called.. " + "UserId : " + replenScanDTO.getUserId());
				return new ResponseEntity<String>(replenService.scanReplen(replenScanDTO), HttpStatus.OK);
		    }catch(Exception ex){
		    	logger.error("ReplenScanDTO Values : "   +   "LocationCode : " + replenScanDTO.getLocationCode() +
		    			                       "ScannedSerial : " + replenScanDTO.getScannedSerial() +
		    			                       "SuggestedSerial : " + replenScanDTO.getSuggestedSerial() +
		    			                       "TagReference : " + replenScanDTO.getTagReference() +
		    			                       "UserId : " + replenScanDTO.getUserId() +
		    			                       "PickGroupId : " + replenScanDTO.getPickGroupId() +
		    			                       "ReplenTaskId : " + replenScanDTO.getReplenTaskId());
		    	logger.error(ex.getMessage(), ex);
		    	return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
				
		}
	    
	    @RequestMapping(value = "/transferReplen", method = RequestMethod.POST)
		public ResponseEntity<Boolean> transferReplen(@RequestParam Long replenTaskId,@RequestParam String userId) throws IOException {
				
		    	try{
					logger.info("transferReplen called..");
					return new ResponseEntity<Boolean>(replenService.transferReplen(replenTaskId,userId), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +   "replenTaskId : " + replenTaskId +
			    			                                     "userId : " + userId );
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    @RequestMapping(value = "/processedReplen", method = RequestMethod.POST)
		public ResponseEntity<Boolean> processedReplen(@RequestParam String serial,@RequestParam String partNumber,@RequestParam String userId,@RequestParam String locationCode) throws IOException {
	    	   
				
				try{
					logger.info("processedReplen called..");
					return new ResponseEntity<Boolean>(replenService.processedReplen(serial,partNumber,userId,locationCode), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +   "serial : " + serial +
			    			                                     "partNumber : " + partNumber + 
			    			                                     "userId : " + userId +
			    			                                     "locationCode : " + locationCode );
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    @RequestMapping(value = "/f6Replen", method = RequestMethod.POST)
		public ResponseEntity<ReplenResponseDTO> f6Replen(@RequestParam Long replenTaskId,@RequestParam String userId) throws IOException {
				
				try{
					logger.info("f6Replen called..");
					return new ResponseEntity<ReplenResponseDTO>(replenService.f6Replen(replenTaskId,userId), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("Values : "   +   "replenTaskId : " + replenTaskId +
			    			                                     "userId : " + userId );
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<ReplenResponseDTO>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
				
		}
	    
	    @RequestMapping(value = "/f6Pick", method = RequestMethod.POST)
		public ResponseEntity<Boolean> f6Pick(@RequestBody ReplenDTO replenDTO) throws IOException {
	    	
				
				try{
					logger.info("f6Pick called..");
					return new ResponseEntity<Boolean>(replenService.f6Pick(replenDTO), HttpStatus.OK);
			    }catch(Exception ex){
			    	logger.error("ReplenDTO Values : "   +   "serialReference : " + replenDTO.getSerialReference() +
			    			                                     "userId : " + replenDTO.getUserId() );
			    	logger.error(ex.getMessage(), ex);
			    	return new ResponseEntity<Boolean>(HttpStatus.INTERNAL_SERVER_ERROR);
			    }
		}
	    
	    

}
