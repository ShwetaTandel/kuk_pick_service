package com.vantec.pick.util;

public interface ConstantHelper {
	
	public static final String NOT_EXIST_PART_NUMBER = "notexist";
	public static final String EMPTY= "";
	/**Order TYPE Constants table constants**/
	public static final String NOT_FOUND = "NOTFOUND";
	public static final String NO_RAN = "NO_RAN";
	public static final String FAB_ORDER_TYPE = "FAB";
	public static final String SPARES_ORDER_TYPE = "SPARES";
	public static final String INSPECT_ORDER_TYPE = "INSPECT";
	public static final String MIXED_ORDER_TYPE = "MIXED";
	public static final String AAA_ORDER_TYPE = "AAA";
	public static final String SPARES_ORDER_PREFIX = "X";
	public static final String FAB_ORDER_PREFIX = "M";
	
	
	
	public static final String MARSHALLING_LOCATION_TYPE_CODE = "MA";
	
	public static final String DROP_ZONE_LOCATION_TYPE_CODE = "DZ";
	public static final String PICK_INTRANSIT_LOCATION_CODE = "INTRAN";
	public static final String TRAIN_LOCATION_CODE = "TRAIN";
	
	public static final String PI_REASON_CODE_F8_MISSING = "F8_MISSING";
	
	
	public static final String TRANSACTION_CODE_PICK_TO_TROLLEY = "PBTP";
	public static final String TRANSACTION_CODE_PICK_TO_MARSHALL = "PBTM";
	public static final String TRANSACTION_CODE_PALLET_TO_TRAIN = "PTTT";
	public static final String TRANSACTION_CODE_MARSHAL_TO_TRAILER = "MTT";
	public static final String TRANSACTION_CODE_SHIP = "SHIP";
	public static final String TRANSACTION_CODE_MODULAR_MOVE_PICK = "MODMVP";
	
	
	public static final String TRANSACTION_CODE_SKIP = "F2SKIP";
	public static final String AUTO_PICK = "AUTO_PICK";
	public static final String MARSHALLING_LOCATION = "PTZ01";
	
	

	

	/**Document status table constants**/
	public static final String DOCUMENT_STATUS_COMPLETED = "COMPLETE";
	public static final String DOCUMENT_STATUS_TRANSMITTED = "TRANSMITTED";
	public static final String DOCUMENT_STATUS_ALLOCATED = "ALLOCATED";
	public static final String DOCUMENT_STATUS_QUARANTINE = "QUARANTINE";
	public static final String DOCUMENT_STATUS_OPEN = "OPEN";
	public static final String DOCUMENT_STATUS_DELETED = "DELETED"; 
	public static final String DOCUMENT_STATUS_CLOSED =  "CLOSED";
	public static final String DOCUMENT_STATUS_PICKED =  "PICKED";
	
	public static final String INVENTORY_STATUS_MISSING =  "MISSING";
	public static final String INVENTORY_STATUS_PICKED =  "PKD";
	

	/**Inventory status table constants**/
	public static final String INVENTORY_STATUS_OK = "OK";
	
	/**KUK  constants**/
	public static final String KUK_VENDOR_CODE = "KUK";
	
	/**PICK FILE CONSTANTS**/
	public static final String BAAN_FILE_START_FILE_RECORD = "Start of file";
	public static final String BAAN_FILE_END_FILE_RECORD = "End of file";

	public static final String PICKFILE1 = "PIK";
	public static final String PICKFILE2 = "TOPICK";
	public static final String PICK_ADD_INDICATOR = "A";
	public static final String PICK_CHANGE_INDICATOR = "C";
	public static final String PICK_DELETE_INDICATOR = "D";
	public static final String BAAN_FILE_SIMPLE_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
	public static final String BAAN_FILE_SIMPLE_DATE_FORMAT = "dd/MM/yyyy";
	
	public static final String MANULI_LOC_SUB_TYPE_CODE = "MH";
	
	
	
	/**General  constants**/
	public static final String SYSUSER = "SYS";
	public static final String PIK_FILE_JOB = "PIK File Job";
	public static final String TMP_FILE_EXT = ".tmp";
	public static final String TEXT_FILE_EXT = ".txt";
	
	/**RTS Constants*/
	
	public static final String REWORK_RTS = "REWORK-RTS";
	public static final String REWORK_EMERG_RTS = "REWORK-EMERG-RTS";
	public static final String EXPEDITE_RTS = "EXPEDITE-RTS";
	public static final String EXPEDITE_EMERG_RTS = "EXPEDITE-EMERG-RTS";
	
	public static final String PICKED_FILE_TYPE = "PICKED";
	public static final String CANCEL_FILE_TYPE = "CANCEL";
	
	

}
