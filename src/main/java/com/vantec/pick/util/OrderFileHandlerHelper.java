package com.vantec.pick.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.vantec.pick.entity.PickBody;
import com.vantec.pick.model.OrderBodyDTO;
import com.vantec.pick.model.OrderHeaderDTO;

public final class OrderFileHandlerHelper {
	
	private static String[] splitLine(String line) {
		// Split Line at |
		line = line.replaceAll("\"", "");
		line = line.concat("|End");
		String[] values = line.split("\\|");
		return values;
	}

	public static void main(String args[]) throws IOException {
		// File file = new File("C://Users//tagrawal//Desktop//Format PCK.txt");
		// List<DocumentHeaderObject> orderObjects =
		// OrderFileHandlerHelper.prepareDocument(file);
		
		System.out.println(new SimpleDateFormat("ddMMyyyy").format(new Date()));
		String d = "CCDM";
		System.out.println(d.substring(0,2));
		String s = "PCK001360 - TR5-AD";
		String[] a = s.split(" - ");
		System.out.println(a[0] + "="+a[1]);
		List<PickBody> shortages = new ArrayList<PickBody>();
		Double qty =4.0;
		PickBody p = new PickBody();
		p.setQtyAllocated(5d);
		p.setQtyExpected(18d);
		p.setQtyTransacted(4d);
		p.setQtyShortage(14d);
		if(qty > p.getQtyShortage()){
			System.out.println("less");
		}else{
			System.out.println("more");
			p.setQtyTransacted(p.getQtyTransacted().doubleValue() + qty);
			System.out.println(p.getQtyTransacted());
			
		}
		p.setQtyShortage(p.getQtyExpected() - p.getQtyTransacted());
		System.out.println(p.getQtyShortage());
		shortages.add(p);
		PickBody q = new PickBody();
		q.setQtyAllocated(0d);
		q.setQtyExpected(5d);
		q.setQtyTransacted(0d);
		q.setQtyShortage(5d);
		shortages.add(q);
		List<PickBody> allocatedList = new ArrayList<PickBody>();
		PickBody p1 = new PickBody();
		p1.setQtyExpected(1d);
		p1.setQtyAllocated(1d);
		p1.setQtyTransacted(0d);
		p1.setQtyShortage(0d);
		allocatedList.add(p1);
		PickBody p2 = new PickBody();
		p2.setQtyExpected(2d);
		p2.setQtyAllocated(2d);
		p2.setQtyTransacted(0d);
		p2.setQtyShortage(0d);
		allocatedList.add(p2);
		
		
		if( shortages!=null && shortages.size() > 0){
			for(PickBody shortBody: shortages){
				//find existing picks for parts
				double shortageQty = shortBody.getQtyShortage().doubleValue();
					for(PickBody allocateBody : allocatedList){
						if(shortageQty <=0){
							System.out.println("Broke offf");
							break;
						}
						if(allocateBody.getQtyAllocated() >0){
						if(shortageQty <= allocateBody.getQtyAllocated()){
							shortBody.setQtyAllocated(shortBody.getQtyAllocated() + shortageQty);
							shortBody.setQtyShortage(0d);
						}else if(shortageQty > allocateBody.getQtyAllocated()){
							shortBody.setQtyAllocated(shortBody.getQtyAllocated() + allocateBody.getQtyAllocated());
							allocateBody.setQtyAllocated(0d);
						}
						shortBody.setQtyShortage(shortBody.getQtyExpected().doubleValue() - shortBody.getQtyTransacted().doubleValue() - shortBody.getQtyAllocated().doubleValue());
						allocateBody.setQtyShortage(allocateBody.getQtyExpected().doubleValue() - allocateBody.getQtyTransacted().doubleValue() - allocateBody.getQtyAllocated().doubleValue());
						shortageQty-=shortBody.getQtyShortage();
					}
						
					System.out.println(shortBody.getPartNumber() + " "+ shortBody.getQtyExpected() + "  "+shortBody.getQtyAllocated() + "  "+shortBody.getQtyTransacted()+ " "+shortBody.getQtyShortage());
			}
			
			}
			}

	}

	/***
	 * Read pick file
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 * @throws InvalidDataException 
	 */
	public static List<OrderHeaderDTO> readPIKFile(File file) throws IOException, ParseException {

		// Sample Lines
		// A|P|4|AD0003606|256|0102360510|6|SA|K77042|PC290NLCI|22/03/2021
		// 08:08:01|PC290NLCI-11E0
		// Add Indicator|?|?|Customer
		// Ref|LineNo|PartNumber|QTY|ZONE-Destination|BAAN-Serail|Machine
		// Model|Expected Delivery Time|?
		//D|P|4|AC0022036|0||0|||
		//D|P|4|AC0022383|0||0|||
		FileReader fileReader = new FileReader(file);
		BufferedReader lines = new BufferedReader(fileReader);
		String line = "";

		List<OrderHeaderDTO> orderHeaderDTOs = new ArrayList<OrderHeaderDTO>();
		OrderHeaderDTO orderHeaderDTO = null;
		OrderBodyDTO orderBodyDTO = null;
		List<String> orders = new ArrayList<>();
		while ((line = lines.readLine()) != null) {

			String[] values = splitLine(line);

			if (values[0].equalsIgnoreCase(ConstantHelper.PICK_ADD_INDICATOR)) {
				//First check for order type 
				
				if (orders.contains(values[3]) == false) {
					orderHeaderDTO = new OrderHeaderDTO();
					orderHeaderDTO.setTypeIndicator(ConstantHelper.PICK_ADD_INDICATOR);
					orderHeaderDTO.setOrigin(values[2]);
					orderHeaderDTO.setCustomerReference(values[3]);
					orderHeaderDTO.setBaanSerial(values[8]);
					orderHeaderDTO.setMachineModel(values[9]);
					orders.add(values[3]);
					orderHeaderDTOs.add(orderHeaderDTO);
				}
				orderBodyDTO = new OrderBodyDTO();
				if (!values[10].isEmpty() && values[10].length() > 11) {
					orderBodyDTO.setExpectedDeliveryDate(
							new SimpleDateFormat(ConstantHelper.BAAN_FILE_SIMPLE_DATE_TIME_FORMAT)
									.parse(values[10]));
				} else if (!values[10].isEmpty()) {
					orderBodyDTO.setExpectedDeliveryDate(
							new SimpleDateFormat(ConstantHelper.BAAN_FILE_SIMPLE_DATE_FORMAT).parse(values[10]));
				}
				orderBodyDTO.setOrderLine(Integer.parseInt(values[4]));
				orderBodyDTO.setPartNumber(values[5]);
				orderBodyDTO.setExpectedQty(Double.valueOf(values[6]));
				orderBodyDTO.setZoneDestination(values[7]);
				orderHeaderDTO.getOrderBodyDTOs().add(orderBodyDTO);

			}else if(values[0].equalsIgnoreCase(ConstantHelper.PICK_CHANGE_INDICATOR)){
				if (orders.contains(values[3]) == false) {
					orderHeaderDTO = new OrderHeaderDTO();
					orderHeaderDTO.setOrigin(values[2]);
					orderHeaderDTO.setTypeIndicator(ConstantHelper.PICK_CHANGE_INDICATOR);
					orderHeaderDTO.setCustomerReference(values[3]);
					orders.add(values[3]);
					orderHeaderDTOs.add(orderHeaderDTO);
				}
				orderBodyDTO = new OrderBodyDTO();
				orderBodyDTO.setOrderLine(Integer.parseInt(values[4]));
				orderBodyDTO.setPartNumber(values[5]);
				orderBodyDTO.setExpectedQty(Double.valueOf(values[6]));
				orderHeaderDTO.getOrderBodyDTOs().add(orderBodyDTO);

			}else if(values[0].equalsIgnoreCase(ConstantHelper.PICK_DELETE_INDICATOR)){
				if (orders.contains(values[3]) == false) {
					orderHeaderDTO = new OrderHeaderDTO();
					orderHeaderDTO.setOrigin(values[2]);
					orderHeaderDTO.setTypeIndicator(ConstantHelper.PICK_DELETE_INDICATOR);
					orderHeaderDTO.setCustomerReference(values[3]);
					orders.add(values[3]);
					orderHeaderDTOs.add(orderHeaderDTO);
				}
				orderBodyDTO = new OrderBodyDTO();
				orderBodyDTO.setOrderLine(Integer.parseInt(values[4]));
				orderBodyDTO.setExpectedQty(Double.valueOf(values[6]));
				orderHeaderDTO.getOrderBodyDTOs().add(orderBodyDTO);

			}
		}

		fileReader.close();
		lines.close();
		return orderHeaderDTOs;

	}
	

}
