package com.vantec.pick.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.vantec.pick.entity.DocumentBody;
import com.vantec.pick.entity.DocumentHeader;
import com.vantec.pick.model.DocumentBodyObject;
import com.vantec.pick.model.DocumentHeaderObject;
import com.vantec.pick.repository.PartRepository;

public final class OrderServiceHelper {
	
	@Autowired
	PartRepository partRepository;
	
	
	
	public static DocumentHeader convertViewToEntity(DocumentHeaderObject headerObject) throws ParseException {
		DocumentHeader orderHeader = new DocumentHeader();
		List<DocumentBody> orderBodies = new ArrayList<DocumentBody>();
		DocumentBody orderBody = null;
		
		List<DocumentBodyObject> bodyObjects  = headerObject.getBodies();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//Date expectDeliveryDate = sdf.parse(customerOrder.getExpectedDate());
		SimpleDateFormat tf = new SimpleDateFormat("HH:mm:ss");
		//Date expectDeliveryTime = sdf.parse(customerOrder.getExpectedDate());
		
		//Header
		orderHeader.setCustomerReferenceCode(headerObject.getTanum());
		orderHeader.setTanum(headerObject.getTanum());
		orderHeader.setExpectedDelivery(new Date());
		orderHeader.setBwlvs(headerObject.getBwlvs());
		orderHeader.setDateCreated(new Date());
		orderHeader.setUpdatedBy("sys");
		orderHeader.setDateUpdated(new Date());
		
		
		//Body
		if(bodyObjects != null){
			for(DocumentBodyObject bodyObject : bodyObjects) {
				orderBody = new DocumentBody();
				//orderBody.setPart(partRepository.fi
				//orderBody.setQtyExpected(bodyObject.getExpectedQty());
				orderBody.setQtyTransacted(0);
				orderBody.setOurLineNo(bodyObject.getOrderLine());
				orderBody.setDateCreated(new Date());
				orderBody.setUpdatedBy("sys");
				orderBody.setDateUpdated(new Date());
				orderBodies.add(orderBody);
		    	}
			    orderHeader.setDocumentBodies(orderBodies);
		    }
	    
		
		
		return orderHeader;
		}

}
