package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.LocationType;




public interface LocationTypeRepository extends JpaRepository<LocationType, Serializable>{
	
	LocationType findById(Long id);
	LocationType findByLocationTypeCode(String code);
	
}
