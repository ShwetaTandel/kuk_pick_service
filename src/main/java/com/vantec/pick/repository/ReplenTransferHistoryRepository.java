package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.ReplenTask;
import com.vantec.pick.entity.ReplenTransferHistory;

public interface ReplenTransferHistoryRepository extends JpaRepository<ReplenTransferHistory, Serializable>{
	
}

