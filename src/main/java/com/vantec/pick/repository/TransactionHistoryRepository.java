package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.TransactionHistory;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory , Serializable>{
}

