package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.Part;

public interface OrderBodyRepository extends JpaRepository<OrderBody, Serializable>{
	
	OrderBody findById(Long id);

	List<OrderBody> findByOrderHeader(OrderHeader orderHeader);
	OrderBody findByOrderHeaderAndLineNo(OrderHeader orderHeader,Integer lineNo);
	
	OrderBody findByOrderHeaderAndPart(OrderHeader orderHeader,Part part);
	
	OrderBody findByPartNumberAndLineNoAndCustomerReference(String partNumber, Integer lineNo, String custRef);
	
	List<OrderBody> findByLineNoAndCustomerReference(Integer lineNo, String custRef);
	List<OrderBody> findByCustomerReference(String custRef);
	
	@Query("SELECT c.partNumber, SUM(c.qtyExpected), c.part, c.zoneDestination FROM OrderBody c where c.orderHeader in(:orderHeaders) GROUP BY c.partNumber, c.zoneDestination")
	List<Object[]>  findBodiesGroupByPartAndZoneForOrderHeaders(@Param("orderHeaders")List<OrderHeader> orderHeaders);
	
	@Query("SELECT c FROM OrderBody c where c.orderHeader in(:orderHeaders) and c.qtyTransacted = c.qtyExpected and c.documentStatus =:documentStatus")
	List<OrderBody>  findAllPickedBodiesForHeaders(@Param("orderHeaders")List<OrderHeader> orderHeaders, @Param("documentStatus")DocumentStatus documentStatus);
	
	//@Query("SELECT c FROM OrderBody c where c.orderHeader.pickHeader.id =:pickId and c.difference > 0")
	@Query(value = "Select * from order_body join order_header on order_header.id = order_body.order_header_id where pick_header_id= :pickId and difference > 0 and part_number in (Select part_number from allocatable)", nativeQuery = true)
	List<OrderBody>  findAllOpenBodies(@Param("pickId")Long pickId);
	
	@Query(value = "Select * from order_body join order_header on order_header.id = order_body.order_header_id where pick_header_id= :pickId and difference > 0 and part_number in (:parts) and part_number in (Select part_number from allocatable)", nativeQuery = true)
	List<OrderBody>  findAllOpenBodiesForParts(@Param("pickId")Long pickId, @Param("parts")List<String> parts);
	
	@Query(value = "Select * from order_body join order_header on order_header.id = order_body.order_header_id join part on order_body.part_number = part.part_number join location_type on part.fixed_location_type_id = location_type.id where  pick_header_id= :pickId and difference > 0 and task_group in (:tasks) and order_body.part_number in (Select part_number from allocatable)", nativeQuery = true)
	List<OrderBody>  findAllOpenBodiesForPickTasks(@Param("pickId")Long pickId, @Param("tasks")List<String> tasks);

	
	@Query("SELECT count(c) FROM OrderBody c where c.orderHeader =:orderHeader and c.documentStatus =:documentStatus")
	Integer  findCountOfOpenBodies(@Param("orderHeader")OrderHeader orderHeader, @Param("documentStatus")DocumentStatus documentStatus);

	
}

