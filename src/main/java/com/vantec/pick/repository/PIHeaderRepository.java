package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.PIHeader;

public interface PIHeaderRepository extends JpaRepository<PIHeader, Serializable>{

	PIHeader findByLocationCodeAndDocumentStatus(String locationCode,DocumentStatus docStatus);
	
	PIHeader findByPartNumberAndDocumentStatus(String partNumber,DocumentStatus docStatus);
	
	PIHeader findById(Long id);
	
}

