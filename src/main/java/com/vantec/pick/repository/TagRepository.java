package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.DocumentHeader;
import com.vantec.pick.entity.Tag;

public interface TagRepository extends JpaRepository<Tag, Serializable>{

	List<Tag> findByDocumentHeader(DocumentHeader orderHeader);
	
	Tag findByTagReference(String tagReference);
	
	@Query("select t from Tag t where tagReference in (select distinct tagReference from InventoryMaster where locationCode = :locationCode))")
	List<Tag> selectTagAfterDispatch(@Param("locationCode") String locationCode);
	
	
}

