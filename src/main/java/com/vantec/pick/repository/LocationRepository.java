package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.Location;

public interface LocationRepository extends JpaRepository<Location, Serializable>{

	Location findById(Long id);
	
	Location findByLocationCode(String locationCode);
	
	//Location findByHashCode(String hashCode);
	
}

