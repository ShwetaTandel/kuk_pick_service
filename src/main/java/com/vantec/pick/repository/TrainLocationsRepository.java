package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.TrainLocations;

public interface TrainLocationsRepository extends JpaRepository<TrainLocations, Serializable>{
	
	TrainLocations findByTrainLocationCode(String trainLocationCode);
	
	@Query("SELECT a FROM TrainLocations a where (a.inUseBy is null or a.inUseBy =:userId or a.dateUpdated <=:dateUpdated ) and a.trainLocationCode=:trainLocationCode")
	TrainLocations findByTrainLocation(@Param("trainLocationCode")String trainLocationCode, @Param("dateUpdated")Date dateUpdated, @Param("userId")String userId);
	
	//and SUBSTRING_INDEX(a.trainLocationCode,'-', -1)=:train
	@Query("SELECT a FROM TrainLocations a where (a.inUseBy is null or a.inUseBy =:userId or a.dateUpdated <=:dateUpdated ) and a.trainLocationCode like :pickRef")
	List<TrainLocations> findByPickRefAndTrain(@Param("pickRef")String pickRef, @Param("dateUpdated")Date dateUpdated, @Param("userId")String userId);
	
	
	
}

