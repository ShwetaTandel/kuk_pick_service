package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.entity.Part;
import com.vantec.pick.entity.PickBody;
import com.vantec.pick.entity.PickHeader;

public interface PickBodyRepository extends JpaRepository<PickBody, Serializable>{

	PickBody findById(Long id);
	
	List<PickBody> findAllByPickHeaderAndProcessed(PickHeader pickHeader, Boolean processed);
	
	PickBody findByPickHeaderAndPart(PickHeader pickHeader, Part part);
	PickBody findByPickHeaderAndPartNumberAndZoneDestinationAndProcessed(PickHeader pickHeader, String partNumber,String zoneDestination, Boolean processed);
	PickBody findByPickHeaderAndPartNumberAndZoneDestination(PickHeader pickHeader, String partNumber,String zoneDestination);
	
	//Check for Task Group instead of location Type code -- Geoff suggested this change to avoid hyphens in RDT
	//(a.inUseBy is null or a.inUseBy =:userId or a.dateUpdated <=:dateUpdated ) and
	@Query("SELECT a FROM PickBody a join a.part p  join p.fixedLocationType l where a.pickHeader=:pickHeader and  a.processed=false and a.qtyAllocated > 0 and l.taskGroup =:pickTask")
	List<PickBody> findAvailableBodiesByHeaderAndPickTask(@Param("pickHeader") PickHeader pickHeader, @Param("pickTask") String pickTask);
	
	@Query("SELECT a FROM PickBody a join a.part p  join p.fixedLocationType l where a.pickHeader=:pickHeader and  a.processed=false and a.qtyAllocated > 0 and l.taskGroup =:pickTask and a.zoneDestination in(:zones)")
	List<PickBody> findAvailableBodiesByHeaderAndPickTaskAndZone(@Param("pickHeader") PickHeader pickHeader, @Param("pickTask") String pickTask, @Param("zones") List<String> zones);
	
	
	//(a.inUseBy is null or a.inUseBy =:userId or a.dateUpdated <=:dateUpdated ) and
	@Query("SELECT a FROM PickBody a join a.part p  join p.fixedLocationType l where a.pickHeader=:pickHeader and  a.processed=false and l.locationTypeCode =:pickTask")
	List<PickBody> findBodiesByHeaderAndPickTask(@Param("pickHeader") PickHeader pickHeader, @Param("pickTask") String pickTask);
	
	
	//@Query("Select count(p) from PickBody p where p.pickHeader = :pickHeader and p.qtyShortage > 0 or p.qtyExpected < (p.qtyAllocated + p.qtyTransacted)")
	@Query("Select count(p) from PickBody p where p.pickHeader = :pickHeader and p.qtyShortage > 0")
    Integer checkIfShortagesForPickHeader(@Param("pickHeader") PickHeader pickHeader);
	//PickBody findByPickHeaderAndPartNumberAndLineNo(PickHeader pickHeader, String partNumber, Integer lineNo);
	
	@Query("Select p from PickBody p where p.pickHeader = :pickHeader and p.qtyShortage > 0")
    List<PickBody> findShortageBodiesByHeader(@Param("pickHeader") PickHeader pickHeader);
	
	@Query("Select count(a) from PickBody a join a.part p  join p.fixedLocationType l where a.pickHeader = :pickHeader and a.qtyShortage > 0 and l.taskGroup in (:tasks)")
	 Integer findShortageBodiesByHeaderAndPickTask(@Param("pickHeader") PickHeader pickHeader,@Param("tasks") List<String> tasks );
	
	
	@Query("Select p from PickBody p where p.pickHeader != :pickHeader and p.qtyAllocated > 0 and p.processed=false and p.partNumber =:partNumber and (p.inUseBy is null or p.inUseBy =:userId or p.dateUpdated <=:dateUpdated ) order by id desc")
    List<PickBody> findAllocatedPart(@Param("pickHeader") PickHeader pickHeader, @Param("partNumber") String partNumber ,@Param("userId") String userId , @Param("dateUpdated") Date dateUpdated);
	
	@Transactional
	@Modifying
	@Query("UPDATE PickBody p SET p.processed = :processed , p.updatedBy = :updatedBy , p.dateUpdated = :dateUpdated where p.documentReference = :documentReference")
    Integer updateByProcessed(@Param("processed") Boolean processed,@Param("documentReference") String documentReference,@Param("updatedBy") String updatedBy,@Param("dateUpdated") Date dateUpdated);

	List<PickBody> findByPickHeader(PickHeader pickHeader);
	

	@Query("SELECT distinct p.zoneDestination FROM PickBody p where p.pickHeader = :pickHeader ")
	List<String>  findDistinctZoneDestinationsForPickHeader(@Param("pickHeader")PickHeader pickHeader);
	
	@Query("SELECT p FROM PickBody p where (p.inUseBy is null or p.inUseBy =:userId or p.dateUpdated <=:dateUpdated ) and p.documentReference=:documentReference and  p.processed=false and p.zoneDestination in (:zones)")
	List<PickBody> findUnProcessedBodiesByPickReferenceForTrain(@Param("documentReference") String documentReference, @Param("userId") String userId   , @Param("dateUpdated") Date dateUpdated, @Param("zones") List<String> zones);
	
	@Query(value = "select distinct pick_header.document_reference from pick_body join pick_header on pick_header.id = pick_body.pick_header_id   where qty_shortage > 0 and  pick_type = 'AAA' and part_number in (:parts) union select distinct pick_header.document_reference from pick_body join pick_header on pick_header.id = pick_body.pick_header_id   where qty_shortage > 0 and  pick_type is not null and part_number in (:parts)", nativeQuery = true)
    List<String> checkShortagesForOrders(@Param("parts") List<String> parts);
	
	@Query("SELECT count(c) FROM PickBody c where c.pickHeader =:pickHeader and c.processed = false")
	Integer  findCountOfOpenBodies(@Param("pickHeader")PickHeader pickHeader);
	
}

