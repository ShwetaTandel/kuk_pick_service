package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, Serializable>{

	Vendor findByVendorReferenceCode(String vendorReferenceCode);
	
	
}

