package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.DocumentHeader;

public interface DocumentHeaderRepository extends JpaRepository<DocumentHeader, Serializable>{

	DocumentHeader findByDocumentReferenceCode(String findByDocumentReferenceCode);
	
	DocumentHeader findByRequirementHeaderId(DocumentHeader customerOrder);
	
	DocumentHeader findByTanum(String tanum);
	
	DocumentHeader findById(Long id);

	DocumentHeader findByCustomerReferenceCode(String customerReferenceCode);
	
}

