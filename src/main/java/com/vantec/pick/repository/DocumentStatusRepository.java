package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.DocumentStatus;

public interface DocumentStatusRepository extends JpaRepository<DocumentStatus, Serializable>{

	DocumentStatus findByDocumentStatusCode(String documentStatusCode);
	
}

