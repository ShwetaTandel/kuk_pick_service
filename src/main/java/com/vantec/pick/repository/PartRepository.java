package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.Part;

public interface PartRepository extends JpaRepository<Part, Serializable>{

	Part findById(Long id);
	
	Part findByPartNumber(String partNumber);
	
	@Query("SELECT p  FROM Part p WHERE p.partNumber LIKE CONCAT('%',:partNumber,'%')")
	List<Part> findByPartNumberLike(@Param("partNumber") String partNumber);
	
	
}

