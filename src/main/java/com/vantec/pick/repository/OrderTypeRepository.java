package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.OrderType;

public interface OrderTypeRepository extends JpaRepository<OrderType, Serializable>{
	
	OrderType findByOrderTypePrefix(String orderTypePrefix);
	
	@Query("select orderTypePrefix from OrderType t where t.orderTypeCode <> 'INSPECT' and t.orderTypePrefix is not null and t.autoAllocate =true")
    List<String> findPrefixesForAutoAllocateOrders();
	
	@Query("select orderTypePrefix from OrderType t where t.orderTypeCode <> 'INSPECT' and t.orderTypePrefix is not null")
    List<String> findPrefixesForOrders();
	
	@Query(value = "select * from order_type where order_type_code= :orderTypeCode limit 1", nativeQuery = true)
	OrderType findByOrderTypeCode(@Param("orderTypeCode")  String orderTypeCode);
 
	
}

