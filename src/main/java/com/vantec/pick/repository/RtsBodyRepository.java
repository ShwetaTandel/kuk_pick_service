package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.RtsBody;
import com.vantec.pick.entity.RtsHeader;


public interface RtsBodyRepository extends JpaRepository<RtsBody, Serializable>{

	List<RtsBody> findByRtsHeaderAndPartNumber(RtsHeader rtsHeader, String partNumber);
	
	@Query("select count(t) from RtsBody t where t.documentStatus !=:documentStatus and t.rtsHeader =:rtsHeader")
    Integer findCountOfUnProcessedBodies(@Param("rtsHeader") RtsHeader rtsHeader,@Param("documentStatus") DocumentStatus documentStatus);
	
	
	@Modifying
    @Query("UPDATE RtsBody c SET c.documentStatus = :documentStatus , c.lastUpdatedBy =:lastUpdatedBy, c.lastUpdated = now() WHERE c.rtsHeader = :rtsHeader")
    int updateStatusByHeader(@Param("rtsHeader") RtsHeader rtsHeader, @Param("lastUpdatedBy") String updatedBy,@Param("documentStatus") DocumentStatus documentStatus);
	
	List<RtsBody> findByPartNumberAndDocumentStatus(String partNumber,DocumentStatus documentStatus);



}
