package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.PickGroup;

public interface PickGroupRepository extends JpaRepository<PickGroup, Serializable>{

	PickGroup findById(Long id);
	
	PickGroup findByPickGroupCode(String pickGroupCode);
}

