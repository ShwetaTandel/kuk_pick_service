package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.vantec.pick.entity.UploadFile;

public interface UploadFileRepository extends JpaRepository<UploadFile, Serializable>{
	
	@Query(value="SELECT * FROM upload_file r order by id desc limit 1",nativeQuery = true)
	UploadFile findLatestReturnFile();



}
