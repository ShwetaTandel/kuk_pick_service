package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.DocumentRegister;

public interface DocumentRegisterRepository extends JpaRepository<DocumentRegister, Serializable>{

	DocumentRegister findByDocumentType(String documentType);
	
}

