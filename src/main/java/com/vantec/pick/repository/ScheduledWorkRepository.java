package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.DocumentHeader;
import com.vantec.pick.entity.ScheduledWork;

public interface ScheduledWorkRepository extends JpaRepository<ScheduledWork, Serializable>{

	List<ScheduledWork> findAll();
	
	ScheduledWork findByAssociatedHeader(DocumentHeader documentHeader);
	
}

