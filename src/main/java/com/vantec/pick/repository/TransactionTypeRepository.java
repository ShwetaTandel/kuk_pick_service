package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.TransactionType;

public interface TransactionTypeRepository extends JpaRepository<TransactionType, Serializable>{
	
  TransactionType findByShortCode(String shortCode);  
}

