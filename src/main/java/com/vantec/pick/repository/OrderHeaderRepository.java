package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.PickHeader;

public interface OrderHeaderRepository extends JpaRepository<OrderHeader, Serializable>{
	OrderHeader findById(Long id);

	OrderHeader findByDocumentReference(String docRef);
	
	List<OrderHeader> findByCustomerReference(String customerReference);
	
	List<OrderHeader> findByPickHeader(PickHeader pick);
	
	
	List<OrderHeader> findByCustomerReferenceAndOrderTypeAndExpectedDeliveryTime(String customerReference,String OrderType, Date expectedDeliveryTime );
	
	@Transactional
	@Modifying
	@Query("UPDATE OrderHeader p SET p.pickHeader = :newPickHeader , p.updatedBy = :updatedBy , p.dateUpdated = :dateUpdated where p.pickHeader = :oldPickHeader")
    Integer updateNewPickHeader(@Param("newPickHeader") PickHeader newPickHeader,@Param("oldPickHeader") PickHeader oldPickHeader,@Param("updatedBy") String updatedBy,@Param("dateUpdated") Date dateUpdated);
	
}

