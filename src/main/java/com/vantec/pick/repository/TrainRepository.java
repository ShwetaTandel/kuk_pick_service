package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.Train;

public interface TrainRepository extends JpaRepository<Train, Serializable>{
	Train findById(Long id);	
	Train findByTrain(String train);	

}

