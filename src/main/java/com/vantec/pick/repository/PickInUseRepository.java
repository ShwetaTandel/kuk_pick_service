package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.PickInUse;

public interface PickInUseRepository extends JpaRepository<PickInUse, Serializable> {

	@Query("SELECT a FROM PickInUse a where  a.inUseBy !=:userId and  a.dateUpdated >=:dateUpdated  and a.pickKey=:key")
	List<PickInUse> findByKeyAndInUseBy(@Param("key") String key, @Param("userId") String userId,
			@Param("dateUpdated") Date dateUpdated);
	
	@Query(value="SELECT * FROM pick_in_use a where a.pick_key=:key order by id desc limit 1" ,nativeQuery = true)
	PickInUse findByKey(@Param("key") String key);
	

}
