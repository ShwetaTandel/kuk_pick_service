package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.TransactionHistorySkip;

public interface TransactionHistorySkipRepository extends JpaRepository<TransactionHistorySkip , Serializable>{
}

