package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.PickHeader;

public interface PickHeaderRepository extends JpaRepository<PickHeader, Serializable>{

	PickHeader findById(Long id);
	
	PickHeader findByDocumentReference(String documentReference);
	
	PickHeader findByCustomerReference(String customerReference);
	
	PickHeader findByOrderHeader(OrderHeader orderHeader);
	
	
	@Transactional
	@Modifying
	@Query("UPDATE PickHeader p SET p.documentStatus = :documentStatus , p.updatedBy = :updatedBy , p.dateUpdated = :dateUpdated where p.documentReference = :documentReference")
    Integer updateByStatus(@Param("documentStatus") DocumentStatus documentStatus,@Param("documentReference") String documentReference,@Param("updatedBy") String updatedBy,@Param("dateUpdated") Date dateUpdated);
	
}

