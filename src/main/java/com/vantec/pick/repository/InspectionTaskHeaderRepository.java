package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.InspectionTaskHeader;

public interface InspectionTaskHeaderRepository extends JpaRepository<InspectionTaskHeader, Serializable>{
	
	InspectionTaskHeader findByCustomerReference(String customerReference);
	
}

