package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.PickBody;
import com.vantec.pick.entity.PickDetail;
import com.vantec.pick.entity.ZoneDestination;

public interface PickDetailRepository extends JpaRepository<PickDetail, Serializable>{

	PickDetail findById(Long id);
	
	PickDetail findByInventoryMasterAndDocumentReference(InventoryMaster inventoryMaster,String documentReference);
	
	List<PickDetail> findByPickBodyAndProcessed(PickBody pickBody, boolean processed);
	PickDetail findByPickBodyAndPartNumberAndProcessedAndLocationCodeAndSerialReference(PickBody pickBody, String partNumber, boolean processed, String locationCode, String serialreference);
	
	@Query("SELECT p FROM PickDetail p where p.documentReference= :documentReference and (p.serialReferenceScanned is not null or p.serialReferenceScanned='')")
	List<PickDetail> findByDocumentReferenceNotPicked(@Param("documentReference") String documentReference);
	
	//and (p.inUseBy is null or p.inUseBy =:userId)
	@Query("SELECT p FROM PickDetail p where p.pickBody in(:pickBodies) and p.processed = false and p.qtyAllocated > 0 order by p.pickSequence asc")
	List<PickDetail> findPickForBodies(@Param("pickBodies") List<PickBody> pickBodies);
	
	// and (p.inUseBy is null or p.inUseBy =:userId) -- removing this for time being
	@Query("SELECT p FROM PickDetail p where p.pickBody in(:pickBodies) and p.processed = false and p.zoneDestination in(:zoneDestinations) and p.part.fullBoxPick=false and p.part.modular=false order by p.pickSequence asc")
	List<PickDetail> findPickForBodiesAndZoneDestinations(@Param("pickBodies") List<PickBody> pickBodies, @Param("zoneDestinations") List<ZoneDestination> zoneDestinations );
	
	List<PickDetail> findByDocumentReference(String documentReference);
	
	PickDetail findByDocumentReferenceAndSerialReference(String documentReference,String serialReference);
	
	@Query("SELECT p FROM PickDetail p where p.serialReference= :serialReference and p.partNumber=:partNumber and p.processed = false and p.qtyAllocated > 0")
	List<PickDetail> findBySerialReferenceAndPartNumber(@Param("serialReference") String serialReference,@Param("partNumber") String partNumber);

	List<PickDetail> findByDocumentReferenceOrderByLineNoAsc(String documentReference);
	
	List<PickDetail> findByInventoryMaster(InventoryMaster  inventoryMaster);
	
	@Query("SELECT count(p) FROM PickDetail p where p.pickBody= :pickBody and (p.inUseBy is not null or p.inUseBy<>'')")
	List<PickDetail> findInUsedDetailsByPickBody(@Param("pickBody") PickBody pickBody);
	
	
	@Transactional
	@Modifying
	@Query("UPDATE PickDetail p SET p.processed = true , p.updatedBy = :updatedBy , p.dateUpdated = now() where p.pickBody = :pickBody and p.processed = false")
    Integer markProcessedByBody(@Param("pickBody") PickBody pickBody,@Param("updatedBy") String updatedBy);
	
	@Transactional
	@Modifying
	@Query("UPDATE PickDetail p SET p.inventoryMaster = NULL where p.inventoryMaster in(:inventoryMasters)")
    Integer markInventoryNullInPick(@Param("inventoryMasters")  List<InventoryMaster> inventoryMasters );

	@Query("SELECT p FROM PickDetail p where p.documentReference= :documentReference and p.inventoryMaster in (:inventories)")
	List<PickDetail> findByInventoriesAndDocumentReference(@Param("inventories")  List<InventoryMaster> inventories, @Param("documentReference") String documentReference);
	
	@Query("SELECT p FROM PickDetail p where p.id in (:ids)")
	List<PickDetail> findAllByIds(@Param("ids")  List<Long> ids);
}

