package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.InventoryStatus;

public interface InventoryStatusRepository extends JpaRepository<InventoryStatus, Serializable>{

	InventoryStatus findByInventoryStatusCode(String status);
	
}

