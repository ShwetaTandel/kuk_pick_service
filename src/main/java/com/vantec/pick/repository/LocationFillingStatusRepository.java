package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.LocationFillingStatus;

public interface LocationFillingStatusRepository extends JpaRepository<LocationFillingStatus, Serializable>{

	LocationFillingStatus findByFillingCode(String fillingCode);
	
}

