package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.DocumentBody;
import com.vantec.pick.entity.DocumentHeader;
import com.vantec.pick.entity.Part;

public interface DocumentBodyRepository extends JpaRepository<DocumentBody, Serializable>{
	
	DocumentBody findById(Long id);

	List<DocumentBody> findByDocumentHeader(DocumentHeader documentHeader);
	
	DocumentBody findByDocumentHeaderAndPart(DocumentHeader documentHeader,Part part);
	
	
	
	
	
}

