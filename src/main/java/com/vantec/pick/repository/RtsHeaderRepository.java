package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.RtsHeader;


public interface RtsHeaderRepository extends JpaRepository<RtsHeader, Serializable>{

	RtsHeader findByOrderReference(String orderReference);
	


}
