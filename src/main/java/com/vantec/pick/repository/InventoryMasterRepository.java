package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.InventoryStatus;
import com.vantec.pick.entity.Location;
import com.vantec.pick.entity.Tag;

public interface InventoryMasterRepository extends JpaRepository<InventoryMaster, Serializable>{

	InventoryMaster findById(Long id);
	List<InventoryMaster> findByPartNumberOrderByDateCreatedAsc(String partNumber);
	
	List<InventoryMaster> findByParentTagAndInventoryStatus(Tag tag,InventoryStatus status);
	
	List<InventoryMaster> findByCurrentLocationAndInventoryStatus(Location location,InventoryStatus status);
	
	InventoryMaster findByPartNumberAndSerialReference(String partNumber,String serialReference);
	
	List<InventoryMaster>  findBySerialReference(String serialReference);

	List<InventoryMaster> findByPartNumberOrderByDateCreatedAscLocationCodeAscInventoryQtyAscSerialReferenceAsc(String partNumber);
	
	List<InventoryMaster> findBylocationCode(String locationCode);
	
	List<InventoryMaster> findAllByPartNumber(String partNumber);
	
	List<InventoryMaster> findAllByTagReference(String tagReference);
	
	List<InventoryMaster> findByOriginatingDetailId(Long detailId);


	@Query("SELECT a FROM InventoryMaster a where a.originatingDetailId in (:detailIds) and a.inventoryStatus =:status")
	List<InventoryMaster> findAllByOriginatingDetailId(@Param("detailIds") List<Long> detailIds, @Param("status") InventoryStatus status);
	
	@Transactional
	@Modifying
	@Query("UPDATE InventoryMaster p SET p.currentLocation = :location , p.updatedBy = :user , p.dateUpdated = now() where p.tagReference = :tagReference")
	Integer updateByTagReference(@Param("tagReference") String tagReference, @Param("location") Location location,  @Param("user")String user);
	
	List<InventoryMaster> findByTagReferenceAndInventoryStatusNotIn(String tagReference,InventoryStatus inventoryStatus);
	
	List<InventoryMaster> findByTagReferenceAndInventoryStatusIsNull(String tagReference);
	List<InventoryMaster> findByTagReference(String tagReference);
	
	List<InventoryMaster> findBySerialReferenceAndInventoryStatusNotIn(String serialReference,InventoryStatus inventoryStatus);
	
	List<InventoryMaster> findBySerialReferenceAndInventoryStatusIsNull(String serialReference);

}

