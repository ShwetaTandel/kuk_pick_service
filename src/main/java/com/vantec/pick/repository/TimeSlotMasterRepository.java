package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.TimeSlotMaster;

public interface TimeSlotMasterRepository extends JpaRepository<TimeSlotMaster, Serializable>{

	TimeSlotMaster findByOrderType(String orderType);
	
}

