package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.F6MissingHistory;

public interface F6MissingHistoryRepository extends JpaRepository<F6MissingHistory, Serializable>{
	
}

