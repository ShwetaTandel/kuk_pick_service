package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.AllocationDetail;
import com.vantec.pick.entity.Part;
import com.vantec.pick.entity.ScheduledWork;

public interface AllocationDetailRepository extends JpaRepository<AllocationDetail, Serializable>{

	List<AllocationDetail> findByScheduledWork(ScheduledWork scheduledWork);
	
	AllocationDetail findById(Long id);
	
	@Query("SELECT a FROM AllocationDetail a join a.scheduledWork s where a.part= :part and a.processed=false and s.replenishmentOrder=true")
	List<AllocationDetail> findAllocationsForReplenTaskByPart(@Param("part") Part part);
	
}

