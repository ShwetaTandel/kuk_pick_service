package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.Trolley;

public interface TrolleyRepository extends JpaRepository<Trolley, Serializable>{
	Trolley findById(Long id);	
	Trolley findByTrolley(String trolley);

}

