package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Serializable>{

	
	List<Company> findAll();
	
}

