package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.PickDetail;
import com.vantec.pick.entity.PickOrderLink;

public interface PickOrderLinkRepository extends JpaRepository<PickOrderLink, Serializable>{
	
	List<PickOrderLink> findByPickDetail(PickDetail detail);
	
	@Query("SELECT a FROM PickOrderLink a where a.orderBody in (:orderbodies)")
	List<PickOrderLink> findAllForOrderBodies(@Param("orderbodies") List<OrderBody> orderBodies);
	
	@Query("SELECT a FROM PickOrderLink a where a.pickDetail in (:pickDetails)")
	List<PickOrderLink> findAllForPickDetails(@Param("pickDetails") List<PickDetail> pickDetails);
	
	@Query("SELECT COALESCE(sum(a.qtyAllocated), 0) FROM PickOrderLink a where a.orderBody=:orderBody and a.processed = false")
	Integer getAllocatedForOrderBody(@Param("orderBody") OrderBody orderBody);
	
	@Transactional
	@Modifying
	@Query("UPDATE PickOrderLink p SET p.processed = true , p.updatedBy = :updatedBy , p.lastUpdatedAt = now() where p.pickDetail =:pickDetail")
    Integer markProcessedByPickDetail(@Param("pickDetail") PickDetail pickDetail,@Param("updatedBy") String updatedBy);
	
	

	
}

