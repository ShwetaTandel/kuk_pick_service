package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.ZoneDestination;

public interface ZoneDestinationRepository extends JpaRepository<ZoneDestination, Serializable>{
	ZoneDestination findById(Long id);	
	ZoneDestination findByZoneDestination(String zoneDestination);
	
	
	@Query("SELECT distinct t.trolley FROM ZoneDestination z join z.trolley t  where z.zoneDestination in (:zones)")
	List<String> findTrainsForZones(@Param("zones")List<String> zones);
	
	
}

