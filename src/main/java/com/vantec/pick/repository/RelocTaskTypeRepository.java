package com.vantec.pick.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vantec.pick.entity.RelocaTaskType;

public interface RelocTaskTypeRepository extends JpaRepository<RelocaTaskType, Serializable>{
	
	RelocaTaskType findByCustRefPrefix(String custRefPrefix);

	
	
}

