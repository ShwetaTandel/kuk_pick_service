package com.vantec.pick.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.vantec.pick.entity.DocumentBody;
import com.vantec.pick.entity.DocumentDetail;

public interface DocumentDetailRepository extends JpaRepository<DocumentDetail, Serializable>{
	
	@Query("SELECT d FROM DocumentDetail d WHERE d.documentBody = :documentBody and d.qtyTransacted < d.qtyExpected order by d.ourDetailNo asc")
	List<DocumentDetail> findAllByDocumentBody(@Param("documentBody") DocumentBody documentBody);
	
	DocumentDetail findByDocumentBodyAndSerialReferenceLike(DocumentBody documentBody,String serialReference);
	
	List<DocumentDetail> findByDocumentBody(DocumentBody documentBody);
	
}

