package com.vantec.pick.command;

public interface CustomValueGeneratorCommand<T> {

	public T generate(Long id,String name);
}
