package com.vantec.pick.dao;

import com.vantec.pick.entity.Part;



public interface DAO {
	
	public Part findPartByPartNumber(String partNumber);
}
