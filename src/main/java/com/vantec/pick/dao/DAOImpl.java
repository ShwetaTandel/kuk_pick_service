package com.vantec.pick.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.entity.Part;



@Transactional
@Repository
public class DAOImpl implements DAO {

	private static Logger LOGGER = LoggerFactory.getLogger(DAOImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	

	
 
	@Override
	public Part findPartByPartNumber(String partNumber) {
		Part part = (Part) entityManager.createNamedQuery("findPartByPartNumber").setParameter("partNumber", "%"+partNumber).getSingleResult();
		return part;
	}
	
	
	

	
	
	

}
