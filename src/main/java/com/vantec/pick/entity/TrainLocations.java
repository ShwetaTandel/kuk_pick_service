package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "train_locations" )
public class TrainLocations implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "trainLocationCode")
	private String trainLocationCode;
	
	@Column(name = "trainLocationHash")
	private String trainLocationHash;
	

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateUpdated")
	private Date dateUpdated;
	
	@Column(name = "updatedBy")
	private String updatedBy;
	
	@Column(name = "inUseBy")
	private String inUseBy;

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getInUseBy() {
		return inUseBy;
	}

	public void setInUseBy(String inUseBy) {
		this.inUseBy = inUseBy;
	}

	public TrainLocations() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTrainLocationCode() {
		return trainLocationCode;
	}

	public void setTrainLocationCode(String trainLocationCode) {
		this.trainLocationCode = trainLocationCode;
	}

	public String getTrainLocationHash() {
		return trainLocationHash;
	}

	public void setTrainLocationHash(String trainLocationHash) {
		this.trainLocationHash = trainLocationHash;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
}
