package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pickInUse")
public class PickInUse implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@Column(name = "pickKey")
	private String pickKey;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedAt")
	private Date dateUpdated;
	
	@Column(name = "inUseBy")
	private String inUseBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPickKey() {
		return pickKey;
	}

	public void setPickKey(String pickKey) {
		this.pickKey = pickKey;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getInUseBy() {
		return inUseBy;
	}

	public void setInUseBy(String inUseBy) {
		this.inUseBy = inUseBy;
	}
	
	
	

}
