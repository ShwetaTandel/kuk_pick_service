package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "inspectionTaskHeader")
public class InspectionTaskHeader implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "documentReference")
	private String documentReference;
	
	@Column(name = "customerReference")
	private String customerReference;
	
	
	@Column(name = "orderType")
	private String orderType;
	
	
	@Column(name = "expectedDeliveryTime")
	private Date expectedDeliveryTime;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DocumentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;
	
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;
	
	@Column(name = "baanSerial")
	private String baanSerial;

	@Column(name = "machineModel")
	private String machineModel;
	
	@OneToMany(mappedBy = "inspectionTaskHeader")
	private List<InspectionTaskBody> inspectTaskBodies = new ArrayList<InspectionTaskBody>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public String getCustomerReference() {
		return customerReference;
	}

	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Date getExpectedDeliveryTime() {
		return expectedDeliveryTime;
	}

	public void setExpectedDeliveryTime(Date expectedDeliveryTime) {
		this.expectedDeliveryTime = expectedDeliveryTime;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getBaanSerial() {
		return baanSerial;
	}

	public void setBaanSerial(String baanSerial) {
		this.baanSerial = baanSerial;
	}

	public String getMachineModel() {
		return machineModel;
	}

	public void setMachineModel(String machineModel) {
		this.machineModel = machineModel;
	}

	public List<InspectionTaskBody> getInspectTaskBodies() {
		return inspectTaskBodies;
	}

	public void setInspectTaskBodies(List<InspectionTaskBody> inspectTaskBodies) {
		this.inspectTaskBodies = inspectTaskBodies;
	}


}
