package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "pickOrderLink")
public class PickOrderLink implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "pickDetailId", referencedColumnName = "id")
	private PickDetail pickDetail;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orderBodyId", referencedColumnName = "id")
	private OrderBody orderBody;
	
	@Column(name = "lineNo")
	private Integer lineNo;
	
	@Column(name = "dateCreated")
	private Date dateCreated;

	@Column(name = "lastUpdatedDate")
	private Date lastUpdatedAt;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "qtyAllocated")
	private Double qtyAllocated;

	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "processed")
	private Boolean processed;

	public Boolean getProcessed() {
		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}

	public Integer getLineNo() {
		return lineNo;
	}

	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public PickOrderLink() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	
	

	public Double getQtyAllocated() {
		return qtyAllocated;
	}

	public void setQtyAllocated(Double qtyAllocated) {
		this.qtyAllocated = qtyAllocated;
	}

	public PickDetail getPickDetail() {
		return pickDetail;
	}

	public void setPickDetail(PickDetail pickDetail) {
		this.pickDetail = pickDetail;
	}

	public OrderBody getOrderBody() {
		return orderBody;
	}

	public void setOrderBody(OrderBody orderBody) {
		this.orderBody = orderBody;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}


	public Date getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(Date lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "PickOrderLink [id=" + id + ", pickDetail=" + (pickDetail != null ? pickDetail.getId() : "null")
				+ ", orderBody=" + (orderBody != null ? orderBody.getId() : "null") + ", qtyAllocated="
				+ qtyAllocated + ", lineNo=" + lineNo + ", lastUpdatedAt=" + lastUpdatedAt + ", updatedBy=" + updatedBy
				+ ", processed=" + processed + "]";
	}

	
}
