package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "documentBody")
public class DocumentBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documentHeaderId", referencedColumnName = "id")
	private DocumentHeader documentHeader;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "requirementBodyId", referencedColumnName = "id")
	private  DocumentBody requirementBody;
	
	@Column(name = "qtyExpected")
	private Integer qtyExpected;
	
	@Column(name = "qtyTransacted")
	private Integer qtyTransacted;
	
	@Column(name = "class")
	private String documentClass;
	
	@Column(name = "prufCube")
	private Boolean prufCube;
	
	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "ourLineNo")
	private int ourLineNo;
	
	@Column(name = "qtyAllocated")
	private Integer qtyAllocated;
	
	@Column(name = "wempf")
	private String wempf;
	
	@Column(name = "sequenceNo")
	private String sequenceNo;
	
	@Column(name = "tray")
	private String tray;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;
	
	@OneToMany(mappedBy = "documentBody")
	private List<DocumentDetail> documentDetails = new ArrayList<DocumentDetail>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public DocumentHeader getDocumentHeader() {
		return documentHeader;
	}

	public void setDocumentHeader(DocumentHeader documentHeader) {
		this.documentHeader = documentHeader;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

    
	public Integer getQtyExpected() {
		return qtyExpected;
	}

	public void setQtyExpected(Integer qtyExpected) {
		this.qtyExpected = qtyExpected;
	}

	public Integer getQtyTransacted() {
		return qtyTransacted;
	}

	public void setQtyTransacted(Integer qtyTransacted) {
		this.qtyTransacted = qtyTransacted;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public List<DocumentDetail> getDocumentDetails() {
		return documentDetails;
	}

	public void setDocumentDetails(List<DocumentDetail> documentDetails) {
		this.documentDetails = documentDetails;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public DocumentBody getRequirementBody() {
		return requirementBody;
	}

	public void setRequirementBody(DocumentBody requirementBody) {
		this.requirementBody = requirementBody;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getDocumentClass() {
		return documentClass;
	}

	public void setDocumentClass(String documentClass) {
		this.documentClass = documentClass;
	}

	public Boolean getPrufCube() {
		return prufCube;
	}

	public void setPrufCube(Boolean prufCube) {
		this.prufCube = prufCube;
	}

	public int getOurLineNo() {
		return ourLineNo;
	}

	public void setOurLineNo(int ourLineNo) {
		this.ourLineNo = ourLineNo;
	}

	public Integer getQtyAllocated() {
		return qtyAllocated;
	}

	public void setQtyAllocated(Integer qtyAllocated) {
		this.qtyAllocated = qtyAllocated;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getWempf() {
		return wempf;
	}

	public void setWempf(String wempf) {
		this.wempf = wempf;
	}

	public String getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(String sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getTray() {
		return tray;
	}

	public void setTray(String tray) {
		this.tray = tray;
	}
    
    
	
	

}
