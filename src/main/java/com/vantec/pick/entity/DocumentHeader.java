package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "documentHeader")
public class DocumentHeader implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;
	
	@Column(name = "documentReferenceCode")
	private String documentReferenceCode;
	
	@Column(name = "customerReferenceCode")
	private String customerReferenceCode;
	
	@Column(name = "tanum")
	private String tanum;
	
	@Column(name = "bwlvs")
	private String bwlvs;
	
	@Column(name = "rollingNo")
	private String rollingNo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "requirementHeaderId", referencedColumnName = "id")
	private DocumentHeader requirementHeader;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scheduledWorkId", referencedColumnName = "id")
	private ScheduledWork scheduleWork;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documentStatusId", referencedColumnName = "id")
	private DocumentStatus documentStatus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vendorId", referencedColumnName = "id")
	private Vendor vendor;
	
	@Column(name = "pickType")
	private String pickType;
	
	@Column(name = "allocated")
	private Boolean allocated;
	
	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "autoConfirm")
	private Boolean autoConfirm;
	
	@Column(name = "autoCreated")
	private Boolean autoCreated;
	
	@Column(name = "complete")
	private Boolean complete;
	
	@Column(name = "picked")
	private Boolean picked;
	
	@Column(name = "documentType")
	private String documentType;
	
	@Column(name = "orderType")
	private String orderType;
	
	@Column(name = "benum")
	private String benum;
	
	@Column(name = "workStation")
	private String workStation;
	
	@Column(name = "class")
	private String documnetClass;
	
	@Column(name = "inspectionRequired")
	private Boolean inspectionRequired;
	
	@Column(name = "expectedDelivery")
	private Date expectedDelivery;
	
	@Column(name = "timeSlot")
	private Date timeSlot;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;
	
	
	
	@OneToMany(mappedBy = "documentHeader")
	private List<DocumentBody> documentBodies = new ArrayList<DocumentBody>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentReferenceCode() {
		return documentReferenceCode;
	}

	public void setDocumentReferenceCode(String documentReferenceCode) {
		this.documentReferenceCode = documentReferenceCode;
	}

	public String getCustomerReferenceCode() {
		return customerReferenceCode;
	}

	public void setCustomerReferenceCode(String customerReferenceCode) {
		this.customerReferenceCode = customerReferenceCode;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	
	public List<DocumentBody> getDocumentBodies() {
		return documentBodies;
	}

	public void setDocumentBodies(List<DocumentBody> documentBodies) {
		this.documentBodies = documentBodies;
	}

	public String getBwlvs() {
		return bwlvs;
	}

	public void setBwlvs(String bwlvs) {
		this.bwlvs = bwlvs;
	}

	public DocumentHeader getRequirementHeader() {
		return requirementHeader;
	}

	public void setRequirementHeader(DocumentHeader requirementHeader) {
		this.requirementHeader = requirementHeader;
	}

	public String getPickType() {
		return pickType;
	}

	public void setPickType(String pickType) {
		this.pickType = pickType;
	}

	public ScheduledWork getScheduleWork() {
		return scheduleWork;
	}

	public void setScheduleWork(ScheduledWork scheduleWork) {
		this.scheduleWork = scheduleWork;
	}

	public Boolean getAllocated() {
		return allocated;
	}

	public void setAllocated(Boolean allocated) {
		this.allocated = allocated;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getAutoConfirm() {
		return autoConfirm;
	}

	public void setAutoConfirm(Boolean autoConfirm) {
		this.autoConfirm = autoConfirm;
	}

	public Boolean getAutoCreated() {
		return autoCreated;
	}

	public void setAutoCreated(Boolean autoCreated) {
		this.autoCreated = autoCreated;
	}

	public Boolean getComplete() {
		return complete;
	}

	public void setComplete(Boolean complete) {
		this.complete = complete;
	}

	public String getDocumentType() {
		return documentType;
	}
	public String getDocumnetClass() {
		return documnetClass;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public void setDocumnetClass(String documnetClass) {
		this.documnetClass = documnetClass;
	}

	public Boolean getInspectionRequired() {
		return inspectionRequired;
	}

	public void setInspectionRequired(Boolean inspectionRequired) {
		this.inspectionRequired = inspectionRequired;
	}

	public Boolean getPicked() {
		return picked;
	}

	public void setPicked(Boolean picked) {
		this.picked = picked;
	}

	public DocumentStatus getDocumentStatus() {
		return documentStatus;
	}

	public void setDocumentStatus(DocumentStatus documentStatus) {
		this.documentStatus = documentStatus;
	}

	public Date getExpectedDelivery() {
		return expectedDelivery;
	}

	public void setExpectedDelivery(Date expectedDelivery) {
		this.expectedDelivery = expectedDelivery;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getWorkStation() {
		return workStation;
	}

	public void setWorkStation(String workStation) {
		this.workStation = workStation;
	}

	public Date getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(Date timeSlot) {
		this.timeSlot = timeSlot;
	}

	public String getTanum() {
		return tanum;
	}

	public void setTanum(String tanum) {
		this.tanum = tanum;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getBenum() {
		return benum;
	}

	public void setBenum(String benum) {
		this.benum = benum;
	}

	public String getRollingNo() {
		return rollingNo;
	}

	public void setRollingNo(String rollingNo) {
		this.rollingNo = rollingNo;
	}
	
	
	
	
	
}
