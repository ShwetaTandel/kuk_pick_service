package com.vantec.pick.entity;

import java.util.Comparator;
import java.util.Date;

public class SortInventoryMasterByDateCreated implements Comparator<InventoryMaster>{


	@Override
	public int compare(InventoryMaster o1, InventoryMaster o2) {
		// TODO Auto-generated method stub
		return o1.getDateCreated().compareTo(o2.getDateCreated());
	}
	

}
