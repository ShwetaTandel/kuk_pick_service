package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "documentDetail")
@JsonIgnoreProperties(ignoreUnknown=true)
public class DocumentDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "documentBodyId", referencedColumnName = "id")
	private DocumentBody documentBody;

	@Column(name = "serialReference")
	private String serialReference;
	

	@Column(name = "qtyExpected")
	private Integer qtyExpected;
	
	@Column(name = "qtyTransacted")
	private Integer qtyTransacted;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "requirementDetailId", referencedColumnName = "id")
	private DocumentDetail requirementDetail;
	
	@Column(name = "matnr")
	private String matnr;
	
	@Column(name = "class")
	private String documentClass;
	
	@Column(name = "tapos")
	private String tapos;
	
	@Column(name = "prufCube")
	private Boolean prufCube;
	
	@Column(name = "blocked")
	private Boolean blocked;
	
	@Column(name = "qtyAllocated")
	private Integer qtyAllocated;
	
	@Column(name = "ourDetailNo")
	private Integer ourDetailNo;
	
	@Column(name = "meins")
	private String meins;

	@Column(name = "dateCreated")
	private Date dateCreated;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public DocumentDetail() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DocumentBody getDocumentBody() {
		return documentBody;
	}

	public void setDocumentBody(DocumentBody documentBody) {
		this.documentBody = documentBody;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}

	public Integer getQtyExpected() {
		return qtyExpected;
	}

	public void setQtyExpected(Integer qtyExpected) {
		this.qtyExpected = qtyExpected;
	}

	public Integer getQtyTransacted() {
		return qtyTransacted;
	}

	public void setQtyTransacted(Integer qtyTransacted) {
		this.qtyTransacted = qtyTransacted;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public DocumentDetail getRequirementDetail() {
		return requirementDetail;
	}

	public void setRequirementDetail(DocumentDetail requirementDetail) {
		this.requirementDetail = requirementDetail;
	}

	public String getMatnr() {
		return matnr;
	}

	public void setMatnr(String matnr) {
		this.matnr = matnr;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getDocumentClass() {
		return documentClass;
	}

	public void setDocumentClass(String documentClass) {
		this.documentClass = documentClass;
	}

	public Boolean getPrufCube() {
		return prufCube;
	}

	public void setPrufCube(Boolean prufCube) {
		this.prufCube = prufCube;
	}

	public Integer getQtyAllocated() {
		return qtyAllocated;
	}

	public void setQtyAllocated(Integer qtyAllocated) {
		this.qtyAllocated = qtyAllocated;
	}

	public Integer getOurDetailNo() {
		return ourDetailNo;
	}

	public void setOurDetailNo(Integer ourDetailNo) {
		this.ourDetailNo = ourDetailNo;
	}

	public String getTapos() {
		return tapos;
	}

	public void setTapos(String tapos) {
		this.tapos = tapos;
	}

	public Boolean getBlocked() {
		return blocked;
	}

	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}

	public String getMeins() {
		return meins;
	}

	public void setMeins(String meins) {
		this.meins = meins;
	}
    
	
	
	
	

}
