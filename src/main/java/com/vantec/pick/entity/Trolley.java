package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "trolley")
public class Trolley implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	

	@Column(name = "trolley")
	private String trolley;

	@Column(name = "description")
	private String description;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "trainId", referencedColumnName = "id")
	private Train train;
	
	@Column(name = "sequence")
	private Integer sequence;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;
	
	@OneToMany(mappedBy = "trolley")
	private List<ZoneDestination> zonesDestinations = new ArrayList<ZoneDestination>();

	public Trolley() {
	}

	public Long getId() {
		return id;
	}

	public List<ZoneDestination> getZonesDestinations() {
		return zonesDestinations;
	}

	public void setZonesDestinations(List<ZoneDestination> zonesDestinations) {
		this.zonesDestinations = zonesDestinations;
	}

	public Integer getSequence() {
		return sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTrolley() {
		return trolley;
	}

	public void setTrolley(String trolley) {
		this.trolley = trolley;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Train getTrain() {
		return train;
	}

	public void setTrain(Train train) {
		this.train = train;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	 //recursive function to walk the tree
	public List<String> getAllZoneNames() {

		List<String> zones = new ArrayList<>();

		for (ZoneDestination zone : zonesDestinations) {
			zones.add(zone.getZoneDestination());
		}

		return zones;
	}
	  //recursive function to walk the tree
    public List<ZoneDestination> getAllZones(){
    	
        List<ZoneDestination> zones = new ArrayList<>();

        for (ZoneDestination zone : zonesDestinations) {
			zones.add(zone);
		}

        return zones;
    }
    
}
