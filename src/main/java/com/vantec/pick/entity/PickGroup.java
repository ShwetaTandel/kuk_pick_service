package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pickGroup")
public class PickGroup implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "pickGroupCode")
	private String pickGroupCode;
	
	@Column(name = "pickGroupDescription")
	private String pickGroupDescription;
	
	@Column(name = "maxCaseQty")
	private Integer maxCaseQty;
	
	@Column(name = "altPickGroupCode")
	private String altPickGroupCode;
	
	@Column(name = "dateCreated")
	private Date dateCreated;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public PickGroup() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPickGroupCode() {
		return pickGroupCode;
	}

	public void setPickGroupCode(String pickGroupCode) {
		this.pickGroupCode = pickGroupCode;
	}

	public String getPickGroupDescription() {
		return pickGroupDescription;
	}

	public void setPickGroupDescription(String pickGroupDescription) {
		this.pickGroupDescription = pickGroupDescription;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getMaxCaseQty() {
		return maxCaseQty;
	}

	public void setMaxCaseQty(Integer maxCaseQty) {
		this.maxCaseQty = maxCaseQty;
	}

	public String getAltPickGroupCode() {
		return altPickGroupCode;
	}

	public void setAltPickGroupCode(String altPickGroupCode) {
		this.altPickGroupCode = altPickGroupCode;
	}
	
	
	

}
