package com.vantec.pick.entity;

import java.util.Comparator;

public class SortAllocationDetailByDateCreated implements Comparator<AllocationDetail>{


	@Override
	public int compare(AllocationDetail o1, AllocationDetail o2) {
		// TODO Auto-generated method stub
		return o1.getDateCreated().compareTo(o2.getDateCreated());
	}
	

}
