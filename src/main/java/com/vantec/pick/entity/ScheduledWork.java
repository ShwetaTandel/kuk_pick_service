package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ScheduledWork")
public class ScheduledWork implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;
	
	@Column(name = "lineNumber")
	private Integer lineNumber;
	
	@Column(name = "qtyRequired")
	private Integer qtyRequired;
	
	@Column(name = "replenishmentOrder")
	private Boolean replenishmentOrder;
	
	@Column(name = "complete")
	private Boolean complete;
	
	@Column(name = "inUse")
	private Boolean inUse;
	
	@Column(name = "thirdPartyReference")
	private String thirdPartyReference;
	
	@Column(name = "tag")
	private String tag;
	
	@Column(name = "fullBoxPick")
	private Boolean fullBoxPick;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "associatedTransactionId", referencedColumnName = "id")
	private TransactionType associatedTransaction;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "associatedHeaderId", referencedColumnName = "id")
    private DocumentHeader associatedHeader;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	

	public ScheduledWork() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Boolean getReplenishmentOrder() {
		return replenishmentOrder;
	}

	public void setReplenishmentOrder(Boolean replenishmentOrder) {
		this.replenishmentOrder = replenishmentOrder;
	}

	public Boolean getComplete() {
		return complete;
	}

	public void setComplete(Boolean complete) {
		this.complete = complete;
	}

	public Boolean getInUse() {
		return inUse;
	}

	public void setInUse(Boolean inUse) {
		this.inUse = inUse;
	}

	public String getThirdPartyReference() {
		return thirdPartyReference;
	}

	public void setThirdPartyReference(String thirdPartyReference) {
		this.thirdPartyReference = thirdPartyReference;
	}

	public Boolean getFullBoxPick() {
		return fullBoxPick;
	}

	public void setFullBoxPick(Boolean fullBoxPick) {
		this.fullBoxPick = fullBoxPick;
	}

	public TransactionType getAssociatedTransaction() {
		return associatedTransaction;
	}

	public void setAssociatedTransaction(TransactionType associatedTransaction) {
		this.associatedTransaction = associatedTransaction;
	}

	public DocumentHeader getAssociatedHeader() {
		return associatedHeader;
	}

	public void setAssociatedHeader(DocumentHeader associatedHeader) {
		this.associatedHeader = associatedHeader;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	public Integer getQtyRequired() {
		return qtyRequired;
	}

	public void setQtyRequired(Integer qtyRequired) {
		this.qtyRequired = qtyRequired;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
	
	
	

}
