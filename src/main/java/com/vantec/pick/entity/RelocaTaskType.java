package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "relocTaskType")
public class RelocaTaskType implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "custFromLocation")
	private String custFromLocation;
	
	@Column(name = "custToLocation")
	private String custToLocation;
	
	
	@Column(name = "locationArea")
	private String locationArea;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fixedLocationId", referencedColumnName = "id")
	private Location fixedLocation;
	
	@Column(name = "custRefPrefix")
	private String custRefPrefix;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustFromLocation() {
		return custFromLocation;
	}

	public void setCustFromLocation(String custFromLocation) {
		this.custFromLocation = custFromLocation;
	}

	public String getCustToLocation() {
		return custToLocation;
	}

	public void setCustToLocation(String custToLocation) {
		this.custToLocation = custToLocation;
	}

	public String getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(String locationArea) {
		this.locationArea = locationArea;
	}

	public Location getFixedLocation() {
		return fixedLocation;
	}

	public void setFixedLocation(Location fixedLocation) {
		this.fixedLocation = fixedLocation;
	}

	public String getCustRefPrefix() {
		return custRefPrefix;
	}

	public void setCustRefPrefix(String custRefPrefix) {
		this.custRefPrefix = custRefPrefix;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	

}
