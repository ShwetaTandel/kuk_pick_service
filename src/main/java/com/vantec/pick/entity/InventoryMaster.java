package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "inventoryMaster")
public class InventoryMaster implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;

	@Column(name = "partNumber")
	private String partNumber;

	//ranOrder
	@Column(name = "ranOrOrder")
	private String ranOrOrder;

	@Column(name = "serialReference")
	private String serialReference;

	//pltNumber
	@Column(name = "tagReference")
	private String tagReference;

	@Column(name = "inventoryQty")
	private Integer inventoryQty;

	@Column(name = "allocatedQty")
	private Integer allocatedQty;
	
	@Column(name = "holdQty")
	private Integer holdQty;
	

	@Column(name = "availableQty")
	private Double availableQty;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currentLocationId", referencedColumnName = "id")
	private Location currentLocation;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "locationId", referencedColumnName = "id")
	private Location location;

	@Column(name = "locationCode")
	private String locationCode;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "productTypeId", referencedColumnName = "id")
	private ProductType productType;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventoryStatusId", referencedColumnName = "id")
	private InventoryStatus inventoryStatus;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "currentInventoryStatusId", referencedColumnName = "id")
	private InventoryStatus currentInventoryStatus;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parentTagId", referencedColumnName = "id")
	private Tag parentTag;

	
	@Column(name = "conversionFactor")
	private Integer conversionFactor;
	
	@Column(name = "recordedMissing")
	private Boolean recordedMissing = false;

	//decant
	@Column(name = "requiresDecant")
	private Boolean requiresDecant = false;

	//inspect
	@Column(name = "requiresInspection")
	private Boolean requiresInspection = false;

	//lastStockCheckDate
	@Column(name = "stockDate")
	private Date stockDate;
	
	@Column(name = "originatingDetailId")
	private Long originatingDetailId;
	

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public InventoryMaster() {
	}
	public InventoryStatus getCurrentInventoryStatus() {
		return currentInventoryStatus;
	}
	public void setCurrentInventoryStatus(InventoryStatus currentInventoryStatus) {
		this.currentInventoryStatus = currentInventoryStatus;
	}
	public Long getOriginatingDetailId() {
		return originatingDetailId;
	}
	public void setOriginatingDetailId(Long originatingDetailId) {
		this.originatingDetailId = originatingDetailId;
	}
	public Integer getHoldQty() {
		return holdQty;
	}

	public void setHoldQty(Integer holdQty) {
		this.holdQty = holdQty;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getSerialReference() {
		return serialReference;
	}

	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	
	public Integer getInventoryQty() {
		return inventoryQty;
	}

	public void setInventoryQty(Integer inventoryQty) {
		this.inventoryQty = inventoryQty;
	}

	public Integer getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(Integer allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	public Double getAvailableQty() {
		return availableQty;
	}

	public void setAvailableQty(Double availableQty) {
		this.availableQty = availableQty;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	
	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getRanOrOrder() {
		return ranOrOrder;
	}

	public void setRanOrOrder(String ranOrOrder) {
		this.ranOrOrder = ranOrOrder;
	}

	public String getTagReference() {
		return tagReference;
	}

	public void setTagReference(String tagReference) {
		this.tagReference = tagReference;
	}

	public Boolean getRequiresDecant() {
		return requiresDecant;
	}

	public void setRequiresDecant(Boolean requiresDecant) {
		this.requiresDecant = requiresDecant;
	}

	public Boolean getRequiresInspection() {
		return requiresInspection;
	}

	public void setRequiresInspection(Boolean requiresInspection) {
		this.requiresInspection = requiresInspection;
	}

	public Date getStockDate() {
		return stockDate;
	}

	public void setStockDate(Date stockDate) {
		this.stockDate = stockDate;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}

	public InventoryStatus getInventoryStatus() {
		return inventoryStatus;
	}

	public void setInventoryStatus(InventoryStatus inventoryStatus) {
		this.inventoryStatus = inventoryStatus;
	}

	public Boolean getRecordedMissing() {
		return recordedMissing;
	}

	public void setRecordedMissing(Boolean recordedMissing) {
		this.recordedMissing = recordedMissing;
	}

	public Integer getConversionFactor() {
		return conversionFactor;
	}

	public void setConversionFactor(Integer conversionFactor) {
		this.conversionFactor = conversionFactor;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Tag getParentTag() {
		return parentTag;
	}

	public void setParentTag(Tag parentTag) {
		this.parentTag = parentTag;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	@Override
	public String toString() {
		return "InventoryMaster [id=" + id + ", partNumber=" + partNumber + ", serialReference=" + serialReference
				+ ", inventoryQty=" + inventoryQty + ", allocatedQty=" + allocatedQty + ", holdQty=" + holdQty
				+ ", availableQty=" + availableQty + ", originatingDetailId=" + originatingDetailId + ", dateUpdated="
				+ dateUpdated + ", updatedBy=" + updatedBy + "]";
	}
	
    
}
