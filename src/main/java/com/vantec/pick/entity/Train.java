package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "train" )
public class Train implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	

	@Column(name = "train")
	private String train;
	
	@Column(name = "orderTypeCode")
	private String orderTypeCode;
	

	@Column(name = "description")
	private String description;
	

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;
	
	@OneToMany(mappedBy = "train")
	private List<Trolley> trolleys = new ArrayList<Trolley>();

	public Train() {
	}

	public List<Trolley> getTrolleys() {
		return trolleys;
	}

	public void setTrolleys(List<Trolley> trolleys) {
		this.trolleys = trolleys;
	}

	

	public String getOrderTypeCode() {
		return orderTypeCode;
	}

	public void setOrderTypeCode(String orderTypeCode) {
		this.orderTypeCode = orderTypeCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTrain() {
		return train;
	}

	public void setTrain(String train) {
		this.train = train;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	

    //recursive function to walk the tree
    public List<ZoneDestination> getAllZones(){
    	 List<Trolley>  trolleys = this.getTrolleys();
        List<ZoneDestination> zones = new ArrayList<>();

        for(Trolley child : trolleys){
        	zones.addAll(child.getZonesDestinations());
        }

        return zones;
    }

    //recursive function to walk the tree
    public List<String> getAllZonesNames(){
    	 List<Trolley>  trolleys = this.getTrolleys();
        List<String> zones = new ArrayList<>();

        for(Trolley child : trolleys){
        	zones.addAll(child.getAllZoneNames());
        }

        return zones;
    }
	
}
