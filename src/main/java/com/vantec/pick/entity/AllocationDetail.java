package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "AllocationDetail")
@JsonIgnoreProperties(ignoreUnknown=true)
public class AllocationDetail implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "allocatedQty")
	private Integer allocatedQty;
	
	@Column(name = "priority")
	private Integer priority;
	
	@Column(name = "processed")
	private Boolean processed;
	
	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "replenishment")
	private Boolean replenishment;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "inventoryMasterId", referencedColumnName = "id")
	private InventoryMaster inventory;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "scheduledWorkId", referencedColumnName = "id")
	private ScheduledWork scheduledWork;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "toLocationId", referencedColumnName = "id")
	private Location toLocation;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "originatingRequestId", referencedColumnName = "id")
	private DocumentDetail originatingRequest;
	
	@Column(name = "lineNo")
	private Integer lineNo;

	@Column(name = "dateCreated")
	private Date dateCreated;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	public AllocationDetail() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Boolean getProcessed() {
		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}

	public InventoryMaster getInventory() {
		return inventory;
	}

	public void setInventory(InventoryMaster inventory) {
		this.inventory = inventory;
	}

	public Integer getAllocatedQty() {
		return allocatedQty;
	}

	public void setAllocatedQty(Integer allocatedQty) {
		this.allocatedQty = allocatedQty;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	public ScheduledWork getScheduledWork() {
		return scheduledWork;
	}

	public void setScheduledWork(ScheduledWork scheduledWork) {
		this.scheduledWork = scheduledWork;
	}

	public DocumentDetail getOriginatingRequest() {
		return originatingRequest;
	}

	public void setOriginatingRequest(DocumentDetail originatingRequest) {
		this.originatingRequest = originatingRequest;
	}

	public Integer getLineNo() {
		return lineNo;
	}

	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}

	public Location getToLocation() {
		return toLocation;
	}

	public void setToLocation(Location toLocation) {
		this.toLocation = toLocation;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getReplenishment() {
		return replenishment;
	}

	public void setReplenishment(Boolean replenishment) {
		this.replenishment = replenishment;
	}
    
    

}
