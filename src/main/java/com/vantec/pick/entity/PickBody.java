package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pickBody")
public class PickBody implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "partNumber")
	private String partNumber;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partId", referencedColumnName = "id")
	private Part part;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orderBodyId", referencedColumnName = "id")
	private OrderBody orderBody;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pickHeaderId", referencedColumnName = "id")
	private PickHeader pickHeader;
	
	@Column(name = "documentReference")
	private String documentReference;
	
	@Column(name = "customerReference")
	private String customerReference;
	
	@Column(name = "qtyExpected")
	private Double qtyExpected;
	
	@Column(name = "qtyTransacted")
	private Double qtyTransacted;
	
	@Column(name = "qtyAllocated")
	private Double qtyAllocated;
	
	@Column(name = "qtyShortage")
	private Double qtyShortage;
	
	@Column(name = "processed")
	private Boolean processed;
	
	@Column(name = "zoneDestination")
	private String zoneDestination;
	
	@Column(name = "createdBy")
	private String createdBy;
	
	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "lastUpdatedBy")
	private String updatedBy;

	@Column(name = "lastUpdatedDate")
	private Date dateUpdated;
	
	@Column(name = "inUseBy")
	private String inUseBy;
	
	
	public String getInUseBy() {
		return inUseBy;
	}

	public void setInUseBy(String inUseBy) {
		this.inUseBy = inUseBy;
	}

	@OneToMany(mappedBy = "pickBody")
	private List<PickDetail> pickDetails = new ArrayList<PickDetail>();

	

	public String getZoneDestination() {
		return zoneDestination;
	}

	public void setZoneDestination(String zoneDestination) {
		this.zoneDestination = zoneDestination;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public Part getPart() {
		return part;
	}

	public void setPart(Part part) {
		this.part = part;
	}

	

	public OrderBody getOrderBody() {
		return orderBody;
	}

	public void setOrderBody(OrderBody documentBody) {
		this.orderBody = documentBody;
	}

	public PickHeader getPickHeader() {
		return pickHeader;
	}

	public void setPickHeader(PickHeader pickHeader) {
		this.pickHeader = pickHeader;
	}

	public Double getQtyExpected() {
		return qtyExpected;
	}

	public void setQtyExpected(Double qtyExpected) {
		this.qtyExpected = qtyExpected;
	}

	public Double getQtyAllocated() {
		return qtyAllocated;
	}

	public void setQtyAllocated(Double qtyAllocated) {
		this.qtyAllocated = qtyAllocated;
	}

	public Double getQtyShortage() {
		return qtyShortage;
	}

	public void setQtyShortage(Double qtyShortage) {
		this.qtyShortage = qtyShortage;
	}

	public Boolean getProcessed() {
		return processed;
	}

	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getDocumentReference() {
		return documentReference;
	}

	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}

	public List<PickDetail> getPickDetails() {
		return pickDetails;
	}

	public void setPickDetails(List<PickDetail> pickDetails) {
		this.pickDetails = pickDetails;
	}

	public Double getQtyTransacted() {
		return qtyTransacted;
	}

	public void setQtyTransacted(Double qtyTransacted) {
		this.qtyTransacted = qtyTransacted;
	}

	@Override
	public String toString() {
		return "PickBody [id=" + id + ", qtyExpected=" + qtyExpected + ", qtyTransacted="
				+ qtyTransacted + ", qtyAllocated=" + qtyAllocated + ", qtyShortage=" + qtyShortage + ", processed="
				+ processed + ",  partNumber=" + partNumber + ", documentReference=" + documentReference
				+ ", customerReference=" + customerReference + " zoneDestination=" + zoneDestination + ", updatedBy=" + updatedBy + ", dateUpdated="
				+ dateUpdated + "]";
	}
	

}
