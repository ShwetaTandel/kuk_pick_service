package com.vantec.pick.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


//LocationZoneType
@Entity
@Table(name = "locationSubType",
       uniqueConstraints = {@UniqueConstraint(columnNames={"locationSubTypeCode"})} )
public class LocationSubType implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "version")
	private Long version;

	//locationSubTypeCode
	@Column(name = "locationSubTypeCode")
	private String locationSubTypeCode;

	//zoneTypeDescription
	@Column(name = "locationSubTypeDescription")
	private String locationSubTypeDescription;

	@Column(name = "dateCreated")
	private Date dateCreated;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "lastUpdated")
	private Date dateUpdated;

	@Column(name = "lastUpdatedBy")
	private String updatedBy;
	
	@Column(name = "pltZone")
	private Boolean pltZone;
	
	@Column(name = "isTrain")
	private Boolean isTrain;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    
	public String getLocationSubTypeCode() {
		return locationSubTypeCode;
	}

	public void setLocationSubTypeCode(String locationSubTypeCode) {
		this.locationSubTypeCode = locationSubTypeCode;
	}

	public String getLocationSubTypeDescription() {
		return locationSubTypeDescription;
	}

	public void setLocationSubTypeDescription(String locationSubTypeDescription) {
		this.locationSubTypeDescription = locationSubTypeDescription;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Boolean getPltZone() {
		return pltZone;
	}

	public void setPltZone(Boolean pltZone) {
		this.pltZone = pltZone;
	}

	public Boolean getIsTrain() {
		return isTrain;
	}

	public void setIsTrain(Boolean isTrain) {
		this.isTrain = isTrain;
	}
	

}
