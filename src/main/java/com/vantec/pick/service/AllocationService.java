package com.vantec.pick.service;

import java.io.IOException;
import java.util.List;

import com.vantec.pick.entity.PickHeader;
import com.vantec.pick.model.AllocateDTO;

public interface AllocationService {
	
	public Boolean unAllocate(String documentReference, String userId);
	/**
	 * Called from Screen 
	 * @param allocateDTO
	 * @return
	 * @throws IOException
	 * @throws Exception 
	 */
	public Boolean allocateOrders(AllocateDTO allocateDTO) throws IOException, Exception;
	
	/***
	 * From RDT Allocated whole Pick
	 * @param documentReference
	 * @param userId
	 * @return
	 * @throws IOException
	 * @throws Exception 
	 */
	public Boolean allocateWithPickReference(String documentReference, String userId) throws IOException, Exception;
	
	/****
	 * Called from Reeceiving service and close train
	 * @param parts
	 * @param userId
	 * @return
	 * @throws IOException
	 * @throws Exception 
	 */
	public Boolean allocateForParts(List<String> parts, String userId) throws IOException, Exception;
	
	/****
	 * Call from process pick
	 * @param zones
	 * @param userId
	 * @return
	 * @throws IOException
	 */
	public Boolean allocateRefreshForPickTasks(List<String> tasks, String userId, PickHeader pickHeader) throws IOException, Exception;
	
	
	public Boolean checkInventory(Long orderBodyId) throws IOException;
	

}
