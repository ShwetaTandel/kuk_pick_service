package com.vantec.pick.service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.entity.Company;
import com.vantec.pick.entity.DocumentDetail;
import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.PickHeader;
import com.vantec.pick.entity.UploadFile;
import com.vantec.pick.repository.CompanyRepository;
import com.vantec.pick.repository.DocumentBodyRepository;
import com.vantec.pick.repository.DocumentDetailRepository;
import com.vantec.pick.repository.DocumentHeaderRepository;
import com.vantec.pick.repository.DocumentStatusRepository;
import com.vantec.pick.repository.OrderBodyRepository;
import com.vantec.pick.repository.OrderHeaderRepository;
import com.vantec.pick.repository.PickBodyRepository;
import com.vantec.pick.repository.PickDetailRepository;
import com.vantec.pick.repository.PickHeaderRepository;
import com.vantec.pick.repository.PickOrderLinkRepository;
import com.vantec.pick.repository.UploadFileRepository;
import com.vantec.pick.util.ConstantHelper;

@Service("messageService")
public class MessageServiceImpl implements MessageService {

	private static Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);

	@Autowired
	CompanyRepository companyRepository;

	@Autowired
	PickHeaderRepository pickHeaderRepository;

	@Autowired
	PickDetailRepository pickDetailRepository;

	@Autowired
	DocumentHeaderRepository documentHeaderRepository;

	@Autowired
	OrderHeaderRepository orderHeaderRepository;

	@Autowired
	OrderBodyRepository orderBodyRepository;

	@Autowired
	DocumentDetailRepository documentDetailRepository;

	@Autowired
	DocumentBodyRepository documentBodyRepository;

	@Autowired
	PickBodyRepository pickBodyRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	UploadFileRepository uploadFileRepository;

	@Autowired
	PickOrderLinkRepository pickOrderLinkRepository;

	@Override
	public Boolean sendPickedMessage(String documentRef, String userId) throws IOException {
		DocumentStatus pickedStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_PICKED);
		OrderHeader orderHeader = orderHeaderRepository.findByDocumentReference(documentRef);
		List<OrderHeader> headers = new ArrayList<OrderHeader>();
		headers.add(orderHeader);
		List<OrderBody> orderBodies = orderBodyRepository.findAllPickedBodiesForHeaders(headers, pickedStatus);
		if (orderBodies != null && !orderBodies.isEmpty()) {
			sendPickedMessageForOrderBody(orderBodies, userId);
			return true;
		}
		return false;

	}

	@Override
	public Boolean sendPickedMessageForOrderBody(List<OrderBody> orderBodies, String userId) throws IOException {
		Company company = companyRepository.findAll().get(0);
		DocumentStatus openStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);
		DocumentStatus transmittedStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_TRANSMITTED);
		if (orderBodies != null && !orderBodies.isEmpty()) {
			logger.info("Bodies found");
			writePickFile(userId, company, orderBodies, transmittedStatus);

			for (OrderBody body : orderBodies) {
				OrderHeader orderHeader = body.getOrderHeader();
				Integer count = orderBodyRepository.findCountOfOpenBodies(orderHeader, openStatus);
				if (count == 0) {
					orderHeader.setDocumentStatus(transmittedStatus);
					orderHeader.setUpdatedBy(userId);
					orderHeader.setDateUpdated(new Date());
					orderHeaderRepository.save(orderHeader);
				}

				PickHeader pickHeader = orderHeader.getPickHeader();
				Integer cntPick = pickBodyRepository.findCountOfOpenBodies(pickHeader);
				if (cntPick == 0) {
					pickHeader.setDocumentStatus(transmittedStatus);
					pickHeader.setDateUpdated(new Date());
					pickHeader.setUpdatedBy(userId);
					pickHeaderRepository.save(pickHeader);
				}

			}
			return true;
		}
		return false;

	}

	private void writePickFile(String userId, Company company, List<OrderBody> orderBodies,
			DocumentStatus transmittedStatus) throws IOException {

		UploadFile savedFile = createFileRecord(userId, company, ConstantHelper.PICKED_FILE_TYPE);
		logger.info("File records create for " + savedFile.getFilename() + " ->" + savedFile.getType() + "->"
				+ savedFile.getId());
		String filename = ConstantHelper.PICKED_FILE_TYPE + String.format("%09d", savedFile.getId());

		Path tempPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TMP_FILE_EXT);
		Path textPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TEXT_FILE_EXT);

		// PICKED ------------->
		// 4 EF0000721 9895 20862K1840NK -2
		// 4 EF0000721 9897 20862K1840NK -2
		// 4 MR0037674 0 2084621503 -1
		// Origin External Order Position Part Quantity

		// Origin "Fixed needs to match what was sent originally for BaaN Order
		// Not used by WMS 1,2,4,22"
		// External Order "BaaN Order Number
		// Used by BaaN to reference WMS"
		// Position Set to what was sent in Planned RTS (can be 0)
		// Part WMS Part Number
		// Qty Quantity (can be negative as being removed from order)

		BufferedWriter writer = Files.newBufferedWriter(tempPath);
		writer.write(ConstantHelper.BAAN_FILE_START_FILE_RECORD);
		writer.write("\n");
		int lineCnt = 0;
		for (OrderBody orderBody : orderBodies) {

			int diff = orderBody.getQtyTransacted().intValue() - orderBody.getQtyTransmitted().intValue();

			if (diff > 0) {

				String origin = orderBody.getOrderHeader().getOrigin() != null ? orderBody.getOrderHeader().getOrigin()
						: "";
				String pos = orderBody.getLineNo().toString();
				writer.write(origin + "|" + orderBody.getCustomerReference() + "|" + pos + "|"
						+ orderBody.getPartNumber() + "|" + diff);
				writer.write("\n");
				if (orderBody.getDifference().doubleValue() == 0) {
					orderBody.setDocumentStatus(transmittedStatus);
				}
				orderBody.setQtyTransmitted(orderBody.getQtyTransmitted().doubleValue() + diff);
				orderBodyRepository.save(orderBody);
				lineCnt++;
			}

		}

		writer.write(ConstantHelper.BAAN_FILE_END_FILE_RECORD + " - " + lineCnt + " record written");
		// Copy data from temp to txt file and delete temp file
		writer.close();
		Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
		Files.delete(tempPath);

		savedFile.setFilename(filename + ".txt");
		uploadFileRepository.save(savedFile);
	}

	@Override
	public Boolean sendCancelMessage(List<String> customerRefs, String userId) throws IOException {
		
		Company company = companyRepository.findAll().get(0);
		UploadFile savedFile = createFileRecord(userId, company, ConstantHelper.CANCEL_FILE_TYPE);
		logger.info("File records create for " + savedFile.getFilename() + " ->" + savedFile.getType() + "->"
				+ savedFile.getId());
		String filename = ConstantHelper.CANCEL_FILE_TYPE + String.format("%09d", savedFile.getId());

		Path tempPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TMP_FILE_EXT);
		Path textPath = Paths.get(company.getInterfaceOutput() + "\\" + filename + ConstantHelper.TEXT_FILE_EXT);

		// CANCEL ------------->
		// Start of file
		//PM0002562
		//PM0002562
		//End of file - 2 records written

		BufferedWriter writer = Files.newBufferedWriter(tempPath);
		writer.write(ConstantHelper.BAAN_FILE_START_FILE_RECORD);
		writer.write("\n");
		int lineCnt = 0;
		for (String custRef :  customerRefs) {

				writer.write(custRef);
				writer.write("\n");
				lineCnt++;

		}

		writer.write(ConstantHelper.BAAN_FILE_END_FILE_RECORD + " - " + lineCnt + " record written");
		// Copy data from temp to txt file and delete temp file
		writer.close();
		Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
		Files.delete(tempPath);

		savedFile.setFilename(filename + ".txt");
		uploadFileRepository.save(savedFile);
		return true;
	}

	private UploadFile createFileRecord(String userId, Company company, String type) {

		UploadFile receiptMessageFile = new UploadFile();
		receiptMessageFile.setFilename(type);
		// receiptMessageFile.setFileNumber(latestFile.getFileNumber() + 1);
		receiptMessageFile.setType(type);
		receiptMessageFile.setFullPath(company.getInterfaceOutput());
		receiptMessageFile.setUpdatedBy(userId);
		receiptMessageFile.setDateCreated(new Date());
		receiptMessageFile.setLastUpdated(new Date());
		UploadFile savedFile = uploadFileRepository.save(receiptMessageFile);
		return savedFile;

	}

	public String getLineNo(int line) {
		int maxvalue = 4;
		String lineNumber = "";
		int numberOfDigits = 0;
		numberOfDigits = String.valueOf(line).length();
		for (int i = 0; i < maxvalue - numberOfDigits; i++) {
			lineNumber += "0";
		}
		lineNumber = lineNumber + String.valueOf(line);
		return lineNumber;
	}

	public static void main(String args[]) {
		Path tempPath = Paths.get("C:\\rrfiles\\output\\" + "test100" + ".tmp");
		Path textPath = Paths.get("C:\\rrfiles\\output\\" + "test100" + ".txt");
		try {
			BufferedWriter writer = Files.newBufferedWriter(tempPath);
			writer.write("test the file");
			writer.close();
			Files.copy(tempPath, textPath, StandardCopyOption.REPLACE_EXISTING);
			Files.delete(tempPath);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class OrderDetailsSortByLineNo implements Comparator<DocumentDetail> {

		@Override
		public int compare(DocumentDetail d1, DocumentDetail d2) {
			// Used for sorting in ascending order of
			// Line No
			return d1.getOurDetailNo().compareTo(d2.getOurDetailNo());
		}
	}

	public String getLineNo(int line, int maxvalue) {

		String lineNumber = "";
		int numberOfDigits = 0;
		numberOfDigits = String.valueOf(line).length();
		for (int i = 0; i < maxvalue - numberOfDigits; i++) {
			lineNumber += "0";
		}
		lineNumber = lineNumber + String.valueOf(line);
		return lineNumber;
	}

}
