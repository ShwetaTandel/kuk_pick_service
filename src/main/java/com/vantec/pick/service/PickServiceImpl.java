package com.vantec.pick.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.business.AllocationInfo;
import com.vantec.pick.business.InventoryInfo;
import com.vantec.pick.business.PickDocumentInfo;
import com.vantec.pick.business.TransactionInfo;
import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.InventoryStatus;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.OrderType;
import com.vantec.pick.entity.PickBody;
import com.vantec.pick.entity.PickDetail;
import com.vantec.pick.entity.PickHeader;
import com.vantec.pick.entity.PickInUse;
import com.vantec.pick.entity.ZoneDestination;
import com.vantec.pick.exception.ConcurrentException;
import com.vantec.pick.exception.InventoryQtyNotAdequate;
import com.vantec.pick.model.GetPickDTO;
import com.vantec.pick.model.PickDTO;
import com.vantec.pick.model.ProcessPickDTO;
import com.vantec.pick.repository.DocumentStatusRepository;
import com.vantec.pick.repository.InventoryMasterRepository;
import com.vantec.pick.repository.InventoryStatusRepository;
import com.vantec.pick.repository.LocationRepository;
import com.vantec.pick.repository.OrderBodyRepository;
import com.vantec.pick.repository.OrderHeaderRepository;
import com.vantec.pick.repository.OrderTypeRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.repository.PickBodyRepository;
import com.vantec.pick.repository.PickDetailRepository;
import com.vantec.pick.repository.PickHeaderRepository;
import com.vantec.pick.repository.PickInUseRepository;
import com.vantec.pick.repository.TrainRepository;
import com.vantec.pick.repository.TrolleyRepository;
import com.vantec.pick.repository.ZoneDestinationRepository;
import com.vantec.pick.util.ConstantHelper;

@Service("pickService")
public class PickServiceImpl implements PickService {

	private static Logger logger = LoggerFactory.getLogger(PickServiceImpl.class);

	@Autowired
	OrderHeaderRepository orderHeaderRepository;

	@Autowired
	OrderBodyRepository orderBodyRepository;

	@Autowired
	PickHeaderRepository pickHeaderRepository;

	@Autowired
	PickDetailRepository pickDetailRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	PickDocumentInfo pickDocumentInfo;
	
	@Autowired
	TransactionInfo transInfo;

	@Autowired
	AllocationInfo allocateInfo;

	@Autowired
	MessageService messageService;

	@Autowired
	OrderTypeRepository orderTypeRepository;

	@Autowired
	InventoryInfo inventoryInfo;

	@Autowired
	PickBodyRepository pickBodyRepository;

	@Autowired
	PartRepository partRepository;

	@Autowired
	InventoryMasterRepository inventoryRepository;

	@Autowired
	TrainRepository trainRepo;

	@Autowired
	ZoneDestinationRepository zoneDestRepo;

	@Autowired
	LocationRepository locationRepo;

	@Autowired
	AllocationService allocateService;

	@Autowired
	InventoryStatusRepository invStatRepo;
	@Autowired
	TrolleyRepository trolleyRepo;
	
	
	
	@Autowired
	PickInUseRepository pickInUseRepo;

	/**
	 * 
	 * 
	 * 
	 */
	@Override
	public PickDTO getPickDetail(GetPickDTO getPickDTO) throws IOException, Exception {
		PickDTO pick = new PickDTO();
		boolean isAllocated = true;
		PickHeader header = pickHeaderRepository.findByDocumentReference(getPickDTO.getPickDocRef());
		DocumentStatus transmittedStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_TRANSMITTED);
		if (header != null && header.getDocumentStatus().getDocumentStatusCode()
				.equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_OPEN)) {
			// Check for Unprocessed pick Tasks
			List<PickBody> bodies = pickBodyRepository.findAllByPickHeaderAndProcessed(header, false);
			if (bodies.isEmpty()) {
				pick.setMessage("Pick is complete");
				logger.info("Pick is complete :" + getPickDTO.getPickDocRef() + " and Pick Task:"
						+ getPickDTO.getPickTask());
				header.setDocumentStatus(transmittedStatus);
				header.setDateUpdated(new Date());
				pickHeaderRepository.save(header);
			} else {
				// Integer cnt =
				// pickBodyRepository.checkIfShortagesForPickHeader(header);
				if (getPickDTO.isAllocateRefresh()) {
					List<String> tasks = new ArrayList<String>();
					tasks.add(getPickDTO.getPickTask());
					Integer cnt = pickBodyRepository.findShortageBodiesByHeaderAndPickTask(header, tasks);
					if (cnt > 0) {
						// Call Allocate only of shortages are present r force
						// refresh
						// isAllocated =
						// allocateService.allocateWithPickReference(getPickDTO.getPickDocRef(),
						// getPickDTO.getUserId());
						isAllocated = allocateService.allocateRefreshForPickTasks(tasks, getPickDTO.getUserId(),
								header);
					}
				}
				if (isAllocated) {
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(new Date());
					calendar.add(Calendar.MINUTE, -5);
					Date dateUpdated = calendar.getTime();
					List<PickBody> pickBodies = new ArrayList<PickBody>();
					if(getPickDTO.getTrain()!=null && !getPickDTO.getTrain().isEmpty()){
						String[] parts = getPickDTO.getTrain().split(" - ");
						String trolley = parts[1];
						List<String> zones = trolleyRepo.findByTrolley(trolley).getAllZoneNames();
						
						pickBodies = pickBodyRepository.findAvailableBodiesByHeaderAndPickTaskAndZone(header,
								getPickDTO.getPickTask(), zones);
						
					}else{
						pickBodies = pickBodyRepository.findAvailableBodiesByHeaderAndPickTask(header,
								getPickDTO.getPickTask());
					}
					
					if (pickBodies.isEmpty()) {
						pickBodies = pickBodyRepository.findBodiesByHeaderAndPickTask(header, getPickDTO.getPickTask());
						if (pickBodies.isEmpty()) {
							pick.setMessage("No tasks for Pick");
							logger.info("No tasks(pick bodies) for Pick :" + getPickDTO.getPickDocRef()
									+ " and Pick Task:" + getPickDTO.getPickTask());
						} else {
							pick.setMessage("No stock for Pick");
							logger.info("No stock for Pick :" + getPickDTO.getPickDocRef() + " and Pick Task:"
									+ getPickDTO.getPickTask());
						}
					} else {
						List<PickDetail> pickDetails = null;
						List<PickInUse> pickInuse = pickInUseRepo.findByKeyAndInUseBy(getPickDTO.getPickDocRef()+getPickDTO.getPickTask(), getPickDTO.getUserId(), dateUpdated);
						if (pickInuse == null || pickInuse.isEmpty()){
							pickDetails = pickDetailRepository.findPickForBodies(pickBodies);
						}
					
						if (pickDetails != null && !pickDetails.isEmpty()) {
							pick = pickDocumentInfo.convertPickDetailEntityToView(pickDetails.get(0));
							PickHeader picHeader = pickDetails.get(0).getPickBody().getPickHeader();
							PickBody pickBody = pickDetails.get(0).getPickBody();
							pickBody.setInUseBy(getPickDTO.getUserId());
							pickBody.setDateUpdated(new Date());
							pickBody.setUpdatedBy(getPickDTO.getUserId());
							pickBodyRepository.save(pickBody);
							PickInUse newPickUse = pickInUseRepo.findByKey(getPickDTO.getPickDocRef()+getPickDTO.getPickTask());
							if(newPickUse == null){
								newPickUse = new PickInUse();
								newPickUse.setPickKey(getPickDTO.getPickDocRef()+getPickDTO.getPickTask());
							}
							newPickUse.setInUseBy(getPickDTO.getUserId());	
							newPickUse.setDateUpdated(new Date());
							newPickUse.setUpdatedBy(getPickDTO.getUserId());
							pickInUseRepo.save(newPickUse);
							List<OrderHeader> orderHeaders = orderHeaderRepository.findByPickHeader(picHeader);
							if (orderHeaders != null && orderHeaders.size() == 1) {
								List<String> prefixes = orderTypeRepository.findPrefixesForOrders();
								OrderType orderType = new OrderType();
								if (prefixes.contains(orderHeaders.get(0).getCustomerReference().substring(0, 2))) {
									orderType = orderTypeRepository.findByOrderTypePrefix(
											orderHeaders.get(0).getCustomerReference().substring(0, 2));
								} else if (prefixes
										.contains(orderHeaders.get(0).getCustomerReference().substring(0, 1))) {
									orderType = orderTypeRepository.findByOrderTypePrefix(
											orderHeaders.get(0).getCustomerReference().substring(0, 1));
								}
								if (orderType.getFixedToLocationId() != null) {
									pick.setToLocationCode(
											locationRepo.findById(orderType.getFixedToLocationId()).getLocationCode());
								}
							}
							// Mark the pick in use

							pick.setMessage("OK");
						} else {
							pick.setMessage("No tasks for Pick");
							logger.info("No tasks(pick details) for Pick :" + getPickDTO.getPickDocRef()
									+ " and Pick Task:" + getPickDTO.getPickTask());
						}
					}

				} else {
					pick.setMessage("No Stock for Pick:" + getPickDTO.getPickDocRef());
					logger.info("No Stock for Pick :" + getPickDTO.getPickDocRef());
				}
			}
		} else {
			pick.setMessage("Invalid Pick Header");
			logger.info("Invalid Pick Header:" + getPickDTO.getPickDocRef());
		}

		return pick;

	}

	@Transactional
	public PickDTO processPick(ProcessPickDTO pickDTO) throws Exception {
		PickDTO returnPick = new PickDTO();
		boolean returnSamePick = false;
		PickDetail pick = pickDetailRepository.findById(pickDTO.getPickId());
		boolean fullBoxPick = pick.getPart().getFullBoxPick();
		//I don't include non exist serial scanned flag here because its not needed here
		boolean serialScannedDiff = pick.getSerialReference().equalsIgnoreCase(pickDTO.getSerialScanned()) ? false: true;
		if (pickDTO.getQtyScanned() != null && pick.getQtyAllocated() != null
				&& pick.getQtyAllocated().doubleValue() > pickDTO.getQtyScanned().doubleValue() && !fullBoxPick) {
			returnSamePick = true;
		}
		if (pick.getProcessed()) {
			logger.info("Pick is already processed");
		} else {
			if (pickDTO.isSkip()) {
				logger.info("Skip pick true");
				pickDocumentInfo.updatePickSequenceForSkip(pick, pickDTO.getUserId());
			} else if (pickDTO.isMissingSerial()) {
				logger.info("Missing serial true");
				// Mark the pick qty allocated =0 in body which will force
				// allocation, mark inventory for that serial missing, record
				// transaction history
				pickDocumentInfo.updatePickForMissingSerial(pick, pickDTO.getUserId());
			} else {
				if (serialScannedDiff) {
					logger.info("Alternate serial :Serial scanned is different than allocated:");
					List<PickDetail> pickForScannedSerialList = pickDetailRepository
							.findBySerialReferenceAndPartNumber(pickDTO.getSerialScanned(),pick.getPartNumber());
					if (pickForScannedSerialList != null && !pickForScannedSerialList.isEmpty()) {
						logger.info("Alternate serial :Allocated");
						// Release all the pick details associated with the
						// serial
						for (PickDetail pickForSerialScanned : pickForScannedSerialList) {
							if (pickForSerialScanned.getPickBody().getId() == pick.getPickBody().getId()) {
								// this is the use case where its in the same
								// pick body
								pick = pickForSerialScanned;
								//re evaluate return same pick because we are swapping picks
								if (pickDTO.getQtyScanned() != null && pick.getQtyAllocated() != null
										&& pick.getQtyAllocated().doubleValue() > pickDTO.getQtyScanned().doubleValue() && !fullBoxPick) {
									returnSamePick = true;
								}else{
									returnSamePick = false;
								}
								if(fullBoxPick){
									logger.info("Alternate serial : Full box pick swapping the scanned qty with pick allocated qty");
									pickDTO.setQtyScanned(pick.getQtyAllocated());
								}else{
									//TODO For piece pick
									if (pickDTO.getQtyScanned().doubleValue() > pick.getQtyAllocated().doubleValue()
											&& pickDTO.getQtyScanned()
													.doubleValue() <= (pick.getQtyAllocated().doubleValue() + pick
															.getInventoryMaster().getAvailableQty().doubleValue())) {
										double diff = pickDTO.getQtyScanned() -  pick.getQtyAllocated().doubleValue();
										
										inventoryInfo.updateInventoryWithQty(pick.getInventoryMaster(), diff, pickDTO.getUserId());
										pick.setQtyAllocated(pick.getQtyAllocated().doubleValue() + diff);
										//TODO should we update pick order link qty for that pick detail
									}
								}
								logger.info(
										"alternate serial : The alt serial scanned is in same pick body just process that pick instead");
								serialScannedDiff = false;
								//break;
							} else {
								logger.info(
										"Alternate serial :Calling unallocating the picks this is differet pickBodies than the one being processed");
								pickDocumentInfo.unAllocatePickBody(null, pickForSerialScanned.getPickBody(),
										pickDTO.getUserId());
							}
						}
					}
				}
					logger.info("ProcessPick called..PickId " + pick.getId() + " - Document Ref"
							+ pick.getDocumentReference());

					try {
						pick = pickDocumentInfo.processPickDetail(pick, pickDTO, serialScannedDiff);
					} catch (Exception e) {
						logger.info("Exception is " + e.getMessage());
						pick = null;
						if (e instanceof ConcurrentException) {
							logger.info("Exception is DATA IN USE");
						} else if (e instanceof InventoryQtyNotAdequate) {
							logger.info("Exception is NOT ENOUGH INVENTORY");
						} else {
							logger.info("Exception is ERROR");
						}
						throw e;
					}
				

			}
		}

		if (pick != null) {
			returnPick.setProcessedPickId(pick.getId());
			if (!pickDTO.isSkip() && !pickDTO.isMissingSerial() &&  returnSamePick) {
				logger.info("Same pick being returned pick is " + pick.getId());
				returnPick = pickDocumentInfo.convertPickDetailEntityToView(pick);
			} else {
				// set the previous picks pick body to inuse  null
				PickBody pickBody = pick.getPickBody();
				pickBody.setInUseBy(null);
				pickBody.setDateUpdated(new Date());
				pickBodyRepository.save(pickBody);
				GetPickDTO getPick = new GetPickDTO();
				getPick.setPickDocRef(pick.getDocumentReference());
				String locTypeCode = pick.getPart().getFixedLocationType().getLocationTypeCode();
				if (locTypeCode.indexOf('-') != -1) {
					getPick.setPickTask(locTypeCode.substring(0, locTypeCode.indexOf('-')));
				} else {
					getPick.setPickTask(locTypeCode);
				}
				getPick.setAllocateRefresh(false);
				getPick.setUserId(pickDTO.getUserId());

				if (pickDTO.getTransactionCode().equalsIgnoreCase(ConstantHelper.TRANSACTION_CODE_PICK_TO_TROLLEY)) {
					ZoneDestination zone = zoneDestRepo.findByZoneDestination(pick.getZoneDestination().getZoneDestination());
					String trainLocation = pick.getDocumentReference() + " - "+ zone.getTrolley().getTrolley() ;
					getPick.setTrain(trainLocation);
				} else {
					getPick.setTrain(null);
				}
				if ((!pickDTO.isSkip() && serialScannedDiff) || pickDTO.isMissingSerial()) {
					// Force allocate refresh for a specific part in case of
					// different serial scanned and its not skip or it can be
					// missing serial
					logger.info("Force allocate refresh for " + pick.getDocumentReference() + "  and part Number "
							+ pick.getPartNumber());
					getPick.setPart(pick.getPartNumber());
				}
				
				returnPick = getPickDetail(getPick);
			}
		}
		logger.info(returnPick.toString());
		return returnPick;

	}

	@Transactional
	@Override
	public String ptttTransaction(String tagReference, String locationCode, String userId) {

		String message = "Fail  -Failed";
		logger.info("ptttTransaction " + tagReference);

		InventoryStatus inventotyStatus = invStatRepo.findByInventoryStatusCode(ConstantHelper.INVENTORY_STATUS_PICKED);
		List<InventoryMaster> inventoryMaster = inventoryRepository
				.findByTagReferenceAndInventoryStatusNotIn(tagReference, inventotyStatus);
		List<InventoryMaster> inventoryMasterWithNoStatus = inventoryRepository
				.findByTagReferenceAndInventoryStatusIsNull(tagReference);

		if (inventoryMaster.size() > 0 || inventoryMasterWithNoStatus.size() > 0) {
			return "FAIL  -All Serials are not picked";
		}

		List<InventoryMaster> inventoryForSameTag = inventoryRepository.findByTagReference(tagReference);


		for (InventoryMaster inventoryWithTag : inventoryForSameTag) {
			transInfo.recordTransaction(inventoryWithTag, locationCode, userId, ConstantHelper.TRANSACTION_CODE_PALLET_TO_TRAIN);
			inventoryInfo.updateInventoryLocationForPTTT(inventoryWithTag, locationCode, userId);
			message = "OK  -Successfully Transacted";

		}
		return message;

	}
	/***
	 * tagReference - serial Reference
	 */
	@Transactional
	@Override
	public String mttTransaction(String serialReference, String locationCode, String userId) {

		String message = "Fail  -Failed";

		logger.info("mttTransaction " + serialReference);

		InventoryStatus inventotyStatus = invStatRepo.findByInventoryStatusCode("PKD");
		List<InventoryMaster> inventoryMaster = inventoryRepository
				.findBySerialReferenceAndInventoryStatusNotIn(serialReference, inventotyStatus);
		List<InventoryMaster> inventoryMasterWithNoStatus = inventoryRepository
				.findBySerialReferenceAndInventoryStatusIsNull(serialReference);

		if (inventoryMaster.size() > 0 || inventoryMasterWithNoStatus.size() > 0) {
			return "FAIL  -All Serials are not picked";
		}

		List<InventoryMaster> inventoryForSameSerial = inventoryRepository.findBySerialReference(serialReference);

		for (InventoryMaster inventoryWithTag : inventoryForSameSerial) {

			inventoryInfo.updateInventoryLocation(inventoryWithTag, locationCode, userId);
			transInfo.recordTransaction(inventoryWithTag, locationCode, userId,
					ConstantHelper.TRANSACTION_CODE_MARSHAL_TO_TRAILER);
			message = "OK  -Successfully Transacted";

		}

		return message;

	}

}
