package com.vantec.pick.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.business.AllocationInfo;
import com.vantec.pick.business.InventoryInfo;
import com.vantec.pick.business.PickDocumentInfo;
import com.vantec.pick.business.TransactionInfo;
import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.PickBody;
import com.vantec.pick.entity.PickDetail;
import com.vantec.pick.entity.PickHeader;
import com.vantec.pick.entity.PickOrderLink;
import com.vantec.pick.entity.Tag;
import com.vantec.pick.repository.DocumentStatusRepository;
import com.vantec.pick.repository.InventoryMasterRepository;
import com.vantec.pick.repository.LocationRepository;
import com.vantec.pick.repository.OrderBodyRepository;
import com.vantec.pick.repository.OrderHeaderRepository;
import com.vantec.pick.repository.OrderTypeRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.repository.PickBodyRepository;
import com.vantec.pick.repository.PickDetailRepository;
import com.vantec.pick.repository.PickHeaderRepository;
import com.vantec.pick.repository.PickOrderLinkRepository;
import com.vantec.pick.repository.TagRepository;
import com.vantec.pick.repository.TrainRepository;
import com.vantec.pick.repository.ZoneDestinationRepository;
import com.vantec.pick.util.ConstantHelper;

@Service("dispatchService")
public class DispatchServiceImpl implements DispatchService {

	private static Logger logger = LoggerFactory.getLogger(DispatchServiceImpl.class);

	@Autowired
	OrderHeaderRepository orderHeaderRepository;

	@Autowired
	OrderBodyRepository orderBodyRepository;

	@Autowired
	PickHeaderRepository pickHeaderRepository;

	@Autowired
	PickDetailRepository pickDetailRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	PickDocumentInfo pickDocumentInfo;

	@Autowired
	AllocationInfo allocateInfo;

	@Autowired
	MessageService messageService;

	@Autowired
	OrderTypeRepository orderTypeRepository;

	@Autowired
	InventoryInfo inventoryInfo;

	@Autowired
	PickBodyRepository pickBodyRepository;

	@Autowired
	PartRepository partRepository;

	@Autowired
	InventoryMasterRepository inventoryRepository;

	@Autowired
	TrainRepository trainRepo;

	@Autowired
	TagRepository tagRepo;

	@Autowired
	ZoneDestinationRepository zoneDestRepo;

	@Autowired
	LocationRepository locationRepo;

	@Autowired
	PickOrderLinkRepository pickOrderLinkRepo;

	@Autowired
	TransactionInfo transInfo;

	@Autowired
	InventoryInfo invInfo;

	@Autowired
	AllocationService allocationService;

	@Override
	public String closeTrain(String trainLocation, String userId) throws Exception {

		logger.info("shipTheTrain called.. " + trainLocation);

		String pickRef = "";
		String train = "";
		List<String> orderRef = new ArrayList<>();
		if (trainLocation != null && !trainLocation.isEmpty()) {
			String[] subs = trainLocation.split("-");
			pickRef = subs[0].trim();
			train = subs[2].trim();
		}
		PickHeader pickHeader = pickHeaderRepository.findByDocumentReference(pickRef);
		// fetch all inventorys to dispatch
		List<InventoryMaster> inventorys = inventoryRepository.findBylocationCode(trainLocation);
		List<Long> pickIds = new ArrayList<Long>();
		for (InventoryMaster inventory : inventorys) {
			pickIds.add(inventory.getOriginatingDetailId());
		}

		DocumentStatus pickedstatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_PICKED);
		List<PickDetail> pickDetails = pickDetailRepository.findAllByIds(pickIds);

		List<OrderBody> orderBodies = new ArrayList<OrderBody>();
		List<PickOrderLink> pickOrders = pickOrderLinkRepo.findAllForPickDetails(pickDetails);
		for (PickOrderLink pickOrder : pickOrders) {
			OrderBody orderBody = pickOrder.getOrderBody();
			orderBodies.add(orderBody);
			orderRef.add(orderBody.getDocumentReference());
			if (orderBody.getDifference().doubleValue() == 0) {
				orderBody.setDocumentStatus(pickedstatus);
			}
			orderBodyRepository.save(orderBody);
		}

		messageService.sendPickedMessageForOrderBody(orderBodies, userId);
		List<String> noDupOrderRefs = orderRef.stream().distinct().collect(Collectors.toList());
		transInfo.recordCloseTrainTransaction(trainLocation, userId, 0D, pickRef, String.join(",", noDupOrderRefs));

		for (InventoryMaster inventory : inventorys) {
			// Delete Inventory
			invInfo.deleteInventory(inventory);
		}

		// Delete Pallet
		// fetch all tags
		List<Tag> tags = tagRepo.selectTagAfterDispatch(trainLocation);
		tagRepo.deleteInBatch(tags);
		logger.info("Shipping of the Train Done");
		logger.info("Creating AAA Picks");

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MINUTE, -5);
		Date dateUpdated = calendar.getTime();
		List<String> zones = trainRepo.findByTrain(train).getAllZonesNames();
		List<PickBody> pickBodies = pickBodyRepository.findUnProcessedBodiesByPickReferenceForTrain(pickRef, userId,
				dateUpdated, zones);
		PickHeader newPickHeader = pickDocumentInfo.createPickHeader(userId, ConstantHelper.AAA_ORDER_TYPE);
		for (PickBody pickBody : pickBodies) {
			pickDocumentInfo.copyPickBody(pickBody, newPickHeader, userId);
		}

		List<OrderHeader> orders = orderHeaderRepository.findByPickHeader(pickHeader);
		for (OrderHeader order : orders) {
			order.setPickHeader(newPickHeader);
			order.setDateUpdated(new Date());
			order.setUpdatedBy(userId);
			orderHeaderRepository.save(order);
		}

		allocationService.allocateWithPickReference(newPickHeader.getDocumentReference(), userId);
		// Check if any shortage are present .. if yes then check if there is
		// stock allocated in any other picks and release them
		List<PickBody> shortages = pickBodyRepository.findShortageBodiesByHeader(newPickHeader);
		boolean foundStock = false;
		if (shortages != null && shortages.size() > 0) {
			for (PickBody shortBody : shortages) {
				// find existing picks for parts
				double shortageQty = shortBody.getQtyShortage().doubleValue();
				List<PickBody> allocatedList = pickBodyRepository.findAllocatedPart(newPickHeader,
						shortBody.getPartNumber(), userId, dateUpdated);
				for (PickBody allocateBody : allocatedList) {
					foundStock = true;
					if (shortageQty <= 0) {
						break;
					}
					shortageQty=-allocateBody.getQtyAllocated().doubleValue();
					pickDocumentInfo.unAllocatePickBody(null, allocateBody, userId);
					
				}
				logger.info(shortBody.getPartNumber() + " " + shortBody.getQtyExpected() + "  "
						+ shortBody.getQtyAllocated() + "  " + shortBody.getQtyTransacted() + " "
						+ shortBody.getQtyShortage());
			}

		}
		if(foundStock){
			allocationService.allocateWithPickReference(newPickHeader.getDocumentReference(), userId);
		}

		return "OK";
	}

}
