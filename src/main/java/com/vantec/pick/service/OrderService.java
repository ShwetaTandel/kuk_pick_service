package com.vantec.pick.service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

public interface OrderService {
	
	public void readOrder(File file) throws IOException, ParseException, Exception;

}
