package com.vantec.pick.service;

import java.io.IOException;
import java.text.ParseException;

import com.vantec.pick.model.OrderHeaderDTO;

public interface OrderingService {

	///Create Order Records after reading Info from PIK file
	public void createOrder(OrderHeaderDTO customerOrder) throws ParseException, IOException, Exception;
	
	public void updateOrder(OrderHeaderDTO customerOrder) throws ParseException;
	
	public void deleteOrder(OrderHeaderDTO customerOrderDTO) throws ParseException,  IOException;


}
