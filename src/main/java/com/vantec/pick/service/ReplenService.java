package com.vantec.pick.service;

import com.vantec.pick.model.ReplenDTO;
import com.vantec.pick.model.ReplenResponseDTO;
import com.vantec.pick.model.ReplenScanDTO;

public interface ReplenService {

	Boolean createReplen(ReplenDTO replenDTO);

	Boolean editReplen(ReplenDTO replenDTO);

	Boolean deleteReplen(ReplenDTO replenDTO);

	String scanReplen(ReplenScanDTO replenScanDTO);

	ReplenResponseDTO findReplenTask(String replenGroupCode, String userId);

	String createPallet(String userId);

	Boolean isPalletFull(String tagReference, Long pickGroupId);

	Boolean transferReplen(Long replenTaskId, String userid);

	String getToLocationCodeForSerial(String serial, String partNumber);

	Boolean updateReplenWithScannedSerial(String tagReference, String serial, Long replenTaskId);

	Boolean processedReplen(String serial,String partNumber,String userId, String locationCode);

	Boolean validateScannedSerail(String serial, String partNumber, String locationCode);

	ReplenResponseDTO f6Replen(Long replenTaskId, String userId);

	Boolean f6Pick(ReplenDTO replenDTO);
}
