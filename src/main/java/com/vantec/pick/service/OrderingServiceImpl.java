package com.vantec.pick.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.business.OrderInfo;
import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.InspectionTaskHeader;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.OrderType;
import com.vantec.pick.entity.ZoneDestination;
import com.vantec.pick.model.AllocateDTO;
import com.vantec.pick.model.OrderBodyDTO;
import com.vantec.pick.model.OrderHeaderDTO;
import com.vantec.pick.repository.DocumentStatusRepository;
import com.vantec.pick.repository.InspectionTaskHeaderRepository;
import com.vantec.pick.repository.LocationTypeRepository;
import com.vantec.pick.repository.OrderBodyRepository;
import com.vantec.pick.repository.OrderHeaderRepository;
import com.vantec.pick.repository.OrderTypeRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.repository.ZoneDestinationRepository;
import com.vantec.pick.util.ConstantHelper;

@Service("orderingService")
public class OrderingServiceImpl implements OrderingService {

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	PartRepository partRepository;

	@Autowired
	LocationTypeRepository locTypeRepo;

	@Autowired
	OrderBodyRepository orderBodyRepository;

	@Autowired
	OrderHeaderRepository orderHeaderRepository;

	@Autowired
	ZoneDestinationRepository zoneDestinationRepository;
	@Autowired
	private InspectionTaskHeaderRepository inspectionTaskHeaderRepository;

	@Autowired
	OrderInfo orderInfo;

	@Autowired
	OrderTypeRepository orderTypeRepository;

	@Autowired
	AllocationService allocationService;
	

	@Autowired
	MessageService messageService;

	private static Logger logger = LoggerFactory.getLogger(OrderingServiceImpl.class);

	@Override
	@Transactional
	public void createOrder(OrderHeaderDTO customerOrderDTO) throws Exception {

		List<OrderBodyDTO> allOrderBodyDTOs = customerOrderDTO.getOrderBodyDTOs();
		HashMap<String, OrderHeaderDTO> allHeaders = new HashMap<String, OrderHeaderDTO>();

		// For each order divide the Order Header in parts

		for (OrderBodyDTO orderBodyDTO : allOrderBodyDTOs) {
			String orderType = checkOrderType(orderBodyDTO.getZoneDestination(),
					customerOrderDTO.getCustomerReference());
			String mapKey = "";
			if (orderType.equalsIgnoreCase(ConstantHelper.INSPECT_ORDER_TYPE)) {
				// INSPECT ORDERS get split my Cust Reference only
				mapKey = customerOrderDTO.getCustomerReference();
			} else {
				// ALL other order types are split my cust ref - order type -
				// expected delivery date
				String expectedDeliveryDate = orderBodyDTO.getExpectedDeliveryDate().toString().substring(0, 16);
				mapKey = customerOrderDTO.getCustomerReference() + orderType + expectedDeliveryDate;
			}
			if (allHeaders.containsKey(mapKey)) {
				OrderHeaderDTO orderHeader = allHeaders.get(mapKey);
				orderHeader.getOrderBodyDTOs().add(orderBodyDTO);
			} else {
				OrderHeaderDTO orderHeader = getOrderHeaderDTO(customerOrderDTO);
				orderHeader.getOrderBodyDTOs().add(orderBodyDTO);
				orderHeader.setOrderType(orderType);
				orderHeader.setExpectedDeliveryTime(orderBodyDTO.getExpectedDeliveryDate());
				allHeaders.put(mapKey, orderHeader);
			}

		}
		for (OrderHeaderDTO tmpOrderHeader : allHeaders.values()) {
			if (tmpOrderHeader.getOrderType().equalsIgnoreCase(ConstantHelper.INSPECT_ORDER_TYPE)) {
				// INSPECT ORDERS go in different tables - Inspect tables
				InspectionTaskHeader dbInspectTaskHeader = inspectionTaskHeaderRepository
						.findByCustomerReference(tmpOrderHeader.getCustomerReference());
				if (dbInspectTaskHeader == null) {
					dbInspectTaskHeader = orderInfo.createInpectTaskHeader(tmpOrderHeader);
				}
				List<OrderBodyDTO> orderBodyDTOs = tmpOrderHeader.getOrderBodyDTOs();

				for (OrderBodyDTO orderBodyDTO : orderBodyDTOs) {
					orderInfo.createInspectTaskBody(dbInspectTaskHeader, orderBodyDTO);
				}
				if(dbInspectTaskHeader.getInspectTaskBodies().size() == 0){
					logger.info("Deleteing Inspect Task header as no bodies created");
					inspectionTaskHeaderRepository.delete(dbInspectTaskHeader);
					continue;
				}

			} else {
				// ALL other order types
				boolean autoAllocate = false;
				OrderHeader dbOrderHeader = null;
				String[] orderType = tmpOrderHeader.getOrderType().split("-");
				List<String> prefixes = orderTypeRepository.findPrefixesForAutoAllocateOrders();
				if (prefixes.contains(tmpOrderHeader.getCustomerReference().substring(0, 1))
						|| prefixes.contains(tmpOrderHeader.getCustomerReference().substring(0, 2)) 
						|| orderTypeRepository.findByOrderTypeCode(orderType[0]).getAutoAllocate()) {
					autoAllocate = true;
				}
				List<OrderHeader> dbOrderHeaders = orderHeaderRepository
						.findByCustomerReferenceAndOrderTypeAndExpectedDeliveryTime(
								tmpOrderHeader.getCustomerReference(), tmpOrderHeader.getOrderType(),
								tmpOrderHeader.getExpectedDeliveryTime());
				if (dbOrderHeaders != null && !dbOrderHeaders.isEmpty()) {
					//This logic is to add proper version number in case of orders split across files
					int ordersCnt = dbOrderHeaders.size();
					dbOrderHeader = dbOrderHeaders.get(ordersCnt -1);
				}
				if (dbOrderHeader == null) {
					dbOrderHeader = orderInfo.createOrderHeader(tmpOrderHeader);
				} else {
					// This could be a case where orders is split across files
					// If the auto allocate is true create a new order with the
					// next version
					if(autoAllocate){
						logger.info(
								"Order with saem ref but new version being created " + dbOrderHeader.getDocumentReference()+ "  " + dbOrderHeader.getCustomerReference()
										+ "  " + dbOrderHeader.getMachineModel() + dbOrderHeader.getBaanSerial());
						tmpOrderHeader.setVersion(dbOrderHeader.getVersion()+1);
						dbOrderHeader =  orderInfo.createOrderHeader(tmpOrderHeader);
					}
				}
				

				List<OrderBodyDTO> orderBodyDTOs = tmpOrderHeader.getOrderBodyDTOs();

				for (OrderBodyDTO orderBodyDTO : orderBodyDTOs) {
					orderInfo.createOrderBody(dbOrderHeader, orderBodyDTO);
				}
				if(dbOrderHeader.getOrderBodies().size() == 0){
					logger.info(
							"Deleteing order header as no bodies created for " + dbOrderHeader.getDocumentReference()+ "  " + dbOrderHeader.getCustomerReference()
									+ "  " + dbOrderHeader.getMachineModel() + dbOrderHeader.getBaanSerial());
					orderHeaderRepository.delete(dbOrderHeader);
					continue;
				}

				if (autoAllocate) {
					logger.info("Order type prefix match. Go ahead and allocate order");
					AllocateDTO allocateDTO = new AllocateDTO();
					allocateDTO.getDocRef().add(dbOrderHeader.getDocumentReference());
					allocateDTO.setUserId(ConstantHelper.PIK_FILE_JOB);
					allocationService.allocateOrders(allocateDTO);
					logger.info("Order Allocated =" + dbOrderHeader.getDocumentReference());
				}

			}
		}

	}

	@Override
	public void updateOrder(OrderHeaderDTO customerOrderDTO) throws ParseException {

		for (OrderBodyDTO orderBodyDTO : customerOrderDTO.getOrderBodyDTOs()) {

			OrderBody dbOrderBody = findPartLine(orderBodyDTO.getPartNumber(), orderBodyDTO.getOrderLine(),
					customerOrderDTO.getCustomerReference());
			// TODO - Have to check either body or header status here
			if (dbOrderBody != null) {
				dbOrderBody.setQtyExpected(orderBodyDTO.getExpectedQty());
				dbOrderBody.setDifference(orderBodyDTO.getExpectedQty());
				dbOrderBody.setDateUpdated(new Date());
				dbOrderBody.setUpdatedBy(ConstantHelper.SYSUSER);
				orderBodyRepository.save(dbOrderBody);
			}
		}
	}
	
	@Override
	@Transactional
	public void deleteOrder(OrderHeaderDTO customerOrderDTO) throws ParseException, IOException {
		
		logger.info("Deleted Order called ");
		boolean sendCancelMessage = false;
		List<String> custRefs = new ArrayList<String>();
		DocumentStatus deletedStatus = documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_DELETED); 
		for (OrderBodyDTO orderBodyDTO : customerOrderDTO.getOrderBodyDTOs()) {
			if (orderBodyDTO.getOrderLine() == 0) {

				List<OrderHeader> headers = orderHeaderRepository
						.findByCustomerReference(customerOrderDTO.getCustomerReference());
				for (OrderHeader header : headers) {
					boolean canDelete = header.getDocumentStatus().getDocumentStatusCode().equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_OPEN);
					if(header.getDocumentStatus().getDocumentStatusCode().equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_ALLOCATED)){
						// UN allocated first
						 canDelete =  allocationService.unAllocate(header.getPickHeader().getDocumentReference(), ConstantHelper.SYSUSER);
					} else if (header.getDocumentStatus().getDocumentStatusCode()
							.equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_PICKED)
							|| header.getDocumentStatus().getDocumentStatusCode()
									.equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_TRANSMITTED)) {
						canDelete = false;
						logger.info("The order which is cancelled is already picked. ->"+header.getDocumentReference());
					}
					if(canDelete){
						header.setDocumentStatus(deletedStatus);
						header.setDateUpdated(new Date());
						header.setUpdatedBy(ConstantHelper.SYSUSER);
						orderHeaderRepository.save(header);
						if(!custRefs.contains(header.getCustomerReference())){
							custRefs.add(header.getCustomerReference());
						}
						sendCancelMessage = true;
						logger.info("Deleted Order Header id=" + header.getId());
					}
				}
			
			} else {
				List<OrderBody> dbOrderBodies = orderBodyRepository.findByLineNoAndCustomerReference(orderBodyDTO.getOrderLine(),
						customerOrderDTO.getCustomerReference());
				for (OrderBody dbOrderBody : dbOrderBodies){
					String headerStatus  = dbOrderBody.getOrderHeader().getDocumentStatus().getDocumentStatusCode();
					String bodyStatus = dbOrderBody.getDocumentStatus().getDocumentStatusCode();
					if (headerStatus.equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_OPEN)
							&& bodyStatus.equalsIgnoreCase(ConstantHelper.DOCUMENT_STATUS_OPEN)) {
						if (dbOrderBody != null) {
							if (orderBodyDTO.getExpectedQty() == 0) {
								dbOrderBody.setQtyExpected(0.0);
								dbOrderBody.setDifference(0.0);
								dbOrderBody.setDocumentStatus(deletedStatus);
								logger.info("Deleted Order Body id=" + dbOrderBody.getId());
							} else {
								dbOrderBody
										.setQtyExpected(dbOrderBody.getQtyExpected() - orderBodyDTO.getExpectedQty());
								dbOrderBody.setDifference(dbOrderBody.getQtyExpected());
								logger.info("Reduced qty for  Order Body id=" + dbOrderBody.getId());
							}
							dbOrderBody.setDateUpdated(new Date());
							dbOrderBody.setUpdatedBy(ConstantHelper.SYSUSER);
							orderBodyRepository.save(dbOrderBody);

						}
					}
				}
			}
		}
		if(sendCancelMessage && !custRefs.isEmpty()){
			messageService.sendCancelMessage(custRefs, ConstantHelper.SYSUSER);
		}
	}

	private OrderBody findPartLine(String partNumber, Integer lineNo, String customerOrderNumber) {

		return (orderBodyRepository.findByPartNumberAndLineNoAndCustomerReference(partNumber, lineNo,
				customerOrderNumber));

	}

	/**
	 * Check the sub location type of a part and determine the order type
	 * 
	 * @param partNumber
	 * @return
	 * @throws InvalidDataException
	 */
	private String checkOrderType(String zoneDestination, String custRef) {

		ZoneDestination zone = zoneDestinationRepository.findByZoneDestination(zoneDestination);
//		if(zone == null){
//			zone = zoneDestinationRepository.findByZoneDestination(zoneDestination.substring(0,2));
//		}
		OrderType oType = orderTypeRepository.findByOrderTypePrefix(custRef.substring(0, 1));
		if (oType == null) {
			oType = orderTypeRepository.findByOrderTypePrefix(custRef.substring(0, 2));
		}
		String orderType = "";
		if (oType != null) {
			orderType = oType.getOrderTypeCode();
		} else if (zone == null) {
			logger.info("Zone destination Not found "+ zoneDestination +" setting order type NOTFOUND");
			orderType = ConstantHelper.NOT_FOUND;
		} else {
			
			orderType = zone.getTrolley().getTrain().getOrderTypeCode()+'-'+zone.getTrolley().getTrain().getTrain();

		}
		return orderType;

	}

	private OrderHeaderDTO getOrderHeaderDTO(OrderHeaderDTO customerOrderHeaderDTO) {
		OrderHeaderDTO newOrderHeaderDTO = new OrderHeaderDTO();
		newOrderHeaderDTO.setTypeIndicator(customerOrderHeaderDTO.getTypeIndicator());
		newOrderHeaderDTO.setOrigin(customerOrderHeaderDTO.getOrigin());
		newOrderHeaderDTO.setBaanSerial(customerOrderHeaderDTO.getBaanSerial());
		newOrderHeaderDTO.setCustomerReference(customerOrderHeaderDTO.getCustomerReference());
		newOrderHeaderDTO.setMachineModel(customerOrderHeaderDTO.getMachineModel());
		newOrderHeaderDTO.setExpectedDeliveryTime(customerOrderHeaderDTO.getExpectedDeliveryTime());
		newOrderHeaderDTO.setVersion(0L);
		return newOrderHeaderDTO;

	}

}
