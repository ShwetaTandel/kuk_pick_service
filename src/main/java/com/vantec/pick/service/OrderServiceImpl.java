package com.vantec.pick.service;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.entity.DownloadFile;
import com.vantec.pick.model.OrderHeaderDTO;
import com.vantec.pick.repository.DownloadFileRepository;
import com.vantec.pick.util.ConstantHelper;
import com.vantec.pick.util.OrderFileHandlerHelper;

@Service("orderService")
public class OrderServiceImpl implements OrderService {


	@Autowired
	OrderingService orderingservice;
	
	@Autowired
	DownloadFileRepository downloadFileRepo;
	
	/***
	 * Read the PIK file and create DB records
	 * @throws Exception 
	 * @throws InvalidDataException 
	 */
	@Override
	public void readOrder(File file) throws Exception {
		List<OrderHeaderDTO> orders = OrderFileHandlerHelper.readPIKFile(file);
		recordFileRead(file.getName(), ConstantHelper.PIK_FILE_JOB, ConstantHelper.SYSUSER);
		for (OrderHeaderDTO orderDTO : orders) {
			if(orderDTO.getTypeIndicator().equalsIgnoreCase(ConstantHelper.PICK_ADD_INDICATOR)){
				orderingservice.createOrder(orderDTO);
			}else if(orderDTO.getTypeIndicator().equalsIgnoreCase(ConstantHelper.PICK_CHANGE_INDICATOR)){
				orderingservice.updateOrder(orderDTO);
			}else if(orderDTO.getTypeIndicator().equalsIgnoreCase(ConstantHelper.PICK_DELETE_INDICATOR)){
				orderingservice.deleteOrder(orderDTO);
				
			}
		}
	}
	
	/****
	 * @param filename
	 * 
	 * Update the file name processed in DB
	 */
	private void recordFileRead(String fileName, String type, String user){
		DownloadFile file = new DownloadFile();
		file.setCreatedBy(user);
		file.setDateCreated(new Date());
		file.setFilename(fileName);
		file.setType(type);
		downloadFileRepo.save(file);		
		
	}
}


