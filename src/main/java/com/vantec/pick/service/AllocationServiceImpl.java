package com.vantec.pick.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.pick.business.AllocationInfo;
import com.vantec.pick.business.InventoryInfo;
import com.vantec.pick.business.PickDocumentInfo;
import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.PickBody;
import com.vantec.pick.entity.PickDetail;
import com.vantec.pick.entity.PickHeader;
import com.vantec.pick.entity.PickOrderLink;
import com.vantec.pick.model.AllocateDTO;
import com.vantec.pick.repository.DocumentStatusRepository;
import com.vantec.pick.repository.InventoryMasterRepository;
import com.vantec.pick.repository.LocationRepository;
import com.vantec.pick.repository.OrderBodyRepository;
import com.vantec.pick.repository.OrderHeaderRepository;
import com.vantec.pick.repository.OrderTypeRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.repository.PickBodyRepository;
import com.vantec.pick.repository.PickDetailRepository;
import com.vantec.pick.repository.PickHeaderRepository;
import com.vantec.pick.repository.PickOrderLinkRepository;
import com.vantec.pick.repository.TrainRepository;
import com.vantec.pick.repository.ZoneDestinationRepository;
import com.vantec.pick.util.ConstantHelper;

@Service("allocationService")
public class AllocationServiceImpl implements AllocationService {

	private static Logger logger = LoggerFactory.getLogger(AllocationServiceImpl.class);

	@Autowired
	OrderHeaderRepository orderHeaderRepository;

	@Autowired
	OrderBodyRepository orderBodyRepository;

	@Autowired
	PickHeaderRepository pickHeaderRepository;

	@Autowired
	PickDetailRepository pickDetailRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	PickDocumentInfo pickDocumentInfo;

	@Autowired
	AllocationInfo allocateInfo;

	@Autowired
	MessageService messageService;

	@Autowired
	OrderTypeRepository orderTypeRepository;

	@Autowired
	InventoryInfo inventoryInfo;

	@Autowired
	PickBodyRepository pickBodyRepository;

	@Autowired
	PartRepository partRepository;

	@Autowired
	InventoryMasterRepository inventoryRepository;

	@Autowired
	TrainRepository trainRepo;

	@Autowired
	ZoneDestinationRepository zoneDestRepo;

	@Autowired
	LocationRepository locationRepo;

	@Autowired
	PickOrderLinkRepository pickOrderLinkRepo;
	
	@Autowired
	PickOrderLinkRepository pickOrderLikRepo;

	/****
	 * FROM SCREEN Creates PickHeaders
	 * 
	 * @param allocateDTO
	 * @return
	 * @throws Exception 
	 */
	@Transactional
	@Override
	public Boolean allocateOrders(AllocateDTO allocateDTO) throws Exception {

		logger.info("Allocate documentReferenceCodes " + allocateDTO.getAllDocRef());
		Boolean isAllocated = false;
		PickHeader pickHeader = null;
		String pickType = "";
		List<OrderHeader> orderHeaders = new ArrayList<OrderHeader>();
		List<String> orderTypes = new ArrayList<String>();
		for (String documentReferenceCode : allocateDTO.getDocRef()) {
			OrderHeader orderHeader = orderHeaderRepository.findByDocumentReference(documentReferenceCode);
			orderHeaders.add(orderHeader);
			orderTypes.add(orderHeader.getOrderType());
			if (orderHeader.getPickHeader() != null) {
				logger.info("The order has pickHeader already using the same pick header");
				pickHeader = orderHeader.getPickHeader();
			}
		}
		List<String> noDupOrderTypes = orderTypes.stream().distinct().collect(Collectors.toList());
		if (noDupOrderTypes.size() == 1) {
			pickType = noDupOrderTypes.get(0);
		} else {
			pickType = ConstantHelper.MIXED_ORDER_TYPE;
		}

		if (pickHeader == null) {
			pickHeader = pickDocumentInfo.createPickHeader(allocateDTO.getUserId(), pickType);
		}
		// Create the pick bodies
		pickDocumentInfo.createPickBodies(pickHeader, orderHeaders, allocateDTO.getUserId());
		// PickHeader pickHeader =
		// pickDocumentInfo.findOrBuildPickDocument(orderHeaders,
		// allocateDTO.getUserId());
		// pass null for part
		isAllocated = allocateOrder1(allocateDTO.getUserId(), orderBodyRepository.findAllOpenBodies(pickHeader.getId()), pickHeader);
		//isAllocated = allocateOrder(allocateDTO.getUserId(), orderHeaders, pickHeader, false, false, null);

		for (OrderHeader orderHeader : orderHeaders) {
			updateDocumentStatus(orderHeader, "ALLOCATED", allocateDTO.getUserId());
		}
		// Mark all the pick details as Processed true if the parts are marked
		// autopick
		pickDocumentInfo.processAutoPick(pickHeader, allocateDTO.getUserId());
		// Send message if any picks are marked processes
		//messageService.sendPickedMessage(orderHeaders, allocateDTO.getUserId());

		return isAllocated;
	}

	/****
	 * FROM RDT For Allocate refresh
	 * 
	 * @param documentReference
	 * @param userId
	 * @return
	 * @throws Exception 
	 */
	@Transactional
	@Override
	public Boolean allocateWithPickReference(String documentReference, String userId) throws Exception {
		logger.info("allocateWithPickReference Pickreference " + documentReference);
		Boolean isAllocated = false;
		PickHeader pickHeader = pickHeaderRepository.findByDocumentReference(documentReference);
		if (pickHeader == null) {
			return false;
		}
		List<OrderHeader> orderHeaders = orderHeaderRepository.findByPickHeader(pickHeader);
		isAllocated = allocateOrder1(userId, orderBodyRepository.findAllOpenBodies(pickHeader.getId()), pickHeader);
		//isAllocated = allocateOrder(userId, orderHeaders, pickHeader, false, false, null);
		if (isAllocated) {
			for (OrderHeader orderHeader : orderHeaders) {
				updateDocumentStatus(orderHeader, "ALLOCATED", userId);
			}

			// Mark all the pick details as Processed true if the parts are
			// marked
			// autopick
			pickDocumentInfo.processAutoPick(pickHeader, userId);
			// Send message if any picks are marked processes
			//messageService.sendPickedMessage(orderHeaders, userId);

		}
		logger.info("allocateWithPickReference Pickreference done" + documentReference);
		return isAllocated;
	}

	/****
	 * FROM RDT/ receiving service For Allocate Refresh
	 * 
	 * @param parts
	 *            List
	 * @param userId
	 * @return
	 * @throws Exception 
	 */
	public Boolean allocateForParts(List<String> parts, String userId) throws Exception {

		// check if there are any shortages
		List<String> pickRefs = pickBodyRepository.checkShortagesForOrders(parts);
		if (pickRefs != null && pickRefs.size() > 0) {
			logger.info("Shortages found picks with parts. Calling allocation");
			for (String pickRef : pickRefs) {
				logger.info("allocate Pick : Pick Reference is  " + pickRef);
				Boolean isAllocated = false;
				PickHeader pickHeader = pickHeaderRepository.findByDocumentReference(pickRef);
				if (pickHeader == null) {
					return false;
				}
				List<OrderHeader> orderHeaders = orderHeaderRepository.findByPickHeader(pickHeader);
				isAllocated = allocateOrder1(userId, orderBodyRepository.findAllOpenBodiesForParts(pickHeader.getId(), parts), pickHeader);
				//isAllocated = allocateOrder(userId, orderHeaders, pickHeader, true, false, parts);
				if (isAllocated) {
					for (OrderHeader orderHeader : orderHeaders) {
						updateDocumentStatus(orderHeader, "ALLOCATED", userId);
					}
					// Mark all the pick details as Processed true if the parts
					// are marked
					// autopick
					pickDocumentInfo.processAutoPick(pickHeader, userId);
					// Send message if any picks are marked processes
					//messageService.sendPickedMessage(orderHeaders, userId);

				}
			}
		}
		return true;

	}

	/****
	 * from Pick process For allocate Refresh
	 * 
	 * @param parts
	 *            List
	 * @param userId
	 * @return
	 * @throws IOException
	 */
	public Boolean allocateRefreshForPickTasks(List<String> tasks, String userId, PickHeader pickHeader) throws IOException , Exception{

		logger.info("allocateRefreshForPickTasks pick reference " + pickHeader.getDocumentReference());
		Boolean isAllocated = false;
		//PickHeader pickHeader = pickHeaderRepository.findByDocumentReference(pickRef);
	
		List<OrderHeader> orderHeaders = orderHeaderRepository.findByPickHeader(pickHeader);
		isAllocated = allocateOrder1(userId, orderBodyRepository.findAllOpenBodiesForPickTasks(pickHeader.getId(), tasks), pickHeader);
		//isAllocated = allocateOrder(userId, orderHeaders, pickHeader, false, true, tasks);
		if (isAllocated) {
			for (OrderHeader orderHeader : orderHeaders) {
				updateDocumentStatus(orderHeader, "ALLOCATED", userId);
			}
			// Mark all the pick details as Processed true if the parts
			// are marked
			// autopick
			//Un Commenting this here when RDT calls refresh as not needed 
			pickDocumentInfo.processAutoPick(pickHeader, userId);
			// Send message if any picks are marked processes
			//messageService.sendPickedMessage(orderHeaders, userId);

		}
		return true;

	}

	/***
	 * Core Allocation method called from all allocate methods
	 * 
	 * @param userId
	 * @param orderHeaders
	 * @param pickHeader
	 * @param checkPart
	 *            - used when only to allocate some bodies with specific parts
	 * @return
	 * @throws IOException
	 */
	private Boolean allocateOrder(String userId, List<OrderHeader> orderHeaders, PickHeader pickHeader,
			boolean checkPart, boolean checkPickTask, List<String> values) throws IOException {

		Boolean isAllocated = false;
		for (OrderHeader orderHeader : orderHeaders) {
			for (OrderBody orderBody : orderHeader.getOrderBodies()) {
				boolean isPickTask = checkPickTask == false ? true
						: values != null && values.contains(orderBody.getPart().getFixedLocationType().getTaskGroup());
				boolean isPart = checkPart == false ? true
						: values != null && values.contains(orderBody.getPartNumber());
				Integer bodyAllocQty = pickOrderLikRepo.getAllocatedForOrderBody(orderBody);
				if (!orderBody.getBlocked()
						&& orderBody.getQtyExpected().doubleValue() > bodyAllocQty.doubleValue() + orderBody.getQtyTransacted().doubleValue()
						&& isPickTask && isPart) {
					logger.info("allocateOrder 1->"+orderBody.toString());
					List<InventoryMaster> inventorys = null;
					isAllocated = true;
					inventorys = inventoryInfo.filterInventoryForAllocation(orderBody);
					PickBody pickBody = pickBodyRepository.findByPickHeaderAndPartNumberAndZoneDestinationAndProcessed(
							pickHeader, orderBody.getPartNumber(), orderBody.getZoneDestination(), false);
					if (pickBody != null && pickBody.getQtyShortage().doubleValue() > 0) {
						logger.info("allocateOrder 2->"+pickBody.toString());						
						allocateInfo.allocateInventory(inventorys, pickBody, orderBody, userId);
					}
				} else if (orderBody.getBlocked()) {
					logger.info("Order body is blocked id =" + orderBody.getId());
				}
			}
		}
		return isAllocated;
	}
	
	/***
	 * Core Allocation method called from all allocate methods
	 * 
	 * @param userId
	 * @param orderHeaders
	 * @param pickHeader
	 * @param checkPart
	 *            - used when only to allocate some bodies with specific parts
	 * @return
	 * @throws IOException
	 */
	private Boolean allocateOrder1(String userId, List<OrderBody> orderBodies, PickHeader pickHeader) throws IOException {

		Boolean isAllocated = false;
			for (OrderBody orderBody : orderBodies) {
				Integer bodyAllocQty = pickOrderLikRepo.getAllocatedForOrderBody(orderBody);
				if (orderBody.getQtyExpected().doubleValue() > bodyAllocQty.doubleValue() + orderBody.getQtyTransacted().doubleValue()) {
					//logger.info("allocateOrder 1->"+orderBody.toString());
					List<InventoryMaster> inventorys = null;
					isAllocated = true;
					inventorys = inventoryInfo.filterInventoryForAllocation(orderBody);
					PickBody pickBody = pickBodyRepository.findByPickHeaderAndPartNumberAndZoneDestinationAndProcessed(
							pickHeader, orderBody.getPartNumber(), orderBody.getZoneDestination(), false);
					if (pickBody != null && pickBody.getQtyShortage().doubleValue() > 0) {
					//	logger.info("allocateOrder 2->"+pickBody.toString());	
						if(inventorys!=null && !inventorys.isEmpty())
							allocateInfo.allocateInventory(inventorys, pickBody, orderBody, userId);
					}
				} else if (orderBody.getBlocked()) {
					logger.info("Order body is blocked id =" + orderBody.getId());
				}
			}
		return isAllocated;
	}


	/***
	 * From screen
	 */
	@Override
	public Boolean unAllocate(String documentReference, String userId) {

		Boolean isDeleted = false;

		PickHeader pickHeader = pickHeaderRepository.findByDocumentReference(documentReference);

		if (pickHeader != null) {
			List<PickDetail> pickDetailsScanned = pickDetailRepository
					.findByDocumentReferenceNotPicked(documentReference);

			logger.info("pickDetailsScanned " + pickDetailsScanned);

			if (pickDetailsScanned.isEmpty() || pickDetailsScanned.size() == 0) {

				logger.info("no serial scanned ");
				List<PickBody> pickBodies = pickHeader.getPickBodies();

				for (PickBody pickBody : pickBodies) {

					List<PickDetail> pickDetails = pickBody.getPickDetails();
					for (PickDetail pickDetail : pickDetails) {

						inventoryInfo.releaseInventory(pickDetail);
						List<PickOrderLink> links = pickOrderLinkRepo.findByPickDetail(pickDetail);
						pickOrderLinkRepo.delete(links);
						pickDetailRepository.delete(pickDetail);
					}
					pickBodyRepository.delete(pickBody);
				}

				List<OrderHeader> orderHeaders = orderHeaderRepository.findByPickHeader(pickHeader);
				for (OrderHeader orderHeader : orderHeaders) {
					orderHeader.setPickHeader(null);
					updateDocumentStatus(orderHeader, "OPEN", userId);
				}
				pickHeaderRepository.delete(pickHeader);
				isDeleted = true;

			}
		}
		return isDeleted;
	}

	private Boolean updateDocumentStatus(OrderHeader orderHeader, String status, String userId) {
		DocumentStatus documentStatus = documentStatusRepository.findByDocumentStatusCode(status);
		orderHeader.setDocumentStatus(documentStatus);
		orderHeader.setUpdatedBy(userId);
		orderHeader.setDateUpdated(new Date());
		return orderHeaderRepository.save(orderHeader) != null;
	}

	
	
	
	@Override
	public Boolean checkInventory(Long orderBodyId) throws IOException{
		
		List<InventoryMaster> list = inventoryInfo.filterInventoryForAllocation(orderBodyRepository.findById(orderBodyId));
		for(InventoryMaster i : list){
				logger.info("Id is ="+i.getId());
		}
		return true;
	}

}
