package com.vantec.pick.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.business.ReplenInfo;
import com.vantec.pick.entity.F6MissingHistory;
import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.InventoryStatus;
import com.vantec.pick.entity.Location;
import com.vantec.pick.entity.Part;
import com.vantec.pick.entity.PickGroup;
import com.vantec.pick.entity.ReplenTask;
import com.vantec.pick.model.ReplenDTO;
import com.vantec.pick.model.ReplenResponseDTO;
import com.vantec.pick.model.ReplenScanDTO;
import com.vantec.pick.repository.F6MissingHistoryRepository;
import com.vantec.pick.repository.InventoryMasterRepository;
import com.vantec.pick.repository.LocationRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.repository.PickGroupRepository;
import com.vantec.pick.repository.ReplenTaskRepository;
import com.vantec.pick.repository.ReplenTransferHistoryRepository;

@Service("replenService")
public class ReplenServiceImpl implements ReplenService{

	private static Logger logger = LoggerFactory.getLogger(ReplenServiceImpl.class);
	
	@Autowired
	LocationRepository locationRepository;
	
	@Autowired
	InventoryMasterRepository inventoryMasterRepository;
	
	@Autowired
	ReplenInfo replenInfo;
	
	@Autowired
	ReplenTransferHistoryRepository replenTransferHistoryRepository;
	
	@Autowired
	ReplenTaskRepository replenTaskRepository;
	
	@Autowired
	PickGroupRepository pickGroupRepository;
	
	@Autowired
	PartRepository partRepository;
	
	
	@Autowired
	F6MissingHistoryRepository f6MissingHistoryRepository;
	
	
	
	
	
	@Override
	public Boolean createReplen(ReplenDTO replenDTO) {
		
		
		Boolean isReplenCreated = false;
		
		Location location = locationRepository.findByLocationCode(replenDTO.getLocationCode());
			
		Boolean isPickGroupCheck = replenInfo.pickGroupCheck(location);
		
		Boolean isPickFaceQtyCheck = replenInfo.pickFaceQtyCheck(location);
		
		//InventoryMaster inventory = replenInfo.filterInventoryForReplen(replenDTO.getPartNumber());
		
		
		
		logger.info("isPickGroupCheck" + isPickGroupCheck);
		logger.info("isPickFaceQtyCheck" + isPickFaceQtyCheck);
		
	    if(isPickGroupCheck && isPickFaceQtyCheck){
	    	isReplenCreated = replenInfo.createReplenTask(replenDTO,location);
	    }
		

		
		return isReplenCreated;
	}
	
	
	@Override
	public Boolean editReplen(ReplenDTO replenDTO) {
		
		ReplenTask replenTask = replenTaskRepository.findById(replenDTO.getReplenTaskId());
		
		if(("").equalsIgnoreCase(replenTask.getInUse()) || 
				replenTask.getInUse().equals(null) ||
				replenTask.getInUse().equalsIgnoreCase(replenDTO.getUserId()) ||
				replenTask.getScannedSerial().equalsIgnoreCase("") ||
				replenTask.getScannedSerial().equalsIgnoreCase(null)){
			replenInfo.createReplenHistory(replenDTO,replenTask);
			
			replenInfo.updateLocationWithPickGoup(replenDTO.getNewLocationCode(),replenDTO.getPickGroupId());
			
			return replenInfo.editReplenTask(replenDTO,replenTask);
		}
		
		return false;
	}


	@Override
	public Boolean deleteReplen(ReplenDTO replenDTO) {
		ReplenTask replenTask = replenTaskRepository.findById(replenDTO.getReplenTaskId());
		
		if(("").equalsIgnoreCase(replenTask.getInUse()) || replenTask.getInUse() == null ||replenTask.getInUse().equalsIgnoreCase(replenDTO.getUserId())){
			//replenInfo.createReplenHistory(replenDTO,replenTask);
			
			if(replenTask.getInventoryMasterId() != null){
				replenInfo.freeInventoryFromTag(replenTask.getInventoryMasterId());
			}
			
			
			replenTask.setProcessed(true);
			replenTask.setUpdatedBy(replenDTO.getUserId());
			replenTask.setDateUpdated(new Date());
			
			return replenTaskRepository.save(replenTask)!=null;
		
		}else{
			return false;
		}
	}


	@Override
	public String scanReplen(ReplenScanDTO replenScanDTO) {
		
		String message = "Fail";
		
		ReplenTask replenTask = replenTaskRepository.findById(replenScanDTO.getReplenTaskId());
		Part part = partRepository.findById(replenTask.getPartId());
		Location location = locationRepository.findByLocationCode(replenScanDTO.getLocationCode());
		InventoryMaster inventory = inventoryMasterRepository.findByPartNumberAndSerialReference(part.getPartNumber(), replenScanDTO.getScannedSerial());
		
		if(inventory!= null && !("OK").equalsIgnoreCase(inventory.getInventoryStatus().getInventoryStatusCode())){
			return "Fail Incorrect Inventory Status";
		}else if(location != null &&  location.getId() != inventory.getCurrentLocation().getId()){
			return "Fail Incorrect Location Code";
		}else if(replenTask != null && replenTask.getScannedSerial() != null){
			return "Fail Replen Task Already complete";
		}else{
			logger.info("doing all updateion");
			//replenInfo.createReplenHistory(replenDTO , replenTask);
			replenInfo.createTransactionHistory(replenScanDTO,replenTask,inventory,part);
			replenInfo.updateInventory(inventory, replenScanDTO);
			replenInfo.updateReplenTask(replenTask,replenScanDTO);
			
			
			Boolean flag = isPalletFull(replenScanDTO.getTagReference(),replenScanDTO.getPickGroupId());
			logger.info("isPalletFull " + flag);
            if(flag){
                  message = "Pallet is full";
            }else{
                  message = "Pallet is not full";
            }
            return message;
		}
		
		
	}


	@Override
	public ReplenResponseDTO findReplenTask(String replenGroupCode,String userId) {
		
		
		
		ReplenResponseDTO replenResponseDTO = new ReplenResponseDTO();
		PickGroup pickGroup = pickGroupRepository.findByPickGroupCode(replenGroupCode);
		
		if(pickGroup != null){
			List<ReplenTask> replenTasks = replenInfo.findReplenTask(pickGroup.getId());
			
			for(ReplenTask replenTask:replenTasks){
				logger.info("replenTask " + replenTask.getId());
				if(replenTask.getInUse() == null || userId.equalsIgnoreCase(replenTask.getInUse()) || ("").equalsIgnoreCase(replenTask.getInUse())){
					logger.info("can be updated");
					Part part = partRepository.findById(replenTask.getPartId());
					InventoryMaster inventory = replenInfo.filterInventoryForReplen(part.getPartNumber());
					if(inventory == null){
						if(replenTask.getPalletReference()==null || ("").equalsIgnoreCase(replenTask.getPalletReference())){
							logger.info("INVENTORY NULL FOR PART " + part.getPartNumber());
							replenInfo.updateReplenWithInventory(replenTask, null , userId);
						}
					}else{
						logger.info("INVENTORY PRESENT  FOR PART " + part.getPartNumber());
						replenInfo.updateReplenWithInventory(replenTask,inventory,userId);
						replenResponseDTO.setReplenTaskId(replenTask.getId());
						replenResponseDTO.setFromlocationCode(inventory.getLocationCode());
						replenResponseDTO.setPartNumber(part.getPartNumber());
						replenResponseDTO.setSerialReference(inventory.getSerialReference());
						break;
					}
				}
			}
		}else{
				replenResponseDTO.setReason("No pick group");
		}
		return replenResponseDTO;
	}


	@Override
	public String createPallet(String userId) {
		return replenInfo.createVirtualPlt("REP",userId);
	}


	@Override
	public Boolean isPalletFull(String tagReference,Long pickGroupId) {
		
		
		Boolean isPalletFull = false;
		PickGroup pickGroup = pickGroupRepository.findById(pickGroupId);
		List<ReplenTask> replens = replenTaskRepository.findByPalletReference(tagReference);
		
		
		if(replens.size() >= pickGroup.getMaxCaseQty()){
			isPalletFull = true;
		}
		
		return isPalletFull;
	}


	@Override
	public Boolean transferReplen(Long replenTaskId,String userId) {
		
		ReplenTask replenTask = replenTaskRepository.findById(replenTaskId);
		PickGroup pickgroup = pickGroupRepository.findById(replenTask.getPickGroupId());
		PickGroup altPickgroup = pickGroupRepository.findByPickGroupCode(pickgroup.getAltPickGroupCode());
		
		ReplenDTO replenDTO = new ReplenDTO();
		replenDTO.setPickGroupId(altPickgroup.getId());
		replenDTO.setUserId(userId);
		replenDTO.setNewLocationCode(replenTask.getToLocationCode());
		//replenTask.setFromLocationCode(replenTask.getToLocationCode());
		replenInfo.createReplenHistory(replenDTO , replenTask);
		replenInfo.updateLocationWithPickGoup(replenTask.getToLocationCode(),altPickgroup.getId());
		
		return replenInfo.transferReplenTask(replenTask,userId,altPickgroup) != null;
	}


	@Override
	public String getToLocationCodeForSerial(String serial,String partNumber) {
		logger.info("getToLocationCodeForSuggestedSerial " + serial);
		String toLocationCode = "";
		Part part = partRepository.findByPartNumber(partNumber);
		ReplenTask replenTask = replenTaskRepository.findByScannedSerialAndPartIdAndProcessed(serial,part.getId(),false);
		if(replenTask!=null){
			toLocationCode = replenTask.getToLocationCode();
		}
		logger.info("toLocationCode " + toLocationCode);
		return toLocationCode;
	}


	@Override
	public Boolean updateReplenWithScannedSerial(String tagReference, String serial, Long replenTaskId) {
		Boolean isUpdated = false;
		ReplenTask replenTask = replenTaskRepository.findById(replenTaskId);
		PickGroup pickGroup = pickGroupRepository.findById(replenTask.getPickGroupId());
		List<ReplenTask> replens = replenTaskRepository.findByPalletReference(tagReference);
		if(replens!=null && replens.size() >= pickGroup.getMaxCaseQty()){
			isUpdated = false;
		}else{
			replenInfo.updateReplenWithScannedSerial(replenTask,tagReference,serial);
			isUpdated = true;
		}
		return isUpdated;
	}


	@Override
	public Boolean processedReplen(String serial,String partNumber,String userId,String locationCode) {
		
		logger.info("partnumber " + partNumber);
		logger.info("serial " + serial);
		logger.info("locationCode " + locationCode);
		
		InventoryMaster inventory = inventoryMasterRepository.findByPartNumberAndSerialReference(partNumber,serial);
		if(inventory != null){
			Part part = partRepository.findByPartNumber(partNumber);
			ReplenTask replenTask = replenTaskRepository.findByScannedSerialAndPartIdAndProcessed(serial, part.getId(), false);
			
			replenInfo.createTransactionHistoryREPLOC(userId,inventory,locationCode,replenTask);
			replenInfo.updateInventoryWithStatusOK(inventory, locationCode,userId);
			
			
			replenTask.setProcessed(true);
			replenTask.setUpdatedBy(userId);
			replenTask.setDateUpdated(new Date());
			return replenTaskRepository.save(replenTask)!=null;
		}else{
			logger.info("process reple not possible no iventory ");
			return false;
		}
		

	}


	@Override
	public Boolean validateScannedSerail(String serial, String partNumber,String locationCode) {
		
		Boolean flag = false;
		InventoryMaster inventory = inventoryMasterRepository.findByPartNumberAndSerialReference(partNumber,serial);
		Location location = locationRepository.findByLocationCode(locationCode);
		if(inventory!= null && 
				("OK").equalsIgnoreCase(inventory.getInventoryStatus().getInventoryStatusCode()) && 
				location != null &&
				location.getId() == inventory.getCurrentLocation().getId())
		{
			flag = true;
		}
		
		
		return flag;
		
	}


	@Override
	public ReplenResponseDTO f6Replen(Long replenTaskId, String userId) {
		
		logger.info("f6Replen");
		ReplenResponseDTO replenResponseDTO = new ReplenResponseDTO();
		ReplenTask replenTask = replenTaskRepository.findById(replenTaskId);
		PickGroup pickGroup = pickGroupRepository.findById(replenTask.getPickGroupId());
		Part part = partRepository.findById(replenTask.getPartId());
		InventoryMaster missingInventory = inventoryMasterRepository.findById(replenTask.getInventoryMasterId());
		
		
		replenInfo.updateInventoryWithStatusMISSING(missingInventory, userId);
		replenInfo.recordF6MissingHistory(replenTask, missingInventory , userId);
		
		InventoryMaster filterInventory = replenInfo.filterInventoryForReplen(part.getPartNumber());
		
		if(filterInventory == null){
			logger.info("INVENTORY NULL FOR PART " + part.getPartNumber());
			if(replenTask.getPalletReference()==null || ("").equalsIgnoreCase(replenTask.getPalletReference()))
			{
				replenInfo.updateReplenWithInventory(replenTask, null , userId);
			}
			replenResponseDTO = findReplenTask(pickGroup.getPickGroupCode(),userId);
		}else{
			logger.info("INVENTORY PRESENT  FOR PART " + part.getPartNumber());
			replenInfo.updateReplenWithInventory(replenTask,filterInventory,userId);
			replenResponseDTO.setReplenTaskId(replenTask.getId());
			replenResponseDTO.setFromlocationCode(filterInventory.getLocationCode());
			replenResponseDTO.setPartNumber(part.getPartNumber());
			replenResponseDTO.setSerialReference(filterInventory.getSerialReference());
		}
		return replenResponseDTO;
	}


	@Override
	public Boolean f6Pick(ReplenDTO replenDTO) {
		
        Boolean isReplenCreated = false;
		InventoryMaster inventory = inventoryMasterRepository.findByPartNumberAndSerialReference(replenDTO.getPartNumber(), replenDTO.getSerialReference());
		if(inventory!=null){
			InventoryStatus status = inventory.getInventoryStatus();
			F6MissingHistory f6MissingHistory = new F6MissingHistory();
			f6MissingHistory.setCreatedBy(replenDTO.getUserId());
			f6MissingHistory.setDateCreated(new Date());
			f6MissingHistory.setInventoryStatusCode(status!=null?status.getInventoryStatusCode():"MISSING");
			f6MissingHistory.setLocationCode(replenDTO.getLocationCode());
			f6MissingHistory.setPartNumber(replenDTO.getPartNumber());
			f6MissingHistory.setQty(inventory.getAvailableQty());
			f6MissingHistory.setSerial(inventory.getSerialReference());
			f6MissingHistoryRepository.save(f6MissingHistory);
			
			isReplenCreated = createReplen(replenDTO);
		}else{
			logger.error("F6pick .. inventory was null");
			isReplenCreated = false;
		}
		
		
		return isReplenCreated;
	}



	
}
