package com.vantec.pick.service;

import java.io.IOException;
import java.util.List;

import com.vantec.pick.entity.OrderBody;

public interface MessageService {
	
	public Boolean sendPickedMessage(String documentRef, String userId) throws IOException;
	public Boolean sendPickedMessageForOrderBody(List<OrderBody> orderBodies, String userId) throws IOException;
	public Boolean sendCancelMessage(List<String> customerRefs, String userId) throws IOException;
	
}
