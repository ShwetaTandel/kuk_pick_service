package com.vantec.pick.service;

import java.io.IOException;

import com.vantec.pick.model.GetPickDTO;
import com.vantec.pick.model.PickDTO;
import com.vantec.pick.model.ProcessPickDTO;

public interface PickService {
	
	public  PickDTO getPickDetail(GetPickDTO getPickDTO) throws IOException, Exception;
	public  PickDTO processPick(ProcessPickDTO pickDTO) throws  Exception;
	public String ptttTransaction(String tagReference, String locationCode, String userId);
	public String mttTransaction(String serialReference, String locationCode, String userId);
}
