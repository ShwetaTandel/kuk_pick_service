package com.vantec.pick.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.command.CustomValueGeneratorCommand;
import com.vantec.pick.command.PltNumberGenerator;
import com.vantec.pick.entity.F6MissingHistory;
import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.InventoryStatus;
import com.vantec.pick.entity.Location;
import com.vantec.pick.entity.Part;
import com.vantec.pick.entity.PickGroup;
import com.vantec.pick.entity.ReplenTask;
import com.vantec.pick.entity.ReplenTransferHistory;
import com.vantec.pick.entity.Tag;
import com.vantec.pick.entity.TransactionHistory;
import com.vantec.pick.model.ReplenDTO;
import com.vantec.pick.model.ReplenScanDTO;
import com.vantec.pick.repository.F6MissingHistoryRepository;
import com.vantec.pick.repository.InventoryMasterRepository;
import com.vantec.pick.repository.InventoryStatusRepository;
import com.vantec.pick.repository.LocationRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.repository.PickGroupRepository;
import com.vantec.pick.repository.ReplenTaskRepository;
import com.vantec.pick.repository.ReplenTransferHistoryRepository;
import com.vantec.pick.repository.TagRepository;
import com.vantec.pick.repository.TransactionHistoryRepository;

@Service("replenInfo")
public class ReplenInfoImpl implements ReplenInfo {
	
	private static Logger logger = LoggerFactory.getLogger(ReplenInfoImpl.class);
	
	@Autowired
	InventoryMasterRepository inventoryMasterRepository;
	
	@Autowired
	TransactionHistoryRepository transactionHistoryRepository;
	
	@Autowired
	InventoryStatusRepository inventoryStatusRepository;
	
	@Autowired
	ReplenTaskRepository replenTaskRepository;
	
	@Autowired
	ReplenTransferHistoryRepository replenTransferHistoryRepository;
	
	@Autowired
	LocationRepository locationRepository;
	
	@Autowired
	PickGroupRepository pickGroupRepository;
	
	@Autowired
	TagRepository tagRepository;
	
	@Autowired
	PartRepository partRepository;
	
	@Autowired
	F6MissingHistoryRepository f6MissingHistoryRepository;
	
	public Boolean pickGroupCheck(Location location){
		
		Boolean isPickGroupCheck = false;
		if(location.getPickGroup() != null){
			isPickGroupCheck = true;
		}
		return isPickGroupCheck;
	}
	
	public Boolean pickFaceQtyCheck(Location location){
		Boolean isPickFaceQtyCheck = false;
		InventoryStatus status = inventoryStatusRepository.findByInventoryStatusCode("OK");
		List<InventoryMaster> inventorys = inventoryMasterRepository.findByCurrentLocationAndInventoryStatus(location, status);
		Integer inventoryCaseQty = inventorys.size();
//		for(InventoryMaster inventory:inventorys){
//			
////			Integer availableQty = inventory. getInventoryQty() - inventory.getAllocatedQty();
////			inventoryQty = inventoryQty + availableQty;
////			inventoryQty = inventoryQty;
//			inventoryCaseQty++;
//		}
		
		
		if(location.getMaxPickFaceQty()!= null && location.getMaxPickFaceQty() > inventoryCaseQty){
			isPickFaceQtyCheck = true;
		}
		return isPickFaceQtyCheck;
	}
	
	public Boolean createReplenTask(ReplenDTO replenDTO,Location location){
		
		ReplenTask replenTask = new ReplenTask();
		
		
		String partNumber = null;
		if(location.getPartNumber()!=null  && !("").equalsIgnoreCase(location.getPartNumber())){
			
			partNumber = location.getPartNumber();
		}else{
			
			partNumber = replenDTO.getPartNumber();
		}
		
		
		
		Part part = partRepository.findByPartNumber(partNumber);
		
		replenTask.setCreatedBy(replenDTO.getUserId());
		replenTask.setDateCreated(new Date());
		replenTask.setDateUpdated(new Date());
		replenTask.setUpdatedBy(replenDTO.getUserId());
		//replenTask.setFromLocationCode(inventory.getCurrentLocation().getLocationCode());
		replenTask.setInUse("");
		//replenTask.setInventoryMasterId(inventory.getId());
		replenTask.setPartId(part.getId());
		replenTask.setPickGroupId(location.getPickGroup().getId());
		replenTask.setPriority(2);
		replenTask.setProcessed(false);
		replenTask.setToLocationCode(replenDTO.getLocationCode());
		replenTask.setUpdatedBy(replenDTO.getUserId());
		
		return replenTaskRepository.save(replenTask) != null;
		
	}
	
	public InventoryMaster filterInventoryForReplen(String partNumber){
			
		    logger.info("In filterInventoryForReplen partNumber " + partNumber );
			List<InventoryMaster> inventorys = inventoryMasterRepository.findByPartNumberOrderByDateCreatedAsc(partNumber);
			
			inventorys = inventorys.stream()
					.filter(inventory -> inventory.getCurrentLocation() != null
					        && inventory.getCurrentLocation().getPickGroup() == null
					        && inventory.getCurrentLocation().getLocationType().getLocationArea() != null
					        && inventory.getCurrentLocation().getLocationType().getLocationArea().equalsIgnoreCase("storage")
			        		&& inventory.getInventoryStatus() != null
			        		&& inventory.getInventoryStatus().getInventoryStatusCode().equalsIgnoreCase("OK")
			        		&& inventory.getRequiresInspection() == false
					        && !inventory.getInventoryStatus().getNonPickable())
					.collect(Collectors.toList());
			
			List<InventoryMaster> inventorysNotExistInReplenTask = new ArrayList<InventoryMaster>();
			for(InventoryMaster inventory:inventorys){
				List<ReplenTask> replenTasks = replenTaskRepository.findByInventoryMasterIdAndProcessed(inventory.getId(),false);
				if(replenTasks == null || replenTasks.size() == 0){
					inventorysNotExistInReplenTask.add(inventory);
				}else if(replenTasks != null){
					for(ReplenTask replenTask:replenTasks){
						if(replenTask.getScannedSerial()==null || ("").equalsIgnoreCase(replenTask.getScannedSerial())){
							inventorysNotExistInReplenTask.add(inventory);
						}
					}
				}
				
			}
			
			
			if(inventorysNotExistInReplenTask.size()>0)
				return inventorysNotExistInReplenTask.get(0);
			else
				return null;
			
		}

	public Boolean editReplenTask(ReplenDTO replenDTO,ReplenTask replenTask) {
		replenTask.setPriority(replenDTO.getPriority());
		replenTask.setPickGroupId(replenDTO.getPickGroupId());
		replenTask.setToLocationCode(replenDTO.getNewLocationCode());
		replenTask.setUpdatedBy(replenDTO.getUserId());
		replenTask.setDateUpdated(new Date());
		return replenTaskRepository.save(replenTask)!=null;
	}

	@Override
	public Boolean createReplenHistory(ReplenDTO replenDTO,ReplenTask replenTask) {
		
		Location location = locationRepository.findByLocationCode(replenTask.getToLocationCode());
		
		ReplenTransferHistory replenTransferHistory = new ReplenTransferHistory();

	
		replenTransferHistory.setFromPickGroupId(replenTask.getPickGroupId());
		replenTransferHistory.setFromPriority(replenTask.getPriority());
		replenTransferHistory.setInventoryMasterId(replenTask.getInventoryMasterId());
		replenTransferHistory.setLocationCode(location == null ? "" :location.getLocationCode());
		replenTransferHistory.setPartId(replenTask.getPartId());
	
		replenTransferHistory.setToPickGroupId(replenDTO.getPickGroupId()!=null?replenDTO.getPickGroupId():replenTask.getPickGroupId());
		replenTransferHistory.setToPriority(replenDTO.getPriority()!=null?replenDTO.getPriority():replenTask.getPriority());
		replenTransferHistory.setNewLocationCode(replenDTO.getNewLocationCode()!=null?replenDTO.getNewLocationCode():replenTask.getToLocationCode());
		replenTransferHistory.setCreatedBy(replenDTO.getUserId());
		replenTransferHistory.setDateCreated(new Date());
		
		return replenTransferHistoryRepository.save(replenTransferHistory)!=null;
	}

	@Override
	public Boolean updateLocationWithPickGoup(String newLocationCode, Long pickGroupId) {
		
		Location location = locationRepository.findByLocationCode(newLocationCode);
		PickGroup pickGroup = pickGroupRepository.findById(pickGroupId);
		location.setPickGroup(pickGroup);
		return locationRepository.save(location)!=null;
		
	}

	@Override
	public List<ReplenTask> findReplenTask(Long pickGroupId) {
		 return replenTaskRepository.findByPickGroupIdAndProcessedAndPalletReferenceIsNullOrderByPriorityAscDateCreatedAsc(pickGroupId,false);
	}
	
	@Override
	public String createVirtualPlt(String palletTypeName,String userId) {
		Tag tag = new Tag();
		tag.setVersion((long) 0);
		tag.setDateCreated(new Date());
		tag.setTagReference(UUID.randomUUID().toString());
		tag.setValidated(false);
		tag.setPicked(false);
		tag.setAutoGenerated(true);
		tagRepository.save(tag);
		Long tagId = tag.getId();
		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new PltNumberGenerator();
		String pltNumber = customValueGeneratorCommand.generate(tagId,palletTypeName);
		tag.setTagReference(pltNumber);
		tag.setDateUpdated(new Date());
		tag.setUpdatedBy(userId);
		tagRepository.save(tag);

		return pltNumber;
	}

	@Override
	public void updateReplenTask(ReplenTask replenTask,ReplenScanDTO replenScanDTO) {
		
		//replenTask.setProcessed(true);
		replenTask.setInUse("");
		replenTask.setScannedSerial(replenScanDTO.getScannedSerial());
		//replenTask.setSuggestedSerial(replenScanDTO.getSuggestedSerial());
		replenTask.setPalletReference(replenScanDTO.getTagReference());
		replenTask.setUpdatedBy(replenScanDTO.getUserId());
		replenTask.setDateUpdated(new Date());
		replenTaskRepository.save(replenTask);
		
	}
	
	@Override
	public void updateInventory(InventoryMaster inventory,ReplenScanDTO replenScanDTO) {

		Location location = locationRepository.findByLocationCode(replenScanDTO.getLocationCode());
		inventory.setCurrentLocation(location);
		inventory.setLocation(location);
		inventory.setLocationCode(location.getLocationCode());
		inventory.setUpdatedBy(replenScanDTO.getUserId());
		inventory.setDateUpdated(new Date());
		InventoryStatus status = inventoryStatusRepository.findByInventoryStatusCode("PLT");
		inventory.setInventoryStatus(status);
		inventory.setTagReference(replenScanDTO.getTagReference());
		inventory.setParentTag(tagRepository.findByTagReference(replenScanDTO.getTagReference()));
		inventoryMasterRepository.save(inventory);
		
	}
	
	@Override
	public void updateInventoryWithStatusOK(InventoryMaster inventory, String locationCode,String userId) {
		
		Location location = locationRepository.findByLocationCode(locationCode);
		inventory.setCurrentLocation(location);
		inventory.setLocation(location);
		inventory.setLocationCode(location.getLocationCode());
		inventory.setTagReference("");
		inventory.setParentTag(null);
		inventory.setDateUpdated(new Date());
		inventory.setUpdatedBy(userId);
		InventoryStatus status = inventoryStatusRepository.findByInventoryStatusCode("OK");
		inventory.setInventoryStatus(status);
		inventoryMasterRepository.save(inventory);
		
	}
	
	@Override
	public void updateInventoryWithStatusMISSING(InventoryMaster inventory,String userId) {
		
		inventory.setDateUpdated(new Date());
		inventory.setUpdatedBy(userId);
		InventoryStatus status = inventoryStatusRepository.findByInventoryStatusCode("MISSING");
		inventory.setInventoryStatus(status);
		inventoryMasterRepository.save(inventory);
		
	}

	@Override
	public Boolean transferReplenTask(ReplenTask replenTask,String userId,PickGroup altPickgroup) {
		ReplenTask replen = replenTaskRepository.findById(replenTask.getId());
		replen.setPickGroupId(altPickgroup.getId());
		replen.setUpdatedBy(userId);
		replen.setDateUpdated(new Date());
		return replenTaskRepository.save(replen) != null;
		
	}

	@Override
	public void updateReplenWithInventory(ReplenTask replenTask, InventoryMaster inventory,String userId) {
		
		
		
		if(inventory != null){
			replenTask.setInventoryMasterId(inventory.getId());
			replenTask.setSuggestedSerial(inventory.getSerialReference());
			replenTask.setFromLocationCode(inventory.getLocationCode());
			replenTask.setInUse(userId);
			replenTask.setDateUpdated(new Date());
			replenTask.setUpdatedBy(userId);
			replenTaskRepository.save(replenTask);
		
		}else{
			replenTask.setProcessed(true);
			replenTask.setDateUpdated(new Date());
			replenTask.setUpdatedBy(userId);
			replenTaskRepository.save(replenTask);
		}
		
	}

	@Override
	public void updateReplenWithScannedSerial(ReplenTask replenTask, String tagReference, String serial) {
		replenTask.setPalletReference(tagReference);
		replenTask.setScannedSerial(serial);
		replenTaskRepository.save(replenTask);
	}

	@Override
	public void createTransactionHistory(ReplenScanDTO replenScanDTO, ReplenTask replenTask, InventoryMaster inventory, Part part) {
        
		TransactionHistory transactionHistory = new TransactionHistory();
		PickGroup pickGroup = pickGroupRepository.findById(replenTask.getPickGroupId());
		
		//transactionHistory.setCountQty(txn.getCountQty());
		//transactionHistory.setCustomerReference(txn.getReceiptHeader().getCustomerReference());
		
		transactionHistory.setFromLocationCode(replenTask.getFromLocationCode());
		//transactionHistory.setFromPlt(txn.getFromPlt());
		transactionHistory.setPartNumber(part.getPartNumber());
        transactionHistory.setRanOrOrder(pickGroup.getPickGroupCode());
		//transactionHistory.setReasonCode(txn.getReasonCode());
		transactionHistory.setRequiresDecant(inventory.getRequiresDecant());
		transactionHistory.setRequiresInspection(inventory.getRequiresInspection());
		transactionHistory.setTxnQty(inventory.getAvailableQty());
		transactionHistory.setToLocationCode("");
		transactionHistory.setSerialReference(replenScanDTO.getScannedSerial());
		transactionHistory.setShortCode("REPLEN");
		transactionHistory.setTransactionReferenceCode("REPLEN");
		transactionHistory.setToPlt(replenScanDTO.getTagReference());
		//transactionHistory.setAssociatedDocumentReference("");
		transactionHistory.setDateCreated(new Date());
		transactionHistory.setCreatedBy(replenScanDTO.getUserId());
		transactionHistory.setDateUpdated(new Date());
		transactionHistory.setUpdatedBy(replenScanDTO.getUserId());
		transactionHistory.setComment("replen TaskId : " + replenTask.getId());
		
		transactionHistoryRepository.save(transactionHistory);
		
	}
	
	
	@Override
	public void createTransactionHistoryREPLOC(String userId,InventoryMaster inventory,String locationCode,ReplenTask replenTask) {
        
		TransactionHistory transactionHistory = new TransactionHistory();
		PickGroup pickGroup = pickGroupRepository.findById(replenTask.getPickGroupId());
		
		transactionHistory.setFromLocationCode(inventory.getCurrentLocation().getLocationCode());
		transactionHistory.setFromPlt(inventory.getTagReference());
		transactionHistory.setPartNumber(inventory.getPartNumber());
		transactionHistory.setRequiresDecant(inventory.getRequiresDecant());
		transactionHistory.setRequiresInspection(inventory.getRequiresInspection());
		transactionHistory.setTxnQty(inventory.getAvailableQty());
		transactionHistory.setToLocationCode(locationCode);
		transactionHistory.setSerialReference(inventory.getSerialReference());
		transactionHistory.setShortCode("REPLOC");
		transactionHistory.setTransactionReferenceCode("REPLOC");
		transactionHistory.setToPlt("");
		transactionHistory.setDateCreated(new Date());
		transactionHistory.setCreatedBy(userId);
		transactionHistory.setDateUpdated(new Date());
		transactionHistory.setUpdatedBy(userId);
		transactionHistory.setRanOrOrder(pickGroup.getPickGroupCode());
		transactionHistory.setComment("replen TaskId : " + replenTask.getId());
		
		transactionHistoryRepository.save(transactionHistory);
		
	}

	@Override
	public void recordF6MissingHistory(ReplenTask replenTask, InventoryMaster inventory, String userId) {

		Part part = partRepository.findById(replenTask.getPartId());
		F6MissingHistory f6MissingHistory = new F6MissingHistory();
		f6MissingHistory.setCreatedBy(userId);
		f6MissingHistory.setDateCreated(new Date());
		f6MissingHistory.setInventoryStatusCode(inventory == null ? "" : inventory.getInventoryStatus().getInventoryStatusCode());
		f6MissingHistory.setLocationCode(replenTask.getToLocationCode());
		f6MissingHistory.setPartNumber(part.getPartNumber());
		f6MissingHistory.setQty(inventory ==null ? 0.0 :inventory.getAvailableQty());
		f6MissingHistory.setSerial(inventory.getSerialReference());
		f6MissingHistoryRepository.save(f6MissingHistory);
		
		
	}

	@Override
	public void freeInventoryFromTag(Long inventoryMasterId) {
		
		InventoryStatus status = inventoryStatusRepository.findByInventoryStatusCode("OK");
		
		InventoryMaster inventory = inventoryMasterRepository.findById(inventoryMasterId);
		if(inventory!=null){
			inventory.setTagReference(null);
			inventory.setParentTag(null);
			inventory.setInventoryStatus(status);
			inventoryMasterRepository.save(inventory);
		}
		
	}






}
