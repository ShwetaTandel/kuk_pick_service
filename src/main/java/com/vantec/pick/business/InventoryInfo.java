package com.vantec.pick.business;

import java.util.List;

import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.PickDetail;

public interface InventoryInfo {

	

	void updateInventoryWithQty(InventoryMaster inventory, Double allocatedQty, String userId);
	void releaseInventory(PickDetail pickDetail);
	List<InventoryMaster> filterInventoryForAllocation(OrderBody orderBody);
	public void reduceInventoryByScannedQty(PickDetail pickDetail, Double scannedQty) ;
	public void updateInventoryLocationForPTTT(InventoryMaster inventory, String locationCode,String userId) ;
	public void deleteInventory(InventoryMaster inventory);
	public void unallocateInventory(InventoryMaster inventory);
	void updateInventoryLocation(InventoryMaster inventory, String locationCode, String userId);
	
}
