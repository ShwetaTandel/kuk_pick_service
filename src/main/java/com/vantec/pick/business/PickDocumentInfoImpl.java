package com.vantec.pick.business;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.command.CustomValueGeneratorCommand;
import com.vantec.pick.command.DocumentReferenceGenerator;
import com.vantec.pick.entity.DocumentDetail;
import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.InventoryStatus;
import com.vantec.pick.entity.Location;
import com.vantec.pick.entity.LocationType;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.PIHeader;
import com.vantec.pick.entity.Part;
import com.vantec.pick.entity.PickBody;
import com.vantec.pick.entity.PickDetail;
import com.vantec.pick.entity.PickHeader;
import com.vantec.pick.entity.PickOrderLink;
import com.vantec.pick.entity.Tag;
import com.vantec.pick.entity.TrainLocations;
import com.vantec.pick.entity.TransactionHistory;
import com.vantec.pick.entity.TransactionHistorySkip;
import com.vantec.pick.entity.ZoneDestination;
import com.vantec.pick.exception.ConcurrentException;
import com.vantec.pick.model.PickDTO;
import com.vantec.pick.model.ProcessPickDTO;
import com.vantec.pick.repository.DocumentDetailRepository;
import com.vantec.pick.repository.DocumentRegisterRepository;
import com.vantec.pick.repository.DocumentStatusRepository;
import com.vantec.pick.repository.InventoryMasterRepository;
import com.vantec.pick.repository.InventoryStatusRepository;
import com.vantec.pick.repository.LocationRepository;
import com.vantec.pick.repository.LocationTypeRepository;
import com.vantec.pick.repository.OrderBodyRepository;
import com.vantec.pick.repository.OrderHeaderRepository;
import com.vantec.pick.repository.PIHeaderRepository;
import com.vantec.pick.repository.PIReasonCodeRepository;
import com.vantec.pick.repository.PickBodyRepository;
import com.vantec.pick.repository.PickDetailRepository;
import com.vantec.pick.repository.PickHeaderRepository;
import com.vantec.pick.repository.PickOrderLinkRepository;
import com.vantec.pick.repository.TagRepository;
import com.vantec.pick.repository.TrainLocationsRepository;
import com.vantec.pick.repository.TransactionHistoryRepository;
import com.vantec.pick.repository.TransactionHistorySkipRepository;
import com.vantec.pick.repository.ZoneDestinationRepository;
import com.vantec.pick.service.MessageService;
import com.vantec.pick.util.ConstantHelper;

@Service("pickDocumentInfo")
public class PickDocumentInfoImpl implements PickDocumentInfo {

	private static Logger logger = LoggerFactory.getLogger(PickDocumentInfoImpl.class);
	@Autowired
	OrderHeaderRepository orderHeaderRepository;

	@Autowired
	OrderBodyRepository orderBodyRepository;

	@Autowired
	DocumentDetailRepository documentDetailRepository;

	@Autowired
	DocumentRegisterRepository documentRegisterRepository;

	@Autowired
	PickHeaderRepository pickHeaderRepository;

	@Autowired
	LocationTypeRepository locationTypeRepository;

	@Autowired
	PickBodyRepository pickBodyRepository;

	@Autowired
	LocationRepository locationRepository;

	@Autowired
	PickDetailRepository pickDetailRepository;

	@Autowired
	PickOrderLinkRepository pickOrderLinkRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	InventoryMasterRepository inventoryMasterRepository;

	@Autowired
	TransactionHistoryRepository transHistRepo;

	@Autowired
	TransactionHistorySkipRepository transHistSkipRepo;

	@Autowired
	InventoryInfo inventoryInfo;

	@Autowired
	ZoneDestinationRepository zoneDestRepo;

	@Autowired
	InventoryStatusRepository invStatusRepo;

	@Autowired
	MessageService messageService;

	@Autowired
	TagRepository tagRepository;

	@Autowired
	TrainLocationsRepository trainLocsRepo;

	@Autowired
	PIHeaderRepository piHeaderRepository;;
	@Autowired
	PIReasonCodeRepository piReasonCodeRepo;

	public PickHeader createPickHeader(String userId, String pickType) {

		PickHeader pickHeader = new PickHeader();
		pickHeader.setPickType(pickType);
		pickHeader.setCreatedBy(userId);
		pickHeader.setDateCreated(new Date());
		pickHeader.setUpdatedBy(userId);
		pickHeader.setDateUpdated(new Date());
		pickHeader = pickHeaderRepository.save(pickHeader);

		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();
		String orderReference = customValueGeneratorCommand.generate(pickHeader.getId(), "PCK");
		pickHeader.setDocumentReference(orderReference);
		pickHeader.setDocumentStatus(
				documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN));
		pickHeader = pickHeaderRepository.save(pickHeader);
		return pickHeader;
	}

	public void createPickBodies(PickHeader pickHeader, List<OrderHeader> orderHeaders, String userId) {

		// Combine pick bodies by part number, zone destination in all the
		// orders selected
		List<PickBody> pickBodies = createPickBodiesGroupedByPartAndZoneFromOrders(pickHeader, orderHeaders, userId);

		for (PickBody pickBody : pickBodies) {

			pickBody.setPickHeader(pickHeader);
			pickBody.setDocumentReference(pickHeader.getDocumentReference());
			pickHeader.getPickBodies().add(pickBody);
			pickBodyRepository.save(pickBody);

		}
		pickHeader = pickHeaderRepository.save(pickHeader);
		// Add train locations if any
		List<String> pickZones = pickBodyRepository.findDistinctZoneDestinationsForPickHeader(pickHeader);
		List<String> trains = zoneDestRepo.findTrainsForZones(pickZones);
		if (trains != null && !trains.isEmpty()) {
			// Create Trains LOcations
			for (String train : trains) {
				String trainLocCode = pickHeader.getDocumentReference() + " - " + train;
				TrainLocations trainloc = trainLocsRepo.findByTrainLocationCode(trainLocCode);
				if (trainloc == null) {
					trainloc = new TrainLocations();
					trainloc.setTrainLocationCode(trainLocCode);
					trainloc.setTrainLocationHash(createTrainHashCode());
					trainloc.setDateCreated(new Date());
					trainloc.setCreatedBy(userId);
					trainLocsRepo.save(trainloc);
				}
			}
		}

		// Add Pick header reference to Order Header
		for (OrderHeader orderHeader : orderHeaders) {
			orderHeader.setPickHeader(pickHeader);
			orderHeaderRepository.save(orderHeader);
		}
	}

	private String createTrainHashCode() {

		String source = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		SecureRandom secureRandom = new SecureRandom();

		int length = 6;
		StringBuilder hashCode = new StringBuilder(length);

		for (int i = 0; i < length; i++) {
			hashCode.append(source.charAt(secureRandom.nextInt(source.length())));
		}

		return hashCode.toString();
	}

	private List<PickBody> createPickBodiesGroupedByPartAndZoneFromOrders(PickHeader pickHeader,
			List<OrderHeader> orderHeaders, String userId) {

		List<PickBody> pickBodies = new ArrayList<PickBody>();

		List<Object[]> orderData = orderBodyRepository.findBodiesGroupByPartAndZoneForOrderHeaders(orderHeaders);

		for (Object[] orderBody : orderData) {
			PickBody pickBody = pickBodyRepository.findByPickHeaderAndPartNumberAndZoneDestination(pickHeader,
					(String) orderBody[0], (String) orderBody[3]);
			if (pickBody == null) {
				pickBody = new PickBody();
				pickBody.setCreatedBy(userId);
				pickBody.setDateCreated(new Date());
				pickBody.setPart((Part) orderBody[2]);
				pickBody.setPartNumber((String) orderBody[0]);
				pickBody.setPickHeader(pickHeader);
				pickBody.setZoneDestination((String) orderBody[3]);
				pickBody.setProcessed(false);
				pickBody.setQtyExpected((Double) orderBody[1]);
				pickBody.setQtyAllocated(0.0);
				pickBody.setQtyShortage((Double) orderBody[1]);
				pickBody.setQtyTransacted(0.0);
				pickBody.setUpdatedBy(userId);
				pickBody.setDateUpdated(new Date());
				pickBodies.add(pickBody);
			} else {
				logger.info(
						"Skipping adding the quantities. This should happen only for orders split accross files. Pick Header is "
								+ pickHeader.getDocumentReference() + ".Part / zone is =" + (String) orderBody[0] + "/"
								+ (String) orderBody[3]);
				// pickBody.setQtyExpected(pickBody.getQtyExpected() + (Double)
				// orderBody[1]);
				// pickBody.setQtyShortage(pickBody.getQtyShortage() + (Double)
				// orderBody[1]);
			}

		}

		return pickBodies;
	}

	public boolean addPickDetail(PickBody pickBody, InventoryMaster inventory, Double allocatedQty, OrderBody orderBody,
			String userId) {

		Location invLocation = inventory.getCurrentLocation();
		PickDetail pickDetail = pickDetailRepository
				.findByPickBodyAndPartNumberAndProcessedAndLocationCodeAndSerialReference(pickBody,
						pickBody.getPartNumber(), false, inventory.getLocationCode(), inventory.getSerialReference());
		//Commenting this as we don't pick body for in use anymore
		/*if (pickBody != null && pickBody.getInUseBy() != null && !pickBody.getInUseBy().equals(ConstantHelper.EMPTY)) {
			// This is a case where refresh allocation is forced while picking
			// but its in use by other operator
			logger.info("Pick Body id =" + pickBody.getId() + "is in use by " + pickBody.getInUseBy());
			return false;

		}*/
		if (pickDetail == null) {
			pickDetail = new PickDetail();
			pickDetail.setCreatedBy(userId);
			pickDetail.setDateCreated(new Date());
			pickDetail.setQtyAllocated(0D);// just put some default value
			pickDetail.setQtyScanned(0D);// just put some default value
		}
		pickDetail.setUpdatedBy(userId);
		pickDetail.setDateUpdated(new Date());
		pickDetail.setInventoryMaster(inventory);
		pickDetail.setLocationCode(invLocation.getLocationCode());
		pickDetail.setPart(inventory.getPart());
		pickDetail.setPartNumber(inventory.getPartNumber());
		pickDetail.setPickBody(pickBody);
		pickDetail.setProcessed(false);
		pickDetail.setQtyAllocated(pickDetail.getQtyAllocated() + allocatedQty);
		pickDetail.setSerialReference(inventory.getSerialReference());
		pickDetail.setDocumentReference(pickBody.getDocumentReference());

		String pickSeq = "";
		if(ConstantHelper.MARSHALLING_LOCATION_TYPE_CODE.equalsIgnoreCase(invLocation.getLocationType().getLocationTypeCode())){
			pickSeq += "1";
		}
		pickSeq += invLocation.getLocationAisleCode().getAisleCode();
		pickSeq += invLocation.getLocationRackCode().getRackCode();
		if (invLocation.getLocationRackCode().getPriorityRowColumn() == null
				|| invLocation.getLocationRackCode().getPriorityRowColumn().equalsIgnoreCase("C")) {
			pickSeq += invLocation.getLocationColumnCode().getFillingPriority();

		} else {
			pickSeq += invLocation.getLocationRowCode().getFillingPriority();
		}
		if (invLocation.getLocationRackCode().getPriorityRowColumn() != null
				&& !invLocation.getLocationRackCode().getPriorityRowColumn().equalsIgnoreCase("C")) {
			pickSeq += invLocation.getLocationColumnCode().getFillingPriority();
		} else {
			pickSeq += invLocation.getLocationRowCode().getFillingPriority();
		}
		pickSeq += invLocation.getLocationCode();
		pickDetail.setPickSequence(pickSeq);
		if (orderBody != null) {
			pickDetail.setZoneDestination(zoneDestRepo.findByZoneDestination(orderBody.getZoneDestination()));
			pickDetail.setZoneDestinationCode(orderBody.getZoneDestination());
		}
		pickDetail = pickDetailRepository.save(pickDetail);
		logger.info("Pickdetail added " + pickDetail.toString());
		PickOrderLink newLink = new PickOrderLink();
		if (orderBody != null) {
			newLink.setLineNo(orderBody.getLineNo());
			// pickDetail.setPickPriority((long)documentDetail.getOurDetailNo());
			newLink.setOrderBody(orderBody);
			newLink.setPickDetail(pickDetail);
			newLink.setQtyAllocated(allocatedQty);
			newLink.setDateCreated(new Date());
			newLink.setLastUpdatedAt(new Date());
			newLink.setUpdatedBy(userId);
			newLink.setCreatedBy(userId);
			newLink.setProcessed(false);
			pickOrderLinkRepository.save(newLink);
			logger.info("PickLink added " + newLink.toString());
		}
		// pickBody.getPickDetails().add(pickDetail);
		// pickBodyRepository.save(pickBody);

		return true;

	}

	/***
	 * 
	 * Create a new Pick Body for AAA Picks
	 * 
	 */
	public void copyPickBody(PickBody pickBodyToCopy, PickHeader newPickHeader, String user) {
		if (pickBodyToCopy.getQtyTransacted().doubleValue() == 0) {
			pickBodyToCopy.setPickHeader(newPickHeader);
			pickBodyToCopy.setDocumentReference(newPickHeader.getDocumentReference());
			pickBodyToCopy.setDateUpdated(new Date());
			pickBodyToCopy.setUpdatedBy(user);
			newPickHeader.getPickBodies().add(pickBodyToCopy);
			pickBodyRepository.save(pickBodyToCopy);
			for (PickDetail detail : pickBodyToCopy.getPickDetails()) {
				detail.setDocumentReference(newPickHeader.getDocumentReference());
				detail.setDateUpdated(new Date());
				detail.setUpdatedBy(user);
				pickDetailRepository.save(detail);
			}
		} else if (pickBodyToCopy.getQtyTransacted().doubleValue() != 0
				&& pickBodyToCopy.getQtyExpected().doubleValue() > pickBodyToCopy.getQtyTransacted().doubleValue()) {
			// Create new pickBody object
			PickBody newPickBody = new PickBody();
			newPickBody.setPickHeader(newPickHeader);
			newPickBody.setDocumentReference(newPickHeader.getDocumentReference());
			newPickBody.setPart(pickBodyToCopy.getPart());
			newPickBody.setPartNumber(pickBodyToCopy.getPartNumber());
			newPickBody.setQtyAllocated(pickBodyToCopy.getQtyAllocated());
			newPickBody.setQtyExpected(
					pickBodyToCopy.getQtyExpected().doubleValue() - pickBodyToCopy.getQtyTransacted().doubleValue());
			newPickBody.setQtyShortage(pickBodyToCopy.getQtyShortage());
			newPickBody.setQtyTransacted(0.0);
			newPickBody.setProcessed(false);
			newPickBody.setZoneDestination(pickBodyToCopy.getZoneDestination());
			newPickBody.setCreatedBy(user);
			newPickBody.setUpdatedBy(user);
			newPickBody.setDateCreated(new Date());
			newPickBody.setDateUpdated(new Date());
			newPickHeader.getPickBodies().add(newPickBody);
			newPickBody = pickBodyRepository.save(newPickBody);
			for (PickDetail detail : pickBodyToCopy.getPickDetails()) {
				detail.setPickBody(newPickBody);
				detail.setDocumentReference(newPickHeader.getDocumentReference());
				detail.setDateUpdated(new Date());
				detail.setUpdatedBy(user);
				pickDetailRepository.save(detail);
			}
			pickBodyToCopy.setProcessed(true);
			pickBodyToCopy.setQtyExpected(pickBodyToCopy.getQtyTransacted());
			pickBodyToCopy.setQtyShortage(0D);
			pickBodyToCopy.setDateUpdated(new Date());
			pickBodyToCopy.setUpdatedBy(user);
			pickBodyRepository.save(pickBodyToCopy);

		}
		pickHeaderRepository.save(newPickHeader);

	}

	/**
	 * 
	 * Mark picks processed if its auto pick
	 * 
	 * @param header
	 * @param checkAutoPick
	 *            - whether to check a auto pick or general picking
	 * @return
	 * @throws Exception 
	 */
	public void processAutoPick(PickHeader header, String user) throws Exception {
		logger.info("checking autopick for+" + header.getDocumentReference());
		
		for (PickBody body : header.getPickBodies()) {
			boolean zoneAutoPick = false;
			ZoneDestination zone = zoneDestRepo.findByZoneDestination(body.getZoneDestination());
			if(zone!= null){
				zoneAutoPick =  zone.getAutoPick();
			}
			LocationType locTypePart = locationTypeRepository.findById(body.getPart().getFixedLocationType().getId());
			boolean canPick = locTypePart.getAutoPick() || zoneAutoPick;
			Double scannedTotal = 0.0;
			boolean autoPickMA = locTypePart.getAutoPickMa();
			//canpick overwrites autopick MA
			if ((canPick || autoPickMA ) && body.getQtyAllocated() > 0 && !body.getProcessed()) {
				logger.info("processing the pick for autopick for body id " + body.getId() );
				// Mark the pick as processed
				List<PickDetail> pickDetails = pickDetailRepository.findByPickBodyAndProcessed(body, false);
				for (PickDetail pick : pickDetails) {
					boolean pleasePick = false;
					if(autoPickMA && !canPick){
						//this the case where only AUTO PICK if the pick from MA is true
						//Check whether the stock is actually in MA location and overwrite the flag can pick to true
						logger.info("Can Pick is false but Auto pick MA is true part "+pick.getPartNumber()+" inventory id is = " + pick.getInventoryMaster().getId());
						pleasePick = pick.getInventoryMaster().getCurrentLocation().getLocationType().getLocationTypeCode().equals(ConstantHelper.MARSHALLING_LOCATION_TYPE_CODE);
						logger.info("pleasePick (is it in MA?) ->" + pleasePick);
					}else if(canPick){
						// this means can pick is true so i pick it not check for please pick
						pleasePick = canPick;
						logger.info("can pick is true "+ pleasePick);
					}
					if(pleasePick ){
						ProcessPickDTO pickDTO = new ProcessPickDTO();
						pickDTO.setMissingSerial(false);
						pickDTO.setPallet(ConstantHelper.EMPTY);
						pickDTO.setPickId(pick.getId());
						pickDTO.setQtyScanned(pick.getQtyAllocated());
						pickDTO.setSerialScanned(pick.getSerialReference());
						pickDTO.setUserId(user);
						pickDTO.setTransactionCode(ConstantHelper.TRANSACTION_CODE_PICK_TO_MARSHALL);
						pickDTO.setSkip(false);
						pickDTO.setToLocation(pick.getInventoryMaster().getCurrentLocation().getLocationCode());
	
						scannedTotal += pick.getQtyAllocated();
						//try {
							logger.info("Calling process pick detail from auto pick for pick id "+pick.getId());
							processPickDetail(pick, pickDTO, false);
						/*} catch (Exception e) {
							// TODO Auto-generated catch block
							logger.info("Exception while Auto Picking is " + e.getMessage());
						}*/
					}
				}
			}
		}

	}

	/***
	 * 
	 * @param pick
	 * @param user
	 */
	public PickDetail processPickDetail(PickDetail pick, ProcessPickDTO pickDTO, boolean serialScannedDiff)
			throws Exception {
		logger.info("processPickDetail 1-> "+pickDTO.toString());
		logger.info("processPickDetail 2-> "+pick.toString());
		DocumentStatus pickedstatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_PICKED);
		boolean fullBoxPick = pick.getPart().getFullBoxPick();
		boolean returnSamePick = false;
		boolean qtyLess = pick.getQtyAllocated().doubleValue() > pickDTO.getQtyScanned().doubleValue();
		boolean clearPickBodyAllocated = false;
		InventoryStatus pkdInvStatus = invStatusRepo.findByInventoryStatusCode(ConstantHelper.INVENTORY_STATUS_PICKED);
		InventoryStatus okInvStatus = invStatusRepo.findByInventoryStatusCode(ConstantHelper.INVENTORY_STATUS_OK);
		List<OrderBody> orderBodies = new ArrayList<OrderBody>();

		if (qtyLess && !fullBoxPick) {
			returnSamePick = true;
		}
		boolean modularPick = pick.getPart().getModular();
		InventoryMaster serialScannedInv = inventoryMasterRepository
				.findByPartNumberAndSerialReference(pick.getPartNumber(), pickDTO.getSerialScanned());
		boolean nonExistSerial = (serialScannedInv == null) ? true : false;
		boolean noSerialScan = pickDTO.getSerialScanned().isEmpty()
				&& pick.getPart().getFixedLocationType().getNoSerialScan();
		// update return same pick flag of serial scanned is diff
		if (qtyLess && serialScannedDiff) {
			returnSamePick = true;
		}
		PickDetail newPick = null;
		// get new Inventory Master with scanned serial and update detail with
		// it and free the previous one with allocated serial
		// If its return same pick create a new pick for returning back

		if (returnSamePick) {
			logger.info(
					"Creating new pick because the qty scanned was less than expected or its serial scanned diff scenario");
			newPick = copyPickDetail(pick, pickDTO);
			logger.info("processPickDetail 3->new pick" + newPick.toString());
			pick.setQtyAllocated(0d);
		}

		if (noSerialScan) {
			// If its no serial scan then use the advised serial
			pickDTO.setSerialScanned(pick.getSerialReference());
		} else if (nonExistSerial) {
			// If its non exists serial scan then use then just changed serial
			// scanned to the one scanned and dont change inv master or advised
			// serial
			pick.setSerialReferenceScanned(pickDTO.getSerialScanned());
		} else if (serialScannedDiff) {
			// release the Inventory master of expected serial only if its not
			// in same pick body
			if (returnSamePick) {
				// Only reduce the allocated qty for the Inventory
				logger.info("Alternate serial Reduce Before");
				inventoryInfo.reduceInventoryByScannedQty(pick, pickDTO.getQtyScanned());
				logger.info("Alternate serial Reduce - After");
			} else {
				// only release inventory if its not return same pick
				logger.info("Alternate serial Realease - Before ");
				inventoryInfo.releaseInventory(pick);
				logger.info("Alternate serial Realease - After ");
			}
			// set the new Inventory Master
			InventoryMaster invMaster = inventoryMasterRepository
					.findByPartNumberAndSerialReference(pick.getPartNumber(), pickDTO.getSerialScanned());
				logger.info("Alternate serial scanned Inventory->" + invMaster.toString());
			if (invMaster.getAvailableQty().doubleValue() == 0) {
				inventoryInfo.unallocateInventory(invMaster);

			} else if (invMaster.getAvailableQty().doubleValue() < pickDTO.getQtyScanned().doubleValue()) {
				double diff = pickDTO.getQtyScanned().doubleValue() - invMaster.getAvailableQty().doubleValue();
				logger.info(
						"Alternate Serial Quantity available in inv  is less than scanned for inv So swapping the scanned qty");
				pickDTO.setQtyScanned(invMaster.getAvailableQty().doubleValue());
				pick.setQtyAllocated(invMaster.getAvailableQty().doubleValue());
				// subtract the diff amount from pick body quantities
				logger.info("Alternate Serial Quantity Less ->" + pick.toString());
				PickBody body = pick.getPickBody();
				body.setQtyAllocated(body.getQtyAllocated().doubleValue() - diff);
				pickBodyRepository.save(body);
				logger.info("Alternate Serial Quantity Less ->" + body.toString());

			}
			invMaster.setAllocatedQty(invMaster.getAllocatedQty() + pickDTO.getQtyScanned().intValue());
			invMaster.setAvailableQty(invMaster.getInventoryQty().doubleValue()
					- invMaster.getAllocatedQty().doubleValue() - invMaster.getHoldQty().doubleValue());
			inventoryMasterRepository.save(invMaster);
			pick.setInventoryMaster(invMaster);
			logger.info("Alternate serial Final Inv Master after updation->"+invMaster.toString());

		}
		pick.setSerialReferenceScanned(pickDTO.getSerialScanned());
		if (nonExistSerial) {
			// need to un-allocate that serial
			pick.setQtyAllocated(0.0);
		} else {
			if (pick.getQtyAllocated().doubleValue() > 0) {
				if (pick.getQtyAllocated().doubleValue() < pickDTO.getQtyScanned().doubleValue()) {
					pick.setQtyAllocated(0.0);
					if (serialScannedDiff) {
						clearPickBodyAllocated = true;
					}

				} else {
					pick.setQtyAllocated(pick.getQtyAllocated().doubleValue() - pickDTO.getQtyScanned().doubleValue());
				}
			}
		}
		pick.setQtyScanned(pickDTO.getQtyScanned().doubleValue());
		pick.setUpdatedBy(pickDTO.getUserId());

		if (pick.getQtyAllocated().doubleValue() == 0) {
			pick.setProcessed(true);
		}
		pick.setDateUpdated(new Date());
		pick.setUpdatedBy(pickDTO.getUserId());
		pickDetailRepository.save(pick);
		PickDetail savedPick = pickDetailRepository.findById(pick.getId());
		logger.info("processPickDetail 4 Pick after updation"+pick.toString());

		Double pickQty = savedPick.getQtyScanned();
		List<PickOrderLink> pickOrders = pickOrderLinkRepository.findByPickDetail(savedPick);
		for (PickOrderLink pickOrder : pickOrders) {
			OrderBody orderBody = pickOrder.getOrderBody();
			logger.info("processPickDetail 5-> before "+orderBody.toString());
			//if (pickQty >= orderBody.getDifference().doubleValue()) {
			if (pickQty >= pickOrder.getQtyAllocated().doubleValue()) {
				orderBody.setQtyTransacted(
						orderBody.getQtyTransacted().doubleValue() + pickOrder.getQtyAllocated().doubleValue());
				//pickQty -= orderBody.getDifference().doubleValue();
				pickQty -= pickOrder.getQtyAllocated().doubleValue();
			} else {
				orderBody.setQtyTransacted(orderBody.getQtyTransacted().doubleValue() + pickQty);
				pickQty -= pickQty;
			}

			if (orderBody.getQtyTransacted().doubleValue() > orderBody.getQtyExpected().doubleValue()) {
				orderBody.setQtyTransacted(orderBody.getQtyExpected());
			}

			orderBody.setDifference(orderBody.getQtyExpected() - orderBody.getQtyTransacted());
			orderBody.setDateUpdated(new Date());
			orderBody.setUpdatedBy(pickDTO.getUserId());
			orderBodies.add(orderBody);
			if (orderBody.getDifference().doubleValue() == 0) {
				orderBody.setDocumentStatus(pickedstatus);
				//orderPicked = true;
			}
			//pickOrderLinkRepository.save(pickOrder);
			orderBodyRepository.save(orderBody);
			logger.info("processPickDetail 6-> after "+orderBody.toString());
			if (pickQty.doubleValue() <= 0) {
				break;
			}
		}

		InventoryMaster invMaster = savedPick.getInventoryMaster();
		Location originalInvLocation = invMaster.getCurrentLocation();//THis is the from location
		Location currentInvLocation = locationRepository.findByLocationCode(pickDTO.getToLocation());//This is the to location
		TransactionHistory hist = new TransactionHistory();
		logger.info("processPickDetail 6-> before "+invMaster.toString());
		if (fullBoxPick) {
			logger.info("In full box pick");
			// If full box pick : move inventory to scanned location - and down
			// date by scanned quantity
			// Update Inventory
			//In full box pick RDT does not scan qty so use pick allocated qty
			invMaster.setAllocatedQty(invMaster.getAllocatedQty().intValue() - savedPick.getQtyScanned().intValue());
			invMaster.setInventoryQty(invMaster.getAvailableQty().intValue() + invMaster.getAllocatedQty().intValue()
					+ invMaster.getHoldQty().intValue());

			invMaster.setLocation(currentInvLocation);
			invMaster.setCurrentLocation(currentInvLocation);
			invMaster.setLocationCode(currentInvLocation.getLocationCode());
			invMaster.setDateUpdated(new Date());
			invMaster.setUpdatedBy(pickDTO.getUserId());

		} else if (modularPick) {
			logger.info("In pallet pick");
			// if pallet pick - > move all inventory records with pallet tag to
			// scanned location (MAR) , downdate inventory with serial ref , add
			// trans hist
			invMaster.setAllocatedQty(invMaster.getAllocatedQty().intValue() - savedPick.getQtyScanned().intValue());
			invMaster.setInventoryQty(invMaster.getAllocatedQty().intValue() + invMaster.getAvailableQty().intValue());
			invMaster.setCurrentLocation(currentInvLocation);
			invMaster.setLocation(currentInvLocation);
			invMaster.setLocationCode(currentInvLocation.getLocationCode());
			invMaster.setDateUpdated(new Date());
			invMaster.setUpdatedBy(pickDTO.getUserId());
			// update all inv records with same tag
			List<InventoryMaster> allInv = inventoryMasterRepository.findAllByTagReference(invMaster.getTagReference());
			for (InventoryMaster tempInv : allInv) {
				if (tempInv.getId() != invMaster.getId()) {
					tempInv.setCurrentLocation(currentInvLocation);
					tempInv.setLocation(currentInvLocation);
					tempInv.setLocationCode(currentInvLocation.getLocationCode());
					tempInv.setDateUpdated(new Date());
					tempInv.setUpdatedBy(pickDTO.getUserId());
					inventoryMasterRepository.save(tempInv);

					TransactionHistory tempHist = new TransactionHistory();
					tempHist.setCreatedBy(pickDTO.getUserId());
					tempHist.setDateCreated(new Date());
					tempHist.setDateUpdated(new Date());
					tempHist.setUpdatedBy(pickDTO.getUserId());
					tempHist.setAssociatedDocumentReference(savedPick.getPickBody().getDocumentReference());
					tempHist.setPartNumber(savedPick.getPartNumber());
					tempHist.setTxnQty(tempInv.getAvailableQty());
					tempHist.setTransactionReferenceCode(ConstantHelper.TRANSACTION_CODE_MODULAR_MOVE_PICK);
					tempHist.setFromLocationCode(originalInvLocation.getLocationCode());
					tempHist.setToLocationCode(currentInvLocation.getLocationCode());
					tempHist.setToPlt(invMaster.getTagReference());
					tempHist.setSerialReference(tempInv.getSerialReference());
					tempHist.setShortCode(ConstantHelper.TRANSACTION_CODE_MODULAR_MOVE_PICK);
					transHistRepo.save(tempHist);
				}

			}
			inventoryMasterRepository.updateByTagReference(pickDTO.getPallet(), currentInvLocation, pickDTO.getUserId());

		} else {
			logger.info("In piece pick");
			// piece pick - To location for new record with scanned qty should
			// be INTRAN and status pkd ... the old inventory should be down
			// dated...
			// Update Inventory
			if (invMaster.getInventoryQty().intValue() == savedPick.getQtyScanned().intValue()) {
				// If the inventory qty is equal to the scanned qty then just
				// mark the inv record as Picked status - Don't update
				// quantities and change location
				invMaster.setDateUpdated(new Date());
				invMaster.setUpdatedBy(pickDTO.getUserId());
				invMaster.setInventoryStatus(pkdInvStatus);
				invMaster.setCurrentInventoryStatus(pkdInvStatus);
				invMaster.setLocation(currentInvLocation);
				invMaster.setCurrentLocation(currentInvLocation);
				invMaster.setLocationCode(currentInvLocation.getLocationCode());
				invMaster.setOriginatingDetailId(savedPick.getId());
				if (pickDTO.getTransactionCode().equalsIgnoreCase(ConstantHelper.TRANSACTION_CODE_PICK_TO_TROLLEY)) {
					Tag tag = tagRepository.findByTagReference(pickDTO.getPallet());
					if (tag == null) {
						tag = createPalletTag(pickDTO.getPallet(), pickDTO.getUserId());
					}
					invMaster.setTagReference(tag.getTagReference());
					invMaster.setParentTag(tag);
				}
				logger.info(invMaster.getId() + "Marked PKD status");
			} else {
				// If not then create new records with picked status and then
				// down date the original one
				invMaster
						.setAllocatedQty(invMaster.getAllocatedQty().intValue() - savedPick.getQtyScanned().intValue());
				invMaster.setInventoryQty(
						invMaster.getAllocatedQty().intValue() + invMaster.getAvailableQty().intValue());
				invMaster.setInventoryStatus(okInvStatus);
				invMaster.setCurrentInventoryStatus(okInvStatus);
				invMaster.setDateUpdated(new Date());
				invMaster.setUpdatedBy(pickDTO.getUserId());
				logger.info("Down Date original  inventory"+invMaster.toString());

				InventoryMaster newInv = new InventoryMaster();
				newInv.setInventoryQty(savedPick.getQtyScanned().intValue());
				newInv.setAvailableQty(0.0);
				newInv.setAllocatedQty(0);
				newInv.setHoldQty(0);
				newInv.setLocation(currentInvLocation);
				newInv.setCurrentLocation(currentInvLocation);
				newInv.setLocationCode(currentInvLocation.getLocationCode());
				newInv.setInventoryStatus(pkdInvStatus);
				newInv.setCurrentInventoryStatus(pkdInvStatus);
				if (pickDTO.getTransactionCode().equalsIgnoreCase(ConstantHelper.TRANSACTION_CODE_PICK_TO_TROLLEY)) {
					Tag tag = tagRepository.findByTagReference(pickDTO.getPallet());
					if (tag == null) {
						tag = createPalletTag(pickDTO.getPallet(), pickDTO.getUserId());
					}
					newInv.setTagReference(tag.getTagReference());
					newInv.setParentTag(tag);
				}
				newInv.setTagReference(pickDTO.getPallet());
				newInv.setCreatedBy(pickDTO.getUserId());
				newInv.setDateCreated(new Date());
				newInv.setConversionFactor(1);
				newInv.setDateUpdated(new Date());
				newInv.setPartNumber(savedPick.getPartNumber());
				newInv.setPart(savedPick.getPart());
				// newInv.setSerialReference(pick.getSerialReferenceScanned());
				newInv.setRanOrOrder(savedPick.getPickBody().getDocumentReference());
				newInv.setRequiresDecant(false);
				newInv.setRecordedMissing(false);
				newInv.setRequiresInspection(false);
				newInv.setVersion(0L);
				newInv.setOriginatingDetailId(savedPick.getId());

				newInv = inventoryMasterRepository.save(newInv);
				logger.info("New inventory added with picked status+" + newInv.toString());
			}
		}
		logger.info("processPickDetail 7-> after "+invMaster.toString());
		if (invMaster.getInventoryQty().intValue() == 0 && invMaster.getCurrentLocation().getLocationType()
				.getLocationTypeCode().equalsIgnoreCase(ConstantHelper.MARSHALLING_LOCATION_TYPE_CODE)) {
			logger.info("Inventory deleted because stock 0 id =" + invMaster.getId());
			List<PickDetail> picks = pickDetailRepository.findByInventoryMaster(invMaster);
			for (PickDetail picktmp : picks) {
				picktmp.setInventoryMaster(null);
				pickDetailRepository.save(picktmp);
			}
			inventoryMasterRepository.delete(invMaster);
			/// refresh Saved pick if needed -- not sure if this is needed
			savedPick = pickDetailRepository.findById(pick.getId());
		} else {
			inventoryMasterRepository.save(invMaster);
		}
		// Add Transaction History

		hist.setCreatedBy(pickDTO.getUserId());
		hist.setDateCreated(new Date());
		hist.setDateUpdated(new Date());
		hist.setUpdatedBy(pickDTO.getUserId());
		hist.setAssociatedDocumentReference(savedPick.getPickBody().getDocumentReference());
		hist.setPartNumber(savedPick.getPartNumber());
		hist.setTxnQty(savedPick.getQtyScanned());
		hist.setTransactionReferenceCode(pickDTO.getTransactionCode());
		hist.setFromLocationCode(originalInvLocation.getLocationCode());
		hist.setToPlt(pickDTO.getPallet());
		if (nonExistSerial) {
			// set the advised serial in hist and non exist in comments
			hist.setSerialReference(savedPick.getSerialReference());
			hist.setComment(savedPick.getSerialReferenceScanned());
		} else {
			hist.setSerialReference(savedPick.getSerialReferenceScanned());
		}
		hist.setShortCode(pickDTO.getTransactionCode());
		hist.setToLocationCode(currentInvLocation.getLocationCode());
		transHistRepo.save(hist);
		// UPdate the Pick body details
		PickBody body = pickBodyRepository.findById(savedPick.getPickBody().getId());
		if (clearPickBodyAllocated) {
			unAllocatePickBody(pick, body, pickDTO.getUserId());
			body.setQtyTransacted(body.getQtyTransacted() + savedPick.getQtyScanned());
			body.setQtyShortage(body.getQtyExpected().doubleValue()
					- (body.getQtyTransacted().doubleValue() + body.getQtyAllocated().doubleValue()));
		} else {
			body.setQtyTransacted(body.getQtyTransacted() + savedPick.getQtyScanned());
			body.setQtyAllocated(body.getQtyAllocated() - savedPick.getQtyScanned());
			body.setQtyShortage(body.getQtyExpected().doubleValue()
					- (body.getQtyTransacted().doubleValue() + body.getQtyAllocated().doubleValue()));
		}
		if (body.getQtyTransacted().doubleValue() >= body.getQtyExpected().doubleValue()) {
			body.setProcessed(true);
		}
		body = pickBodyRepository.save(body);
		logger.info("processPickDetail PICK BODY FINAL =" +body.toString());

		if (body.getProcessed()) {
			Integer updated = pickDetailRepository.markProcessedByBody(body, pickDTO.getUserId());
			logger.info("Marking "+updated+" details as process because body was processed");

		}
		// orderPicked flag - removed
		// CR - WHERE PICKS CURRENTLY DON’T SEND MESSAGE UNTIL FULL QTY IS
		// ACHIEVED – PBTM – change to send when any qty is picked – BAAN can
		// handle multiple messages for same order line.
		if (pickDTO.getTransactionCode().equalsIgnoreCase(ConstantHelper.TRANSACTION_CODE_PICK_TO_MARSHALL)) {
			// call the message for creating pick
			try {
				logger.info("Sending pick message for Pick " + savedPick.toString());
				if (pickDTO.getQtyScanned() > 0) {
					messageService.sendPickedMessageForOrderBody(orderBodies, pickDTO.getUserId());
				}
				/* Get all inventories created for the order bodies shipped */
				List<PickOrderLink> pickOrderLinks = pickOrderLinkRepository.findAllForOrderBodies(orderBodies);
				List<Long> origDets = new ArrayList<Long>();
				for (PickOrderLink pickOrder : pickOrderLinks) {
					if (pickOrder.getPickDetail() != null) {
						origDets.add(pickOrder.getPickDetail().getId());
					}
				}
				List<InventoryMaster> invetoriesToDelete = inventoryMasterRepository
						.findAllByOriginatingDetailId(origDets, pkdInvStatus);
				if (invetoriesToDelete != null && invetoriesToDelete.size() > 0) {
					pickDetailRepository.markInventoryNullInPick(invetoriesToDelete);
					inventoryMasterRepository.delete(invetoriesToDelete);
					logger.info("Deleting inventories no of " + invetoriesToDelete.size() + " for pick id "
							+ savedPick.getId());
					
				}
				
				Integer done = pickOrderLinkRepository.markProcessedByPickDetail(savedPick, pickDTO.getUserId());
				logger.info("Marking OrderLink processed done= " + done);

			} catch (IOException e) {
				logger.info("Excption in sending picked message for pick" + pickDTO.getPickId() + " exception is "
						+ e.getMessage());
			}

		}
		if (returnSamePick) {
			savedPick = newPick;
			logger.info("Returning new pick -> "+savedPick.toString());
			
		}
		return savedPick;
	}

	/**
	 * 
	 * @param tagRef
	 * @param user
	 * @return
	 */
	private Tag createPalletTag(String tagRef, String user) {
		Tag tag = new Tag();
		tag.setDateCreated(new Date());
		tag.setDateUpdated(new Date());
		tag.setValidated(false);
		tag.setAutoGenerated(false);
		tag.setVersion(0L);
		tag.setPicked(true);
		tag.setUpdatedBy(user);
		tag.setCreatedBy(user);

		return tagRepository.save(tag);

	}

	private PickDetail copyPickDetail(PickDetail pick, ProcessPickDTO pickDTO) {
		logger.info("copyPickDetail-> old pick"+ pick.toString());
		PickDetail newPick = new PickDetail();
		// create a new pick details
		newPick.setCreatedBy(pickDTO.getUserId());
		newPick.setUpdatedBy(pickDTO.getUserId());
		newPick.setDateCreated(new Date());
		newPick.setDateUpdated(new Date());
		newPick.setDocumentReference(pick.getDocumentReference());
		newPick.setInventoryMaster(pick.getInventoryMaster());
		newPick.setSerialReference(pick.getSerialReference());
		newPick.setProcessed(false);
		newPick.setQtyAllocated(pick.getQtyAllocated().doubleValue() - pickDTO.getQtyScanned().doubleValue());
		newPick.setQtyScanned(0d);
		newPick.setPickSequence(pick.getPickSequence());
		newPick.setPickBody(pick.getPickBody());
		newPick.setLineNo(pick.getLineNo());
		newPick.setZoneDestination(pick.getZoneDestination());
		newPick.setZoneDestinationCode(pick.getZoneDestinationCode());
		newPick.setLocationCode(pick.getLocationCode());
		newPick.setPart(pick.getPart());
		newPick.setPartNumber(pick.getPartNumber());
		newPick = pickDetailRepository.save(newPick);
		List<PickOrderLink> links = pickOrderLinkRepository.findByPickDetail(pick);
		for (PickOrderLink link : links) {
			PickOrderLink newLink = new PickOrderLink();
			newLink.setLineNo(link.getLineNo());
			// pickDetail.setPickPriority((long)documentDetail.getOurDetailNo());
			newLink.setOrderBody(link.getOrderBody());
			newLink.setQtyAllocated(link.getQtyAllocated() -  pickDTO.getQtyScanned().doubleValue());
			newLink.setPickDetail(newPick);
			newLink.setDateCreated(new Date());
			newLink.setLastUpdatedAt(new Date());
			newLink.setProcessed(false);
			newLink.setUpdatedBy(pickDTO.getUserId());
			newLink.setCreatedBy(pickDTO.getUserId());
			pickOrderLinkRepository.save(newLink);
		}
		logger.info("copyPickDetail-> new pick "+ pick.toString());
		return newPick;

	}

	/**
	 * Unallocates the pickBody except for the pick being processed and in use
	 * picks Update the inventory etc
	 * 
	 * @param picktoDel
	 * @throws ConcurrentException
	 */
	public void unAllocatePickBody(PickDetail pick, PickBody pickBody, String user) {
		
		logger.info("unAllocatePickBody Before->"+pickBody.toString());

			List<PickBody> bodies = new ArrayList<>();
			bodies.add(pickBody);
			List<PickDetail> details = pickDetailRepository.findPickForBodies(bodies);
			if (details != null && details.size() > 0) {
				for (PickDetail pickToDel : details) {
					if (pick != null && pickToDel.getId() == pick.getId()) {
						continue;
					}
					logger.info("Marking pick detail deleted /processed" + pickToDel.toString());
					inventoryInfo.releaseInventory(pickToDel);
					List<PickOrderLink> links = pickOrderLinkRepository.findByPickDetail(pickToDel);
					logger.info("Deleting all the pickorderlinks =" + links.size());
					pickOrderLinkRepository.delete(links);

					pickToDel.setQtyAllocated(0.0);
					pickToDel.setQtyScanned(0.0);
					pickToDel.setInventoryMaster(null);
					pickToDel.setProcessed(true);
					pickToDel.setDateUpdated(new Date());
					pickToDel.setUpdatedBy(user);
					pickDetailRepository.save(pickToDel);
					logger.info("Pick details marked useless =" + pickToDel.toString());
				}
			} else {
				logger.info("Nothing (un processed and not in use)was found for marking Invalid for pickBody--"
						+ pickBody.getId());
			}

			pickBody.setQtyAllocated(0D);
			pickBody.setQtyShortage(pickBody.getQtyExpected().doubleValue()
					- (pickBody.getQtyTransacted().doubleValue() + pickBody.getQtyAllocated().doubleValue()));
			pickBody.setDateUpdated(new Date());
			pickBody.setUpdatedBy(user);

			pickBodyRepository.save(pickBody);
			logger.info("unAllocatePickBody After->"+pickBody.toString());
	}

	
	@Override
	public void addBlockedPickDetail(PickBody pickBody, DocumentDetail documentDetail, String userId) {
		PickDetail pickDetail = new PickDetail();
		if (documentDetail != null) {
			pickDetail.setBlocked(documentDetail.getBlocked());
			pickDetail.setLineNo(documentDetail.getOurDetailNo());
			pickDetail.setPickPriority((long) documentDetail.getOurDetailNo());
		}
		pickDetail.setCreatedBy(userId);
		pickDetail.setDateCreated(new Date());
		pickDetail.setDateUpdated(new Date());
		pickDetail.setDocumentReference(pickBody.getDocumentReference());
		// pickDetail.setInUseBy(userId);
		pickDetail.setPart(pickBody.getPart());
		pickDetail.setPartNumber(pickBody.getPartNumber());
		pickDetail.setPickBody(pickBody);
		pickDetail.setProcessed(false);
		pickDetail.setQtyAllocated(documentDetail.getQtyExpected().doubleValue());
		pickDetail.setSerialReference(documentDetail.getSerialReference());
		pickDetail.setUpdatedBy(userId);
		pickDetailRepository.save(pickDetail);

	}

	public Boolean updatePickBodyWithQty(PickBody pickBody, Double allocatedQty, String userId) {

		if (pickBody.getQtyAllocated() == null) {
			pickBody.setQtyAllocated(0.0);
		}
		pickBody.setQtyAllocated(pickBody.getQtyAllocated() + allocatedQty);
		int val = pickBody.getQtyExpected()
				.compareTo(pickBody.getQtyAllocated().doubleValue() + pickBody.getQtyTransacted().doubleValue());
		if (val > 0) {
			pickBody.setQtyShortage(
					pickBody.getQtyExpected() - (pickBody.getQtyAllocated() + pickBody.getQtyTransacted()));
		} else {
			pickBody.setQtyShortage(0.0);
		}
		pickBody.setDateUpdated(new Date());
		pickBody.setUpdatedBy(userId);
		Boolean isUpdated = pickBodyRepository.save(pickBody) != null;
		return isUpdated;

	}

	public PickDTO convertPickDetailEntityToView(PickDetail pickDetail) {

		PickDTO pickDTO = new PickDTO();
		pickDTO.setBlocked(pickDetail.getBlocked());
		pickDTO.setCreatedBy(pickDetail.getCreatedBy());
		pickDTO.setDateCreated(pickDetail.getDateCreated());
		pickDTO.setDateUpdated(pickDetail.getDateUpdated());
		pickDTO.setDocumentReference(pickDetail.getDocumentReference());
		pickDTO.setId(pickDetail.getId());
		// pickDTO.setInUseBy(pickDetail.getInUseBy());
		pickDTO.setInventoryMasterId(pickDetail.getInventoryMaster().getId());
		// pickDTO.setLocationCode(pickDetail.getLocationCode());
		pickDTO.setLocationCode(pickDetail.getInventoryMaster().getCurrentLocation().getLocationCode());
		pickDTO.setPartNumber(pickDetail.getPartNumber());
		pickDTO.setPickBodyId(pickDetail.getPickBody().getId());
		pickDTO.setPickPriority(pickDetail.getPickPriority());
		pickDTO.setPickSequence(pickDetail.getPickSequence());
		pickDTO.setProcessed(pickDetail.getProcessed());
		pickDTO.setQtyAllocated(pickDetail.getQtyAllocated());
		pickDTO.setQtyScanned(pickDetail.getQtyScanned());
		pickDTO.setSerialReference(pickDetail.getSerialReference());
		pickDTO.setSerialReferenceScanned(pickDetail.getSerialReferenceScanned());
		pickDTO.setUpdatedBy(pickDetail.getUpdatedBy());
		if (pickDetail.getZoneDestination() != null) {
			pickDTO.setZoneDestination(pickDetail.getZoneDestination().getZoneDestination());
		} else {
			pickDTO.setZoneDestination(pickDetail.getZoneDestinationCode());
		}
		pickDTO.setIsModular(pickDetail.getPart().getModular());
		pickDTO.setIsFullBoxPick(pickDetail.getPart().getFullBoxPick());
		pickDTO.setIsStandardPick(!pickDetail.getPart().getModular() && !pickDetail.getPart().getFullBoxPick());
		pickDTO.setNoSerialScan(pickDetail.getPart().getFixedLocationType().getNoSerialScan());
		pickDTO.setBodyQtyAllocated(pickDetail.getPickBody().getQtyAllocated());
		List<PickOrderLink> links = pickOrderLinkRepository.findByPickDetail(pickDetail);
		for (PickOrderLink link : links) {
			PickDTO.PickOrder po = pickDTO.new PickOrder();
			po.setLineNo(link.getLineNo());
			po.setOrderBodyId(link.getOrderBody().getId());
			pickDTO.getOrderBodies().add(po);
		}
		pickDTO.setMessage("OK");
		return pickDTO;

	}

	/**
	 * 
	 * 
	 */
	public void updatePickSequenceForSkip(PickDetail pick, String user) {

		// put at end of the priority cycle
		pick.setPickSequence("ZZ" + pick.getPickSequence());
		pick.setDateUpdated(new Date());
		pick.setUpdatedBy(user);
		pickDetailRepository.save(pick);

		// Add TransactionHistory Skip data
		TransactionHistorySkip hist = new TransactionHistorySkip();
		hist.setCreatedBy(user);
		hist.setDateCreated(new Date());
		hist.setDateUpdated(new Date());
		hist.setUpdatedBy(user);
		hist.setAssociatedDocumentReference(pick.getDocumentReference());
		hist.setPartNumber(pick.getPartNumber());
		hist.setTxnQty(0.0);
		hist.setTransactionReferenceCode(ConstantHelper.TRANSACTION_CODE_SKIP);
		hist.setFromLocationCode(pick.getInventoryMaster().getLocationCode());
		hist.setSerialReference(pick.getInventoryMaster().getSerialReference());
		hist.setShortCode(ConstantHelper.TRANSACTION_CODE_SKIP);
		transHistSkipRepo.save(hist);

	}

	public void updatePickForMissingSerial(PickDetail pick, String user) {

		double qtyAllocated = pick.getQtyAllocated();
		/// Update Inventory with MISSING status
		InventoryMaster invMast = pick.getInventoryMaster();
		invMast.setInventoryStatus(invStatusRepo.findByInventoryStatusCode(ConstantHelper.INVENTORY_STATUS_MISSING));
		invMast.setAllocatedQty(invMast.getAllocatedQty().intValue() - pick.getQtyAllocated().intValue());
		invMast.setAvailableQty(invMast.getAvailableQty().doubleValue() + pick.getQtyAllocated().doubleValue());
		invMast.setInventoryQty(
				invMast.getAllocatedQty().intValue() + invMast.getAvailableQty().intValue() + invMast.getHoldQty());
		invMast.setUpdatedBy(user);
		invMast.setDateUpdated(new Date());

		inventoryMasterRepository.save(invMast);

		// Create a transaction History record

		// Add TransactionHistory Missing data
		TransactionHistory hist = new TransactionHistory();
		hist.setCreatedBy(user);
		hist.setDateCreated(new Date());
		hist.setDateUpdated(new Date());
		hist.setUpdatedBy(user);
		hist.setAssociatedDocumentReference(pick.getDocumentReference());
		hist.setPartNumber(pick.getPartNumber());
		hist.setTxnQty(invMast.getInventoryQty().doubleValue());
		hist.setTransactionReferenceCode(ConstantHelper.INVENTORY_STATUS_MISSING);
		hist.setFromLocationCode(pick.getInventoryMaster().getLocationCode());
		hist.setSerialReference(pick.getInventoryMaster().getSerialReference());
		hist.setShortCode(ConstantHelper.INVENTORY_STATUS_MISSING);
		transHistRepo.save(hist);
		// UPdate the pick allocated qty to 0

		pick.setQtyAllocated(0.0);
		pick.setProcessed(true);
		pick.setDateUpdated(new Date());
		pick.setUpdatedBy(user);
		pickDetailRepository.save(pick);
		
		List<PickOrderLink> links = pickOrderLinkRepository.findByPickDetail(pick);
		logger.info("Marking all links processed =" + links.size());
		for(PickOrderLink link : links){
			link.setProcessed(true);
			pickOrderLinkRepository.save(link);
		}
		

		PickBody body = pick.getPickBody();
		body.setQtyAllocated(body.getQtyAllocated() - qtyAllocated);
		body.setQtyShortage(body.getQtyShortage() + qtyAllocated);
		body.setDateUpdated(new Date());
		body.setUpdatedBy(user);
		pickBodyRepository.save(body);
		createPIHeaderForPart(pick.getPartNumber(), user);

	}

	private void createPIHeaderForPart(String partNumber, String user) {
		// PI Header will have either location or partNumber
		DocumentStatus open = documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);
		PIHeader piHeader = piHeaderRepository.findByPartNumberAndDocumentStatus(partNumber, open);
		
		if (piHeader == null) {
			piHeader = new PIHeader();
			// piHeader.setLocationCode(locationCode);
			piHeader.setPartNumber(partNumber);

			piHeader.setDocumentStatus(open);
			piHeader.setSerialNotExist(0.0);
			piHeader.setIncorrectLocation(0.0);
			piHeader.setDateCreated(new Date());
			piHeader.setCreatedBy(user);
			piHeader.setPiReasonCode(piReasonCodeRepo.findByShortCode(ConstantHelper.PI_REASON_CODE_F8_MISSING));
		}
		piHeader.setDateUpdated(new Date());
		piHeader.setUpdatedBy(user);

		piHeaderRepository.save(piHeader);
	}

}
