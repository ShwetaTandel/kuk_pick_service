package com.vantec.pick.business;

import java.util.List;

import org.springframework.stereotype.Service;

import com.vantec.pick.entity.DocumentDetail;
import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.PickBody;
import com.vantec.pick.entity.PickDetail;
import com.vantec.pick.entity.PickHeader;
import com.vantec.pick.model.PickDTO;
import com.vantec.pick.model.ProcessPickDTO;

@Service("pickDocumentInfo")
public interface PickDocumentInfo{
	
	
	public PickHeader createPickHeader(String userId, String pickType) ;
	
	public void createPickBodies(PickHeader pickHeader, List<OrderHeader> orderHeaders, String userId);

	public boolean addPickDetail(PickBody pickBody, InventoryMaster inventory, Double allocatedQty,OrderBody orderBody, String userId);
	
	public void copyPickBody(PickBody pickBodyToCopy, PickHeader newPickHeader, String user);

	public Boolean updatePickBodyWithQty(PickBody pickBody, Double allocatedQty, String userId);


	public void addBlockedPickDetail(PickBody pickBody, DocumentDetail documentDetail, String userId);
	
	public void processAutoPick(PickHeader header,String user) throws Exception;
	
	public PickDetail processPickDetail(PickDetail pick,ProcessPickDTO pickDTO, boolean serialScannedDiff) throws Exception;
	
	public PickDTO convertPickDetailEntityToView(PickDetail pickDetail);
	
	public void updatePickSequenceForSkip(PickDetail pick, String user);
	
	public void updatePickForMissingSerial(PickDetail pick, String user);
	
	public void unAllocatePickBody(PickDetail pick, PickBody pickBody, String user) ;
	

}
