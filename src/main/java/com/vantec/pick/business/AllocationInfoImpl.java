package com.vantec.pick.business;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.PickBody;
import com.vantec.pick.entity.PickDetail;
import com.vantec.pick.entity.PickHeader;
import com.vantec.pick.entity.RtsBody;
import com.vantec.pick.entity.RtsHeader;
import com.vantec.pick.repository.DocumentBodyRepository;
import com.vantec.pick.repository.DocumentDetailRepository;
import com.vantec.pick.repository.DocumentHeaderRepository;
import com.vantec.pick.repository.DocumentRegisterRepository;
import com.vantec.pick.repository.DocumentStatusRepository;
import com.vantec.pick.repository.InventoryMasterRepository;
import com.vantec.pick.repository.LocationRepository;
import com.vantec.pick.repository.OrderTypeRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.repository.PickBodyRepository;
import com.vantec.pick.repository.PickDetailRepository;
import com.vantec.pick.repository.PickOrderLinkRepository;
import com.vantec.pick.repository.RtsBodyRepository;
import com.vantec.pick.repository.RtsHeaderRepository;
import com.vantec.pick.repository.ScheduledWorkRepository;
import com.vantec.pick.repository.TagRepository;
import com.vantec.pick.repository.TransactionTypeRepository;
import com.vantec.pick.util.ConstantHelper;

@Service("allocationInfo")
public class AllocationInfoImpl implements AllocationInfo {

	private static Logger logger = LoggerFactory.getLogger(AllocationInfoImpl.class);

	@Autowired
	DocumentHeaderRepository documentHeaderRepository;

	@Autowired
	DocumentBodyRepository documentBodyRepository;

	@Autowired
	DocumentDetailRepository documentDetailRepository;
	
	@Autowired
	PickBodyRepository pickBodyRepository;
	
	@Autowired
	PickDetailRepository pickDetailRepository;

	@Autowired
	DocumentRegisterRepository documentRegisterRepository;

	@Autowired
	PartRepository partRepository;
	
	@Autowired
	TagRepository tagRepository;

	@Autowired
	TransactionTypeRepository transcationTypeRepository;

	@Autowired
	ScheduledWorkRepository scheduledWorkRepository;;

	@Autowired
	InventoryMasterRepository inventoryMasterRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	LocationRepository locationRepository;

	@Autowired
	InventoryMasterRepository inventoryRepository;
	
	@Autowired
	PickDocumentInfo pickDocumentInfo;
	
	@Autowired
	InventoryInfo inventoryInfo;
	
	@Autowired
	OrderTypeRepository orderTypeRepository;
	
	@Autowired
	RtsBodyRepository rtsBodyRepository;
	
	@Autowired
	RtsHeaderRepository rtsHeaderRepository;
	
	@Autowired
	PickOrderLinkRepository pickOrderLikRepo;
	
    
    public void allocateInventory(List<InventoryMaster> inventorys, PickBody pickBody,OrderBody orderBody, String userId) {
    	Integer bodyAllocQty = pickOrderLikRepo.getAllocatedForOrderBody(orderBody);
		double balanceQty = orderBody.getQtyExpected().doubleValue() - bodyAllocQty.doubleValue() - orderBody.getQtyTransacted().doubleValue();
		
		//logger.info("allocateInventory 1-> Balance QTY for order body id ="+orderBody.getId() + " is "+balanceQty);
		//logger.info("allocateInventory 2-> " + pickBody.toString());
		double allocatedQty =  0D;
		for (InventoryMaster inventory : inventorys) {
			if (balanceQty > 0) {
				double shortageQty = pickBody.getQtyShortage().doubleValue();
				int availableQuantity = inventory.getInventoryQty().intValue() - inventory.getAllocatedQty().intValue()	- inventory.getHoldQty().intValue();
				// allocatedQty = (balanceQty > availableQuantity) ? availableQuantity : balanceQty;
				allocatedQty = (shortageQty > availableQuantity) ? availableQuantity : shortageQty;
				allocatedQty = (allocatedQty > balanceQty) ? balanceQty : allocatedQty;
				if (allocatedQty > 0) {
					boolean pickAdded = pickDocumentInfo.addPickDetail(pickBody, inventory, allocatedQty, orderBody,userId);
					if (pickAdded) {
						balanceQty -= allocatedQty;
						pickDocumentInfo.updatePickBodyWithQty(pickBody, allocatedQty, userId);
						inventoryInfo.updateInventoryWithQty(inventory, allocatedQty, userId);
						//logger.info("allocateInventory 3-> " + pickBody.toString());
						//logger.info("allocateInventory 4-> " + inventory.toString());
					}
				}else{
					logger.info("allocateInventory AllocQty =0-> " + orderBody.toString());
					logger.info("allocateInventory AllocQty =0-> " + pickBody.toString());
				}
			}
			if (balanceQty <= 0) {
				break;
			}
		}
		if(inventorys.size() <= 0){
			//logger.info("Inventory not found for part Number -"+pickBody.getPartNumber());
			PickBody pickBodyItem = pickBodyRepository.findById(pickBody.getId());
			pickDocumentInfo.updatePickBodyWithQty(pickBodyItem,allocatedQty,userId);
			logger.info("allocateInventory 4->shortage="+ pickBody.toString());
			//logger.info("Checking RTS for emergency...");
			markRTSAsEmergency(pickBodyItem.getPartNumber());
		}
}
    
	private void markRTSAsEmergency(String partNumber) {

		DocumentStatus docStatus = documentStatusRepository.findByDocumentStatusCode("OPEN");
		List<RtsBody> rtsBodies = rtsBodyRepository.findByPartNumberAndDocumentStatus(partNumber, docStatus);
		if (rtsBodies != null && rtsBodies.size() > 0) {
			for (RtsBody rtsBody : rtsBodies) {
				RtsHeader dbHeader = rtsHeaderRepository.findOne(rtsBody.getRtsHeader().getId());
				if (dbHeader.getOrderType().equalsIgnoreCase(ConstantHelper.REWORK_RTS)) {
					dbHeader.setOrderType(ConstantHelper.REWORK_EMERG_RTS);
				} else if (dbHeader.getOrderType().equalsIgnoreCase(ConstantHelper.EXPEDITE_RTS)) {
					dbHeader.setOrderType(ConstantHelper.EXPEDITE_EMERG_RTS);
				}
				rtsHeaderRepository.save(dbHeader);
			}
		}
	}
}
