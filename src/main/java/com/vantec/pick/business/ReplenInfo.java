package com.vantec.pick.business;

import java.util.List;

import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.Location;
import com.vantec.pick.entity.Part;
import com.vantec.pick.entity.PickGroup;
import com.vantec.pick.entity.ReplenTask;
import com.vantec.pick.model.ReplenDTO;
import com.vantec.pick.model.ReplenScanDTO;

public interface ReplenInfo {
	
	public Boolean pickGroupCheck(Location location);
	
	public Boolean pickFaceQtyCheck(Location location);
	
	public InventoryMaster filterInventoryForReplen(String partNumber);
	
	public Boolean createReplenTask(ReplenDTO replenDTO,Location location);
	
	public Boolean editReplenTask(ReplenDTO replenDTO,ReplenTask replenTask);
	
	public Boolean createReplenHistory(ReplenDTO replenDTO,ReplenTask replenTask);

	public Boolean updateLocationWithPickGoup(String newLocationCode, Long pickGroupId);

	public List<ReplenTask> findReplenTask(Long pickGroupId);
	
	public String createVirtualPlt(String palletTypeName,String userId);

	public void updateReplenTask(ReplenTask replenTask, ReplenScanDTO replenScanDTO);

	void updateInventory(InventoryMaster inventory, ReplenScanDTO replenScanDTO);

	public Boolean transferReplenTask(ReplenTask replenTask,String userId,PickGroup pickGroup);

	public void updateReplenWithInventory(ReplenTask replenTask, InventoryMaster inventory, String userId);

	public void updateReplenWithScannedSerial(ReplenTask replenTask, String tagReference, String serial);

	public void createTransactionHistory(ReplenScanDTO replenScanDTO, ReplenTask replenTask, InventoryMaster inventory, Part part);

	public void updateInventoryWithStatusOK(InventoryMaster inventory, String locationCode,String userId);

	public void createTransactionHistoryREPLOC(String userId, InventoryMaster inventory, String locationCode,ReplenTask replenTask);
	
	public void updateInventoryWithStatusMISSING(InventoryMaster inventory,String userId);

	public void recordF6MissingHistory(ReplenTask replenTask, InventoryMaster inventory, String userId);

	public void freeInventoryFromTag(Long inventoryMasterId);
	
}
