package com.vantec.pick.business;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.TransactionHistory;
import com.vantec.pick.repository.F6MissingHistoryRepository;
import com.vantec.pick.repository.InventoryMasterRepository;
import com.vantec.pick.repository.InventoryStatusRepository;
import com.vantec.pick.repository.LocationRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.repository.PickGroupRepository;
import com.vantec.pick.repository.ReplenTaskRepository;
import com.vantec.pick.repository.ReplenTransferHistoryRepository;
import com.vantec.pick.repository.TransactionHistoryRepository;
import com.vantec.pick.util.ConstantHelper;

@Service("transactionInfo")
public class TransactionInfoImpl implements TransactionInfo {
	
	private static Logger logger = LoggerFactory.getLogger(TransactionInfoImpl.class);
	
	@Autowired
	InventoryMasterRepository inventoryMasterRepository;
	
	@Autowired
	TransactionHistoryRepository transactionHistoryRepository;
	
	@Autowired
	InventoryStatusRepository inventoryStatusRepository;
	
	@Autowired
	ReplenTaskRepository replenTaskRepository;
	
	@Autowired
	ReplenTransferHistoryRepository replenTransferHistoryRepository;
	
	@Autowired
	LocationRepository locationRepository;
	
	@Autowired
	PickGroupRepository pickGroupRepository;
	
	@Autowired
	TransactionHistoryRepository transHistRepo;
	
	@Autowired
	PartRepository partRepository;
	
	@Autowired
	F6MissingHistoryRepository f6MissingHistoryRepository;
	
	@Override
	public void recordTransaction(InventoryMaster inventory, String locationCode,String userId, String transactionCode) {
		TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setFromLocationCode(inventory.getLocationCode()); 
        transactionHistory.setToLocationCode(locationCode);
		transactionHistory.setToPlt(null);
		transactionHistory.setFromPlt(inventory.getTagReference());
        //transactionHistory.setCountQty(txn.getCountQty());
		//transactionHistory.setCustomerReference(txn.getCustomerReference());
		transactionHistory.setPartNumber(inventory.getPartNumber());
		transactionHistory.setRanOrOrder(inventory.getRanOrOrder()!= null ? inventory.getRanOrOrder() : ConstantHelper.NO_RAN);
		//transactionHistory.setReasonCode(txn.getReasonCode());
		transactionHistory.setRequiresDecant(inventory.getRequiresDecant());
		transactionHistory.setRequiresInspection(inventory.getRequiresInspection());
		transactionHistory.setTxnQty(inventory.getInventoryQty().doubleValue()); 
		transactionHistory.setSerialReference(inventory.getSerialReference());
		transactionHistory.setShortCode(transactionCode);
		transactionHistory.setTransactionReferenceCode(transactionCode);
		transactionHistory.setDateCreated(new Date());
		transactionHistory.setCreatedBy(userId);
		transactionHistory.setDateUpdated(new Date());
		transactionHistory.setUpdatedBy(userId);
		transHistRepo.save(transactionHistory);
		
	}
	
	@Override
	public void recordCloseTrainTransaction( String locationCode,String userId, Double txnQty, String pickRef, String orderRef) {
		TransactionHistory transactionHistory = new TransactionHistory();
        transactionHistory.setFromLocationCode(locationCode); 
       // transactionHistory.setToLocationCode(locationCode);
		transactionHistory.setToPlt(null);
        //transactionHistory.setCountQty(txn.getCountQty());
		//transactionHistory.setCustomerReference(txn.getCustomerReference());
		transactionHistory.setRanOrOrder(orderRef);
		transactionHistory.setAssociatedDocumentReference(pickRef);
		//transactionHistory.setReasonCode(txn.getReasonCode());
		transactionHistory.setTxnQty(txnQty); 
		transactionHistory.setShortCode(ConstantHelper.TRANSACTION_CODE_SHIP);
		transactionHistory.setTransactionReferenceCode(ConstantHelper.TRANSACTION_CODE_SHIP);
		transactionHistory.setDateCreated(new Date());
		transactionHistory.setCreatedBy(userId);
		transactionHistory.setDateUpdated(new Date());
		transactionHistory.setUpdatedBy(userId);
		transHistRepo.save(transactionHistory);
		
	}


}
