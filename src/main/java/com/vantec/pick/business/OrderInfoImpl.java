package com.vantec.pick.business;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.command.CustomValueGeneratorCommand;
import com.vantec.pick.command.DocumentReferenceGenerator;
import com.vantec.pick.entity.DocumentStatus;
import com.vantec.pick.entity.InspectionTaskBody;
import com.vantec.pick.entity.InspectionTaskHeader;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.Part;
import com.vantec.pick.model.OrderBodyDTO;
import com.vantec.pick.model.OrderHeaderDTO;
import com.vantec.pick.repository.DocumentStatusRepository;
import com.vantec.pick.repository.InspectionTaskBodyRepository;
import com.vantec.pick.repository.InspectionTaskHeaderRepository;
import com.vantec.pick.repository.LocationSubTypeRepository;
import com.vantec.pick.repository.OrderBodyRepository;
import com.vantec.pick.repository.OrderHeaderRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.util.ConstantHelper;

@Service("orderInfo")
public class OrderInfoImpl implements OrderInfo {

	@Autowired
	private DocumentStatusRepository documentStatusRepository;

	@Autowired
	private OrderHeaderRepository orderHeaderRepository;

	@Autowired
	private OrderBodyRepository orderBodyRepository;


	@Autowired
	private InspectionTaskHeaderRepository inspectionTaskHeaderRepository;
	
	@Autowired
	private InspectionTaskBodyRepository inspectionTaskBodyRepository;


	@Autowired
	PartRepository partRepository;


	@Autowired
	LocationSubTypeRepository locSubTypeRepo;
	/**
	 * Creates Order Header in Database
	 */
	public OrderHeader createOrderHeader(OrderHeaderDTO orderHeaderDTO) {
		OrderHeader orderHeader = new OrderHeader();
		DocumentStatus documentStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);

		orderHeader.setOrigin(orderHeaderDTO.getOrigin());
		orderHeader.setOrderTypeExt(orderHeaderDTO.getTypeIndicator());
		orderHeader.setCustomerReference(orderHeaderDTO.getCustomerReference());
		orderHeader.setDocumentStatus(documentStatus);
		orderHeader.setExpectedDeliveryTime(orderHeaderDTO.getExpectedDeliveryTime());
		orderHeader.setBaanSerial(orderHeaderDTO.getBaanSerial());
		orderHeader.setMachineModel(orderHeaderDTO.getMachineModel());
		orderHeader.setOrderType(orderHeaderDTO.getOrderType());
		orderHeader.setCreatedBy(ConstantHelper.SYSUSER);
		orderHeader.setUpdatedBy(ConstantHelper.SYSUSER);
		orderHeader.setDateCreated(new Date());
		orderHeader.setDateUpdated(new Date());
		orderHeader.setVersion(orderHeaderDTO.getVersion());
		orderHeaderRepository.save(orderHeader);

		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();
		String documentReference = customValueGeneratorCommand.generate(orderHeader.getId(), "COR");
		orderHeader.setDocumentReference(documentReference);
		OrderHeader savedHeader = orderHeaderRepository.save(orderHeader);
		return savedHeader;

	}

	
	/**
	 * Creates Inspect Task in Database
	 */
	public InspectionTaskHeader createInpectTaskHeader(OrderHeaderDTO orderHeaderDTO) {
		InspectionTaskHeader inspectTask = new InspectionTaskHeader();
		DocumentStatus documentStatus = documentStatusRepository
				.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN);

		inspectTask.setCustomerReference(orderHeaderDTO.getCustomerReference());
		inspectTask.setDocumentStatus(documentStatus);
		inspectTask.setExpectedDeliveryTime(orderHeaderDTO.getExpectedDeliveryTime());
		inspectTask.setBaanSerial(orderHeaderDTO.getBaanSerial());
		inspectTask.setMachineModel(orderHeaderDTO.getMachineModel());
		inspectTask.setOrderType(orderHeaderDTO.getOrderType());
		inspectTask.setCreatedBy(ConstantHelper.SYSUSER);
		inspectTask.setUpdatedBy(ConstantHelper.SYSUSER);
		inspectTask.setDateCreated(new Date());
		inspectTask.setDateUpdated(new Date());
		inspectionTaskHeaderRepository.save(inspectTask);

		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();
		String documentReference = customValueGeneratorCommand.generate(inspectTask.getId(), "INS");
		inspectTask.setDocumentReference(documentReference);
		InspectionTaskHeader savedTask = inspectionTaskHeaderRepository.save(inspectTask);
		return savedTask;

	}

	/***
	 * Create OrderBody in Database
	 */
	public void createOrderBody(OrderHeader orderHeader, OrderBodyDTO orderBodyDTO) {
		
		OrderBody orderBody = orderBodyRepository.findByOrderHeaderAndLineNo(orderHeader,orderBodyDTO.getOrderLine());
		if(orderBody == null){
			orderBody = new OrderBody();
		}
		
		orderBody.setOrderHeader(orderHeader);
		orderBody.setCustomerReference(orderHeader.getCustomerReference());
		orderBody.setDocumentReference(orderHeader.getDocumentReference());

		Part part = partRepository.findByPartNumber(orderBodyDTO.getPartNumber());
		if(part == null){
			//part = partRepository.findByPartNumber(ConstantHelper.NOT_EXIST_PART_NUMBER);
			return;
		} else if (part.getVendor() != null
				&& part.getVendor().getVendorReferenceCode().equalsIgnoreCase("PRD000311")) {
			//temporary hack for UCHIMURA (THAILAND) CO LTD vendor
			return;
		}else if(part.getIsAS400()){
		
			return;
		}
		orderBody.setPart(part);
		orderBody.setPartNumber(orderBodyDTO.getPartNumber());
		orderBody.setQtyExpected(orderBodyDTO.getExpectedQty());
		orderBody.setQtyTransacted(Double.valueOf(0));
		orderBody.setQtyTransmitted(Double.valueOf(0));
		orderBody.setDifference(orderBodyDTO.getExpectedQty());
		orderBody.setLineNo(orderBodyDTO.getOrderLine());
		orderBody.setZoneDestination(orderBodyDTO.getZoneDestination());
		orderBody.setBlocked(false);
		
		orderBody.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN));
		orderBody.setCreatedBy(ConstantHelper.SYSUSER);
		orderBody.setDateCreated(new Date());
		orderBody.setUpdatedBy(ConstantHelper.SYSUSER);
		orderBody.setDateUpdated(new Date());

		OrderBody savedBody = orderBodyRepository.save(orderBody);
		
		orderHeader.getOrderBodies().add(savedBody);
		orderHeaderRepository.save(orderHeader);

	}
	
	/***
	 * Create OrderBody in Database
	 */
	public void createInspectTaskBody(InspectionTaskHeader inspectTaskHeader, OrderBodyDTO orderBodyDTO) {
		
		InspectionTaskBody isnpectTaskBody = new InspectionTaskBody();
		isnpectTaskBody.setInspectTaskHeader(inspectTaskHeader);
		isnpectTaskBody.setCustomerReference(inspectTaskHeader.getCustomerReference());
		isnpectTaskBody.setDocumentReference(inspectTaskHeader.getDocumentReference());

		Part part = partRepository.findByPartNumber(orderBodyDTO.getPartNumber());
		if(part == null){
			//part = partRepository.findByPartNumber(ConstantHelper.NOT_EXIST_PART_NUMBER);
			return;
		}else if(part.getIsAS400()){
			return;
		}
		isnpectTaskBody.setPart(part);
		isnpectTaskBody.setPartNumber(orderBodyDTO.getPartNumber());
		isnpectTaskBody.setQtyExpected(orderBodyDTO.getExpectedQty());
		isnpectTaskBody.setQtyTransacted(Double.valueOf(0));
		isnpectTaskBody.setDifference(-(orderBodyDTO.getExpectedQty()));
		isnpectTaskBody.setLineNo(orderBodyDTO.getOrderLine());
		isnpectTaskBody.setVendorCode(orderBodyDTO.getZoneDestination());//The Zone destination is used as vendor code here
		
		isnpectTaskBody.setDocumentStatus(documentStatusRepository.findByDocumentStatusCode(ConstantHelper.DOCUMENT_STATUS_OPEN));
		isnpectTaskBody.setCreatedBy(ConstantHelper.SYSUSER);
		isnpectTaskBody.setDateCreated(new Date());
		isnpectTaskBody.setUpdatedBy(ConstantHelper.SYSUSER);
		isnpectTaskBody.setDateUpdated(new Date());

		InspectionTaskBody savedBody = inspectionTaskBodyRepository.save(isnpectTaskBody);
		inspectTaskHeader.getInspectTaskBodies().add(savedBody);
		inspectionTaskHeaderRepository.save(inspectTaskHeader);

	}

}
