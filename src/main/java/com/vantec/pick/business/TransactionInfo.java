package com.vantec.pick.business;

import com.vantec.pick.entity.InventoryMaster;

public interface TransactionInfo {
	
	public void recordTransaction(InventoryMaster inventory, String locationCode,String userId, String transactionCode);
	
	public void recordCloseTrainTransaction( String locationCode,String userId, Double txnQty, String pickRef, String orderRef);
	
}
