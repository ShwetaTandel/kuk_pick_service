package com.vantec.pick.business;

import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.Location;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.PickDetail;
import com.vantec.pick.repository.DocumentBodyRepository;
import com.vantec.pick.repository.DocumentDetailRepository;
import com.vantec.pick.repository.DocumentHeaderRepository;
import com.vantec.pick.repository.DocumentRegisterRepository;
import com.vantec.pick.repository.DocumentStatusRepository;
import com.vantec.pick.repository.InventoryMasterRepository;
import com.vantec.pick.repository.InventoryStatusRepository;
import com.vantec.pick.repository.LocationRepository;
import com.vantec.pick.repository.PartRepository;
import com.vantec.pick.repository.PickBodyRepository;
import com.vantec.pick.repository.PickDetailRepository;
import com.vantec.pick.repository.PickOrderLinkRepository;
import com.vantec.pick.repository.ScheduledWorkRepository;
import com.vantec.pick.repository.TagRepository;
import com.vantec.pick.repository.TransactionTypeRepository;
import com.vantec.pick.util.ConstantHelper;

@Service("inventoryInfo")
public class InventoryInfoImpl implements InventoryInfo {
	
	
	private static Logger logger = LoggerFactory.getLogger(InventoryInfoImpl.class);

	

	@Autowired
	DocumentHeaderRepository documentHeaderRepository;

	@Autowired
	DocumentBodyRepository documentBodyRepository;

	@Autowired
	DocumentDetailRepository documentDetailRepository;
	
	@Autowired
	PickBodyRepository pickBodyRepository;
	
	@Autowired
	PickDetailRepository pickDetailRepository;

	@Autowired
	DocumentRegisterRepository documentRegisterRepository;

	@Autowired
	PartRepository partRepository;
	
	@Autowired
	TagRepository tagRepository;

	@Autowired
	TransactionTypeRepository transcationTypeRepository;

	@Autowired
	ScheduledWorkRepository scheduledWorkRepository;;

	@Autowired
	InventoryMasterRepository inventoryMasterRepository;

	@Autowired
	DocumentStatusRepository documentStatusRepository;

	@Autowired
	LocationRepository locationRepository;

	@Autowired
	InventoryMasterRepository inventoryRepository;
	
	@Autowired
	PickDocumentInfo pickDocumentInfo;
	
	@Autowired
	InventoryStatusRepository inventoryStatusRepository;
	
	@Autowired
	PickOrderLinkRepository pickOrderLinkRepo;




	/****
	 * Priority 1 - Check for stock in location types MA, where is inv status OK, inspect false 
	 * Priority 2 - Check for stock in pick priority locations, where inv status is OK and inspect flag false
	 * Priority 3 - Check for stock in location type DZ where inv status is OK and inspect false
	 * Priority 4 - Check for stock in warehouse locations with no priority pick indicator , where inv status is OK and inspect false
	 * 
	 */
	public List<InventoryMaster> filterInventoryForAllocation(OrderBody orderBody) {
		
		Map<Long,InventoryMaster>  allocatableInventorys = new LinkedHashMap<Long,InventoryMaster>(); 
		List<InventoryMaster> dbInventorys = inventoryMasterRepository.findByPartNumberOrderByDateCreatedAsc(orderBody.getPartNumber());
		
		Map<Long,InventoryMaster> maInventorys = dbInventorys.stream()
				.filter(inventory -> inventory.getCurrentLocation() != null
						&& inventory.getCurrentLocation().getLocationType().getIsPickable()
				        && ConstantHelper.MARSHALLING_LOCATION_TYPE_CODE.equalsIgnoreCase(inventory.getCurrentLocation().getLocationType().getLocationTypeCode())  
		        		&& inventory.getInventoryStatus() != null
				        && inventory.getInventoryStatus().getAllocatable()
				        //&& ("OK").equalsIgnoreCase(inventory.getInventoryStatus().getInventoryStatusCode())
						//&& inventory.getParentTag() == null
                        && inventory.getAvailableQty() > 0
						&& !inventory.getRequiresInspection())
				.collect(toMap(inventory->inventory.getId(), inventory->inventory));
		
		
		Map<Long,InventoryMaster>  pickFaceInventorys = dbInventorys.stream()
				.filter(inventory -> inventory.getCurrentLocation() != null
						&& inventory.getCurrentLocation().getLocationType().getIsPickable()
				        && inventory.getCurrentLocation().getPickGroup() != null
		        		&& inventory.getInventoryStatus() != null
		        		&& inventory.getInventoryStatus().getAllocatable()
				       // && ("OK").equalsIgnoreCase(inventory.getInventoryStatus().getInventoryStatusCode())
					  //	&& inventory.getParentTag() == null
                        && inventory.getAvailableQty() > 0
						&& !inventory.getRequiresInspection())
				.collect(toMap(inventory->inventory.getId(), inventory->inventory));
		
		Map<Long,InventoryMaster>  dzInventorys = dbInventorys.stream()
				.filter(inventory -> inventory.getCurrentLocation() != null
						&& inventory.getCurrentLocation().getLocationType().getIsPickable()
				        && ConstantHelper.DROP_ZONE_LOCATION_TYPE_CODE.equalsIgnoreCase(inventory.getCurrentLocation().getLocationType().getLocationTypeCode())  
		        		&& inventory.getInventoryStatus() != null
		        		&& inventory.getInventoryStatus().getAllocatable()
				        //&& ("OK").equalsIgnoreCase(inventory.getInventoryStatus().getInventoryStatusCode())
						//&& inventory.getParentTag() == null
                        && inventory.getAvailableQty() > 0
						&& !inventory.getRequiresInspection())
				.collect(toMap(inventory->inventory.getId(), inventory->inventory));
		
		Map<Long,InventoryMaster> inventorys = dbInventorys.stream()
				.filter(inventory -> inventory.getCurrentLocation() != null
						&& inventory.getCurrentLocation().getLocationType().getIsPickable()
						&& inventory.getInventoryStatus() != null
						&& inventory.getInventoryStatus().getAllocatable()
				        //&& ("OK").equalsIgnoreCase(inventory.getInventoryStatus().getInventoryStatusCode())
						//&& inventory.getParentTag() == null
                        && inventory.getAvailableQty() > 0
						&& !inventory.getRequiresInspection())
				.collect(toMap(inventory->inventory.getId(), inventory->inventory));
		if(maInventorys.size() > 0){
			allocatableInventorys.putAll(maInventorys);
			logger.info("Inventory found for MA partNumber - "+ orderBody.getPartNumber());
		}
		if(pickFaceInventorys.size() > 0){
			allocatableInventorys.putAll( pickFaceInventorys);
			logger.info("Inventory found for PickFace partNumber-  "+ orderBody.getPartNumber());
		}
		if (dzInventorys.size() > 0){
			logger.info("Inventory found for DZ partNumber-  "+ orderBody.getPartNumber());
			allocatableInventorys.putAll(dzInventorys);
		}if(inventorys.size() > 0){
			logger.info("Inventory found for General partNumber-  "+ orderBody.getPartNumber());
			allocatableInventorys.putAll(inventorys);
		}
		
		return new ArrayList<InventoryMaster>(allocatableInventorys.values());
	}
	
	
	
	public void updateInventoryWithQty(InventoryMaster inventory, Double allocatedQty, String userId) {
		inventory.setAllocatedQty(inventory.getAllocatedQty() +  allocatedQty.intValue());
		inventory.setAvailableQty(inventory.getInventoryQty().doubleValue() - inventory.getAllocatedQty());
		inventory.setUpdatedBy(userId);
		inventory.setDateUpdated(new Date());
		inventoryMasterRepository.save(inventory);
		
	}
	public void reduceInventoryByScannedQty(PickDetail pickDetail, Double scannedQty) {
		
		if(pickDetail.getInventoryMaster() != null){
			InventoryMaster inventory = inventoryMasterRepository.findById(pickDetail.getInventoryMaster().getId());
			logger.info("reduceInventoryByScannedQty Before->"+inventory.toString());
			inventory.setAllocatedQty(inventory.getAllocatedQty().intValue() - scannedQty.intValue());
			inventory.setAvailableQty(inventory.getAvailableQty().doubleValue() + scannedQty.doubleValue());
			inventory.setInventoryQty(inventory.getAvailableQty().intValue() + inventory.getAllocatedQty().intValue() + inventory.getHoldQty().intValue());
			inventory.setDateUpdated(new Date());
			inventoryMasterRepository.save(inventory);
			logger.info("reduceInventoryByScannedQty After->"+inventory.toString());
		}
		
	}
    
	public void releaseInventory(PickDetail pickDetail) {
		
		if(pickDetail.getInventoryMaster() != null){
			InventoryMaster inventory = inventoryMasterRepository.findById(pickDetail.getInventoryMaster().getId());
			logger.info("releaseInventory Before->"+inventory.toString());
			inventory.setAvailableQty(inventory.getAvailableQty().doubleValue() + pickDetail.getQtyAllocated().doubleValue());
			inventory.setAllocatedQty(inventory.getAllocatedQty().intValue() - pickDetail.getQtyAllocated().intValue());
			inventory.setInventoryQty(inventory.getAvailableQty().intValue() + inventory.getAllocatedQty().intValue() + inventory.getHoldQty().intValue());
			inventory.setDateUpdated(new Date());
			inventoryMasterRepository.save(inventory);
			logger.info("releaseInventory After->"+inventory.toString());
		}
		
	}
	public void unallocateInventory(InventoryMaster inventory) {
		
		if(inventory != null){
			logger.info("unallocateInventory Before->"+inventory.toString());
			List<PickDetail> picks = pickDetailRepository.findByInventoryMaster(inventory);
			inventory.setAvailableQty(inventory.getInventoryQty().doubleValue());
			inventory.setAllocatedQty(0);
			inventory.setDateUpdated(new Date());
			inventoryMasterRepository.save(inventory);
			logger.info("unallocateInventory After->"+inventory.toString());
			List <InventoryMaster> list = new ArrayList<InventoryMaster>();
			list.add(inventory);
			pickDetailRepository.markInventoryNullInPick(list);
			pickOrderLinkRepo.delete(pickOrderLinkRepo.findAllForPickDetails(picks));
			logger.info("unallocateInventory Pick Order link deleted->"+picks.size());
		}
		
	}

	
	@Override
	public void updateInventoryLocationForPTTT(InventoryMaster inventory, String locationCode,String userId) {
		Location location = locationRepository.findByLocationCode(ConstantHelper.TRAIN_LOCATION_CODE);
		inventory.setLocationCode(locationCode);
		inventory.setCurrentLocation(location);
		inventory.setLocation(location);
		inventory.setUpdatedBy(userId);
		inventory.setDateUpdated(new Date());
		inventoryMasterRepository.save(inventory);
		
	}
	@Override
	public void updateInventoryLocation(InventoryMaster inventory, String locationCode,String userId) {
		Location location = locationRepository.findByLocationCode(locationCode);
		inventory.setLocationCode(locationCode);
		inventory.setCurrentLocation(location);
		inventory.setLocation(location);
		inventory.setUpdatedBy(userId);
		inventory.setDateUpdated(new Date());
		inventoryMasterRepository.save(inventory);
		
	}
	
	@Override
	public void deleteInventory(InventoryMaster inventory) {
		List<PickDetail> pickDetails = pickDetailRepository.findByInventoryMaster(inventory);
		for(PickDetail pickDetail:pickDetails){
			pickDetail.setInventoryMaster(null);
			pickDetailRepository.save(pickDetail);
		}
		inventoryMasterRepository.delete(inventory);
	}
	


}
