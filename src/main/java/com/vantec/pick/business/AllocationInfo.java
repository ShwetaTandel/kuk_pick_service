package com.vantec.pick.business;

import java.util.List;

import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.OrderBody;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.entity.PickBody;

public interface AllocationInfo {

	public void allocateInventory(List<InventoryMaster> inventorys, PickBody pickBody, 
			OrderBody orderBody, String userId);
	
}
