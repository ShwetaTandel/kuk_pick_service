package com.vantec.pick.business;

import com.vantec.pick.entity.InspectionTaskHeader;
import com.vantec.pick.entity.OrderHeader;
import com.vantec.pick.model.OrderBodyDTO;
import com.vantec.pick.model.OrderHeaderDTO;

public interface OrderInfo {
	
	public OrderHeader createOrderHeader(OrderHeaderDTO orderHeaderDTO);
	public void createOrderBody(OrderHeader orderHeader, OrderBodyDTO orderBodyDTO);
	public InspectionTaskHeader createInpectTaskHeader(OrderHeaderDTO orderHeaderDTO);
	public void createInspectTaskBody(InspectionTaskHeader inspecTaskHeaders, OrderBodyDTO orderBodyDTO);
}
