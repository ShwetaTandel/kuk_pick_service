package com.vantec.pick.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DocumentBodyObject {
	
	
	private String partNumber;
	private Double expectedQty;
	private Integer orderLine;
	
	private String zeugn;
	private String abald;
	
	private String serialReference;
	private String meins;
	

	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Double getExpectedQty() {
		return expectedQty;
	}
	public void setExpectedQty(Double expectedQty) {
		this.expectedQty = expectedQty;
	}
	public Integer getOrderLine() {
		return orderLine;
	}
	public void setOrderLine(Integer orderLine) {
		this.orderLine = orderLine;
	}
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public String getZeugn() {
		return zeugn;
	}
	public void setZeugn(String zeugn) {
		this.zeugn = zeugn;
	}
	public String getAbald() {
		return abald;
	}
	public void setAbald(String abald) {
		this.abald = abald;
	}
	public String getMeins() {
		return meins;
	}
	public void setMeins(String meins) {
		this.meins = meins;
	}
	
	
	
	
	

}
