package com.vantec.pick.model;

public class ProcessPickDTO {

	private Long pickId;
	private Double qtyScanned;
	private String userId;
	private String serialScanned;
	private String pallet;
	private String toLocation;
	private String transactionCode;
	private boolean missingSerial;
	private boolean skip;
	
	public boolean isSkip() {
		return skip;
	}
	public void setSkip(boolean skip) {
		this.skip = skip;
	}
	public boolean isMissingSerial() {
		return missingSerial;
	}
	public void setMissingSerial(boolean missingSerial) {
		this.missingSerial = missingSerial;
	}
	public Double getQtyScanned() {
		return qtyScanned;
	}
	public void setQtyScanned(Double qtyScanned) {
		this.qtyScanned = qtyScanned;
	}
	public Long getPickId() {
		return pickId;
	}
	public void setPickId(Long pickId) {
		this.pickId = pickId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getSerialScanned() {
		return serialScanned;
	}
	public void setSerialScanned(String serialScanned) {
		this.serialScanned = serialScanned;
	}
	public String getPallet() {
		return pallet;
	}
	public void setPallet(String pallet) {
		this.pallet = pallet;
	}
	public String getToLocation() {
		return toLocation;
	}
	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	
	public String getAllValues(){
		return "Pick Id = " + pickId + " ,qtyScanned =  " + qtyScanned + " ,serialScanned= " + serialScanned
				+ ", pallet=" + pallet + " , toLocation=" + toLocation + ", transactionCode=" + transactionCode
				+ " , missingSerial +" + missingSerial + ", skip=" + skip + ", userid = "+userId;
	}
		
}
