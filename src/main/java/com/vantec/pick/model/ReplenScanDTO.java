package com.vantec.pick.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ReplenScanDTO {
	
	

	private String tagReference;
	private Long pickGroupId; 
	private Long replenTaskId; 
	private String scannedSerial;
	private String suggestedSerial;
	private String locationCode;
	private String userId;
	
	public String getTagReference() {
		return tagReference;
	}
	public void setTagReference(String tagReference) {
		this.tagReference = tagReference;
	}
	public Long getPickGroupId() {
		return pickGroupId;
	}
	public void setPickGroupId(Long pickGroupId) {
		this.pickGroupId = pickGroupId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getReplenTaskId() {
		return replenTaskId;
	}
	public void setReplenTaskId(Long replenTaskId) {
		this.replenTaskId = replenTaskId;
	}
	public String getScannedSerial() {
		return scannedSerial;
	}
	public void setScannedSerial(String scannedSerial) {
		this.scannedSerial = scannedSerial;
	}
	public String getSuggestedSerial() {
		return suggestedSerial;
	}
	public void setSuggestedSerial(String suggestedSerial) {
		this.suggestedSerial = suggestedSerial;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	
	
	
	
	
}
