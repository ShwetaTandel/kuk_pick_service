package com.vantec.pick.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vantec.pick.entity.InventoryMaster;
import com.vantec.pick.entity.Location;
import com.vantec.pick.entity.OrderType;
import com.vantec.pick.entity.PickDetail;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TransactionDetailRequest {
	
	private String customerReference;
	private String partNumber; //MTT
	private String serialReference; //MTT
	private String toLocationHashCode; //PTM //MTT
	private String currentUser;
	private Long pickDetailId;
	private String plt; //PTM from pallet
	private Double pickQty;
	private String transactionTypeId;
	
	
	private Double qty;
	private String documentReference;
	private InventoryMaster inventoryAllocated;
	private InventoryMaster inventoryScanned;
	private PickDetail pickDetail;
	private OrderType orderType;
	private Location toLocation;
	private String toLocationCode;
	private String fromLocationCode;
	
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public String getToLocationHashCode() {
		return toLocationHashCode;
	}
	public void setToLocationHashCode(String toLocationHashCode) {
		this.toLocationHashCode = toLocationHashCode;
	}
	public String getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(String currentUser) {
		this.currentUser = currentUser;
	}
	public String getCustomerReference() {
		return customerReference;
	}
	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}
	public String getPlt() {
		return plt;
	}
	public void setPlt(String plt) {
		this.plt = plt;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getDocumentReference() {
		return documentReference;
	}
	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}
	public Long getPickDetailId() {
		return pickDetailId;
	}
	public void setPickDetailId(Long pickDetailId) {
		this.pickDetailId = pickDetailId;
	}
	public InventoryMaster getInventoryAllocated() {
		return inventoryAllocated;
	}
	public void setInventoryAllocated(InventoryMaster inventoryAllocated) {
		this.inventoryAllocated = inventoryAllocated;
	}
	public PickDetail getPickDetail() {
		return pickDetail;
	}
	public void setPickDetail(PickDetail pickDetail) {
		this.pickDetail = pickDetail;
	}
	public Double getPickQty() {
		return pickQty;
	}
	public void setPickQty(Double pickQty) {
		this.pickQty = pickQty;
	}
	public OrderType getOrderType() {
		return orderType;
	}
	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
	public InventoryMaster getInventoryScanned() {
		return inventoryScanned;
	}
	public void setInventoryScanned(InventoryMaster inventoryScanned) {
		this.inventoryScanned = inventoryScanned;
	}
	public String getTransactionTypeId() {
		return transactionTypeId;
	}
	public void setTransactionTypeId(String transactionTypeId) {
		this.transactionTypeId = transactionTypeId;
	}
	public Location getToLocation() {
		return toLocation;
	}
	public void setToLocation(Location toLocation) {
		this.toLocation = toLocation;
	}
	public String getFromLocationCode() {
		return fromLocationCode;
	}
	public void setFromLocationCode(String fromLocationCode) {
		this.fromLocationCode = fromLocationCode;
	}
	public String getToLocationCode() {
		return toLocationCode;
	}
	public void setToLocationCode(String toLocationCode) {
		this.toLocationCode = toLocationCode;
	}
	
	
    
    
    
}
