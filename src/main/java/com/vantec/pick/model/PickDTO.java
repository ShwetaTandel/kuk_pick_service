package com.vantec.pick.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PickDTO {
	private Long id;
	private Long inventoryMasterId;
	private Long pickBodyId;
	private Long partId;
	private String documentReference;
	private String partNumber;
	private String serialReference;
	private String serialReferenceScanned;
	private String locationCode;
	private Double qtyAllocated;
	private Double bodyQtyAllocated;
	private Double qtyScanned;
	private Long pickPriority;
	private Boolean processed;
	private Boolean blocked;
	private String inUseBy;
	private String zoneDestination;
	private String pickSequence;
	private String createdBy;
	private Date dateCreated;
	private String updatedBy;
	private Date dateUpdated;
	private String message;
	private Boolean isStandardPick;
	private Boolean noSerialScan;
	private Boolean isFullBoxPick;
	private Boolean isModular;
	private String taskType;
	private String pallet;
	private String transactionCode;
	private String toLocationCode;
	private String trainLocation;
	private Long processedPickId;
	
	public Long getProcessedPickId() {
		return processedPickId;
	}
	public void setProcessedPickId(Long processedPickId) {
		this.processedPickId = processedPickId;
	}
	public String getTrainLocation() {
		return trainLocation;
	}
	public void setTrainLocation(String trainLocation) {
		this.trainLocation = trainLocation;
	}
	public String getToLocationCode() {
		return toLocationCode;
	}
	public void setToLocationCode(String toLocationCode) {
		this.toLocationCode = toLocationCode;
	}
	public Double getBodyQtyAllocated() {
		return bodyQtyAllocated;
	}
	public void setBodyQtyAllocated(Double bodyQtyAllocated) {
		this.bodyQtyAllocated = bodyQtyAllocated;
	}
	public String getPallet() {
		return pallet;
	}
	public void setPallet(String pallet) {
		this.pallet = pallet;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	private List<PickOrder> orderBodies = new ArrayList<PickDTO.PickOrder>();
	
	
	
	public Boolean getIsStandardPick() {
		return isStandardPick;
	}
	public void setIsStandardPick(Boolean isStandardPick) {
		this.isStandardPick = isStandardPick;
	}
	public Boolean getNoSerialScan() {
		return noSerialScan;
	}
	public void setNoSerialScan(Boolean noSerialScan) {
		this.noSerialScan = noSerialScan;
	}
	public Boolean getIsFullBoxPick() {
		return isFullBoxPick;
	}
	public void setIsFullBoxPick(Boolean isFullBoxPick) {
		this.isFullBoxPick = isFullBoxPick;
	}
	public Boolean getIsModular() {
		return isModular;
	}
	public void setIsModular(Boolean isModular) {
		this.isModular = isModular;
	}
	public Long getPartId() {
		return partId;
	}
	public void setPartId(Long partId) {
		this.partId = partId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Long getPickBodyId() {
		return pickBodyId;
	}
	public void setPickBodyId(Long pickBodyId) {
		this.pickBodyId = pickBodyId;
	}
	public List<PickOrder> getOrderBodies() {
		return orderBodies;
	}
	public void setOrderBodies(List<PickOrder> orderBodies) {
		this.orderBodies = orderBodies;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getInventoryMasterId() {
		return inventoryMasterId;
	}
	public void setInventoryMasterId(Long inventoryMasterId) {
		this.inventoryMasterId = inventoryMasterId;
	}

	public String getDocumentReference() {
		return documentReference;
	}
	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public String getSerialReferenceScanned() {
		return serialReferenceScanned;
	}
	public void setSerialReferenceScanned(String serialReferenceScanned) {
		this.serialReferenceScanned = serialReferenceScanned;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public Double getQtyAllocated() {
		return qtyAllocated;
	}
	public void setQtyAllocated(Double qtyAllocated) {
		this.qtyAllocated = qtyAllocated;
	}
	public Double getQtyScanned() {
		return qtyScanned;
	}
	public void setQtyScanned(Double qtyScanned) {
		this.qtyScanned = qtyScanned;
	}
	
	public Long getPickPriority() {
		return pickPriority;
	}
	public void setPickPriority(Long pickPriority) {
		this.pickPriority = pickPriority;
	}
	public Boolean getProcessed() {
		return processed;
	}
	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}
	public Boolean getBlocked() {
		return blocked;
	}
	public void setBlocked(Boolean blocked) {
		this.blocked = blocked;
	}
	public String getInUseBy() {
		return inUseBy;
	}
	public void setInUseBy(String inUseBy) {
		this.inUseBy = inUseBy;
	}
	public String getZoneDestination() {
		return zoneDestination;
	}
	public void setZoneDestination(String zoneDestination) {
		this.zoneDestination = zoneDestination;
	}
	public String getPickSequence() {
		return pickSequence;
	}
	public void setPickSequence(String pickSequence) {
		this.pickSequence = pickSequence;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getDateUpdated() {
		return dateUpdated;
	}
	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}
	public class PickOrder{
		Integer lineNo;
		Long orderBodyId;
		public Integer getLineNo() {
			return lineNo;
		}
		public void setLineNo(Integer lineNo) {
			
			this.lineNo = lineNo;
		}
		public Long getOrderBodyId() {
			return orderBodyId;
		}
		public void setOrderBodyId(Long orderBodyId) {
			this.orderBodyId = orderBodyId;
		}
		
		
	}
	
	@Override
	public String toString() {
		return "PickDTO [id=" + id + ", inventoryMasterId=" + inventoryMasterId + ", pickBodyId=" + pickBodyId
				+ ", partId=" + partId + ", documentReference=" + documentReference + ", partNumber=" + partNumber
				+ ", serialReference=" + serialReference + ", serialReferenceScanned=" + serialReferenceScanned
				+ ", locationCode=" + locationCode + ", qtyAllocated=" + qtyAllocated + ", bodyQtyAllocated="
				+ bodyQtyAllocated + ", qtyScanned=" + qtyScanned + ", pickPriority=" + pickPriority + ", processed="
				+ processed + ", blocked=" + blocked + ", inUseBy=" + inUseBy + ", zoneDestination=" + zoneDestination
				+ ", pickSequence=" + pickSequence + ", createdBy=" + createdBy + ", dateCreated=" + dateCreated
				+ ", updatedBy=" + updatedBy + ", dateUpdated=" + dateUpdated + ", message=" + message
				+ ", isStandardPick=" + isStandardPick + ", noSerialScan=" + noSerialScan + ", isFullBoxPick="
				+ isFullBoxPick + ", isModular=" + isModular + ", taskType=" + taskType + ", pallet=" + pallet
				+ ", transactionCode=" + transactionCode + ", toLocationCode=" + toLocationCode + ", trainLocation="
				+ trainLocation + ", orderBodies=" + orderBodies + "]";
	}
	
}
