package com.vantec.pick.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vantec.pick.entity.Part;
import com.vantec.pick.entity.ProductType;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DocumentBodyDTO {
	
	private String body;
	private String partNumber;
	private Integer expectedQty;
	private Integer orderLine;
	
	private ProductType productType;
	private Part part;
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Integer getExpectedQty() {
		return expectedQty;
	}
	public void setExpectedQty(Integer expectedQty) {
		this.expectedQty = expectedQty;
	}
	public Integer getOrderLine() {
		return orderLine;
	}
	public void setOrderLine(Integer orderLine) {
		this.orderLine = orderLine;
	}
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	public Part getPart() {
		return part;
	}
	public void setPart(Part part) {
		this.part = part;
	}
	
    
	
	
	

}
