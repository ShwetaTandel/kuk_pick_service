package com.vantec.pick.model;

import java.util.List;

public class DocumentHeaderObject {
	
	private String tanum;
	private String bwlvs;
	private String lgnum;
	private String jisInd;
	private String benum;
	private String zreqdatu;
	private String zreqzeit;
	private String zcarrol;
	private List<DocumentBodyObject> bodies;
	

	
	public String getTanum() {
		return tanum;
	}
	public void setTanum(String tanum) {
		this.tanum = tanum;
	}
	public String getBwlvs() {
		return bwlvs;
	}
	public void setBwlvs(String bwlvs) {
		this.bwlvs = bwlvs;
	}
	public String getLgnum() {
		return lgnum;
	}
	public void setLgnum(String lgnum) {
		this.lgnum = lgnum;
	}
	public String getJisInd() {
		return jisInd;
	}
	public void setJisInd(String jisInd) {
		this.jisInd = jisInd;
	}
	public List<DocumentBodyObject> getBodies() {
		return bodies;
	}
	public void setBodies(List<DocumentBodyObject> bodies) {
		this.bodies = bodies;
	}
	public String getZreqdatu() {
		return zreqdatu;
	}
	public void setZreqdatu(String zreqdatu) {
		this.zreqdatu = zreqdatu;
	}
	public String getZreqzeit() {
		return zreqzeit;
	}
	public void setZreqzeit(String zreqzeit) {
		this.zreqzeit = zreqzeit;
	}
	public String getBenum() {
		return benum;
	}
	public void setBenum(String benum) {
		this.benum = benum;
	}
	public String getZcarrol() {
		return zcarrol;
	}
	public void setZcarrol(String zcarrol) {
		this.zcarrol = zcarrol;
	}
	
    
    
}
