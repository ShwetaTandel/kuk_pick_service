package com.vantec.pick.model;

import java.util.List;

public class OrderObjects {
	private String header;
	private List<OrderDetails> details;
	
	
	public OrderObjects(String header, List<OrderDetails> details) {
		super();
		this.header = header;
		this.details = details;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public List<OrderDetails> getDetails() {
		return details;
	}
	public void setDetails(List<OrderDetails> details) {
		this.details = details;
	}
}
