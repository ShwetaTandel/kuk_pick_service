package com.vantec.pick.model;

import java.util.ArrayList;
import java.util.List;

public class AllocateDTO {

	private List<String> docRef= new ArrayList<String>();
	private String userId;
	private String partNumber;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<String> getDocRef() {
		return docRef;
	}

	public void setDocRef(List<String> docRef) {
		this.docRef = docRef;
	}

	public String getAllDocRef() {
		if (docRef != null)
			return String.join(", ", docRef);
		else
			return "";
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

}
