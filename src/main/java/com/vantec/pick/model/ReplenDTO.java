package com.vantec.pick.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ReplenDTO {
	
	
	//create
	private String locationCode;
	private String partNumber;
	private String userId;
	
	
	
	//edit
	private Long pickGroupId;
	private Long replenTaskId;
	private Integer priority;
	private String newLocationCode;
	
	//f6Pick
	private String serialReference;
	
	
    
    
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getReplenTaskId() {
		return replenTaskId;
	}
	public void setReplenTaskId(Long replenTaskId) {
		this.replenTaskId = replenTaskId;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Long getPickGroupId() {
		return pickGroupId;
	}
	public void setPickGroupId(Long pickGroupId) {
		this.pickGroupId = pickGroupId;
	}
	public String getNewLocationCode() {
		return newLocationCode;
	}
	public void setNewLocationCode(String newLocationCode) {
		this.newLocationCode = newLocationCode;
	}
	
}
