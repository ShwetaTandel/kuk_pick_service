package com.vantec.pick.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ReplenResponseDTO {
	
	
	private Long replenTaskId;
	private String fromlocationCode;
	private String partNumber;
	private String serialReference;
	private String reason;
	
	public String getFromlocationCode() {
		return fromlocationCode;
	}
	public void setFromlocationCode(String fromlocationCode) {
		this.fromlocationCode = fromlocationCode;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getSerialReference() {
		return serialReference;
	}
	public void setSerialReference(String serialReference) {
		this.serialReference = serialReference;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Long getReplenTaskId() {
		return replenTaskId;
	}
	public void setReplenTaskId(Long replenTaskId) {
		this.replenTaskId = replenTaskId;
	}
	
	
	
}
