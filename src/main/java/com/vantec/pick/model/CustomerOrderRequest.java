package com.vantec.pick.model;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CustomerOrderRequest {
	
	private DocumentHeaderDTO documentHeaderDTO;
	private List<DocumentBodyDTO> documentBodyDTO;
	
	public DocumentHeaderDTO getDocumentHeaderDTO() {
		return documentHeaderDTO;
	}
	public void setDocumentHeaderDTO(DocumentHeaderDTO documentHeaderDTO) {
		this.documentHeaderDTO = documentHeaderDTO;
	}
	public List<DocumentBodyDTO> getDocumentBodyDTO() {
		return documentBodyDTO;
	}
	public void setDocumentBodyDTO(List<DocumentBodyDTO> documentBodyDTO) {
		this.documentBodyDTO = documentBodyDTO;
	}
	
	
    
}
