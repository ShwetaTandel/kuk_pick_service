package com.vantec.pick.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class DocumentHeaderDTO {
	
	private Integer registerValue;

	public Integer getRegisterValue() {
		return registerValue;
	}

	public void setRegisterValue(Integer registerValue) {
		this.registerValue = registerValue;
	}
	
	
   
}
