package com.vantec.pick.model;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.vantec.pick.entity.Part;
import com.vantec.pick.entity.ProductType;

@JsonIgnoreProperties(ignoreUnknown=true)
public class OrderBodyDTO {
	
	private String body;
	private String partNumber;
	private Double expectedQty;
	private Integer orderLine;
	private String zoneDestination;
	private Date expectedDeliveryDate;
	
	public String getZoneDestination() {
		return zoneDestination;
	}
	public void setZoneDestination(String zoneDestination) {
		this.zoneDestination = zoneDestination;
	}
	private ProductType productType;
	private Part part;
	
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public Double getExpectedQty() {
		return expectedQty;
	}
	public void setExpectedQty(Double expectedQty) {
		this.expectedQty = expectedQty;
	}
	public Integer getOrderLine() {
		return orderLine;
	}
	public void setOrderLine(Integer orderLine) {
		this.orderLine = orderLine;
	}
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	public Part getPart() {
		return part;
	}
	public void setPart(Part part) {
		this.part = part;
	}
	public Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}
	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	
    
	
	
	

}
