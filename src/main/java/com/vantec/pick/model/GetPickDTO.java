package com.vantec.pick.model;

public class GetPickDTO {

	private String pickDocRef;
	private String userId;
	private String pickTask;
	private String train;
	private String part;
	private boolean allocateRefresh = true;
	
	public boolean isAllocateRefresh() {
		return allocateRefresh;
	}
	public void setAllocateRefresh(boolean allocateRefresh) {
		this.allocateRefresh = allocateRefresh;
	}
	public String getPickDocRef() {
		return pickDocRef;
	}
	public void setPickDocRef(String pickDocRef) {
		this.pickDocRef = pickDocRef;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPickTask() {
		return pickTask;
	}
	public void setPickTask(String pickTask) {
		this.pickTask = pickTask;
	}
	public String getTrain() {
		return train;
	}
	public void setTrain(String train) {
		this.train = train;
	}
	public String getPart() {
		return part;
	}
	public void setPart(String part) {
		this.part = part;
	}

	public String getAllValues(){
		return "pickDocRef="+pickDocRef + "-userId=" + userId + "-pickTask=+ " + pickTask + "-train=" +train+ "-part=" +part;  
	}
	
}
