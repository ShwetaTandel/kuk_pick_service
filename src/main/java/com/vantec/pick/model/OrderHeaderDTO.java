package com.vantec.pick.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.vantec.pick.entity.OrderHeader;


public class OrderHeaderDTO  {
	
	private String documentReference;
	private String customerReference;
	private String orderType;
	private String consignee;
	private String shipTo;
	private String dockDestination;
	private Boolean autoCreated;
	private Boolean autoConfirmed;
	private Boolean allocated;
	private Boolean complete;
	private Boolean transmitted;
	private Date expectedDeliveryTime;
	private Date timeSlot;
	private String baanSerial;
	private String machineModel;
	private String typeIndicator;
	private String origin;
	private Long version;
	
	public Long getVersion() {
		return version;
	}
	public void setVersion(Long version) {
		this.version = version;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getTypeIndicator() {
		return typeIndicator;
	}
	public void setTypeIndicator(String typeIndicator) {
		this.typeIndicator = typeIndicator;
	}
	private List<OrderBodyDTO> orderBodyDTOs = new ArrayList<OrderBodyDTO>();
	
	
	public List<OrderBodyDTO> getOrderBodyDTOs() {
		return orderBodyDTOs;
	}
	public void setOrderBodyDTOs(List<OrderBodyDTO> orderBodyDTOs) {
		this.orderBodyDTOs = orderBodyDTOs;
	}
	public String getDocumentReference() {
		return documentReference;
	}
	public String getBaanSerial() {
		return baanSerial;
	}
	public void setBaanSerial(String baanSerial) {
		this.baanSerial = baanSerial;
	}
	public void setDocumentReference(String documentReference) {
		this.documentReference = documentReference;
	}
	public String getCustomerReference() {
		return customerReference;
	}
	public void setCustomerReference(String customerReference) {
		this.customerReference = customerReference;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getConsignee() {
		return consignee;
	}
	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}
	public String getShipTo() {
		return shipTo;
	}
	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}
	public String getDockDestination() {
		return dockDestination;
	}
	public void setDockDestination(String dockDestination) {
		this.dockDestination = dockDestination;
	}
	public Boolean getAutoCreated() {
		return autoCreated;
	}
	public void setAutoCreated(Boolean autoCreated) {
		this.autoCreated = autoCreated;
	}
	public Boolean getAutoConfirmed() {
		return autoConfirmed;
	}
	public void setAutoConfirmed(Boolean autoConfirmed) {
		this.autoConfirmed = autoConfirmed;
	}
	public Boolean getAllocated() {
		return allocated;
	}
	public void setAllocated(Boolean allocated) {
		this.allocated = allocated;
	}
	public Boolean getComplete() {
		return complete;
	}
	public void setComplete(Boolean complete) {
		this.complete = complete;
	}
	public Boolean getTransmitted() {
		return transmitted;
	}
	public void setTransmitted(Boolean transmitted) {
		this.transmitted = transmitted;
	}
	public Date getExpectedDeliveryTime() {
		return expectedDeliveryTime;
	}
	public void setExpectedDeliveryTime(Date expectedDeliveryTime) {
		this.expectedDeliveryTime = expectedDeliveryTime;
	}
	public Date getTimeSlot() {
		return timeSlot;
	}
	public void setTimeSlot(Date timeSlot) {
		this.timeSlot = timeSlot;
	}
	public String getMachineModel() {
		return machineModel;
	}
	public void setMachineModel(String machineModel) {
		this.machineModel = machineModel;
	}
	
}
